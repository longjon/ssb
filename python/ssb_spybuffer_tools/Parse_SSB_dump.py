#!/usr/bin/env python2

import argparse, sys
import re

# Input spybuffer columns
TF_COL_N = 0
DFA_IN_COL_N = 1
DFB_IN_COL_N = 2
AUXA_IN_COL_N = 3
AUXB_IN_COL_N = 4
DFA_OUT_COL_N = 5
DFB_OUT_COL_N = 6
AUXA_OUT_COL_N = 7
AUXB_OUT_COL_N = 8

# output spybuffer columns
HW_OUT_COL_N = 0 #jdl was 1
FTKSIM_COL_N = 0

# Hitwarrior packets
HW_LVL1ID_N = 6

# Key words
BEGIN_FRAGMENT_16 = 0xB0F0
BEGIN_FRAGMENT_32 = 0xB0F00000

# Event, holds one packet
class event:

    packet = list()
    name = ""
    def __init__(self, packet, name=""):
        self.packet = packet
        self.name = name

    # Return lvl1id
    def lvl1id(self):
        lvl1id = -1

        if self.packet[0] & 0xFFFF0000 == BEGIN_FRAGMENT_32:
            lvl1id = self.packet[HW_LVL1ID_N] # 4 ? TODO
        elif self.packet[0] & 0x0000FFFF == BEGIN_FRAGMENT_16:
            lvl1id = ( (0xFFFF & self.packet[6]) << 16 ) + 0xFFFF & self.packet[7]

        return lvl1id

    def Print(self):
        for i in self.packet:
            print "0x{:08x}".format(i)
            


class packetInfo:

    headerLength = 14
    header = list()
    #            (word, description)
    header.append( (0xB0F0, "Beginning of header" )  )
    header.append( (0xCAFE, "" )  )
    header.append( (0xFF12, "" )  )
    header.append( (0x34FF, "" )  )
    header.append( (-1, "Run Number" )  )
    header.append( (-1, "Run Number" )  )
    header.append( (-1, "Lvl1ID" )  )
    header.append( (-1, "Lvl1ID" )  )
    header.append( (-1, "Reserved" )  )
    header.append( (-1, "Reserved" )  )
    header.append( (-1, "Reserved" )  )
    header.append( (-1, "Reserved" )  )
    header.append( (-1, "Reserved" )  )
    header.append( (-1, "Reserved" )  )

    trackPacketLength = 28
    trackPacket = list()
    trackPacket.append( (0x0BDA, "Link ID + Track Packet (BDA)" ) )
    trackPacket.append( (-1, "Sector Number" ) )
    trackPacket.append( (-1, "Tower Number" ) )
    trackPacket.append( (-1, "Layer Map" ) )
    trackPacket.append( (-1, "Road Number" ) )
    trackPacket.append( (-1, "Road Number" ) )
    trackPacket.append( (-1, "Chi2" ) )
    trackPacket.append( (-1, "D0" ) )
    trackPacket.append( (-1, "Z0" ) )
    trackPacket.append( (-1, "Coth" ) )
    trackPacket.append( (-1, "Phi0" ) )
    trackPacket.append( (-1, "Curv" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )

    trailerLength = 16
    trailer = list()
    trailer.append( (0xE0DA, "End of Data") )
    trailer.append( (-1, "Len of debug block") )
    #trailer.append( (-1, "Debug info") )
    #trailer.append( (-1, "Debug info") )
    trailer.append( (0xE0DF, "E0DF") )
    trailer.append( (-1, "Len of debug block") )
    trailer.append( (-1, "LVL1ID") )
    trailer.append( (-1, "LVL1ID") )
    trailer.append( (-1, "Error Flag") )
    trailer.append( (-1, "Error Flag") )
    trailer.append( (-1, "Reserved") )
    trailer.append( (-1, "Reserved") )
    trailer.append( (-1, "Reserved") )
    trailer.append( (-1, "Reserved") )
    trailer.append( (0xE0F0, "E0F0") )
    trailer.append( (0xA5A5, "A5A5") )
    trailer.append( (0x5A5A, "5A5A") )
    trailer.append( (0x0E0F, "0E0F") )

#convert Hex to Float 
def HalfToFloat(h):
    s = int((h >> 15) & 0x00000001)     # sign
    e = int((h >> 10) & 0x0000001f)     # exponent
    f = int(h &         0x000003ff)     # fraction

    if e == 0:
       if f == 0:
          return int(s << 31)
       else:
          while not (f & 0x00000400):
             f <<= 1
             e -= 1
          e += 1
          f &= ~0x00000400
          print s,e,f
    elif e == 31:
       if f == 0:
          return int((s << 31) | 0x7f800000)
       else:
          return int((s << 31) | 0x7f800000 | (f << 13))

    e = e + (127 -15)
    f = f << 13

    return int((s << 31) | (e << 23) | f)


### ASCII column to array, i.e., input text file to array
def textToArray(f_ascii, n_col ):

    out_array = list()

    f_ascii.seek(0)

    for line in f_ascii:
        if "Dump the spybuffers" in line: # something to skip first line of ssb spybuffers
            continue

        if 'word' in line and '0x' in line: #SSB spybuffer dump format
            line_text_words = re.findall("(0x\S*)",line)

            text_word = line_text_words[n_col]
                        
            out_array.append( int(text_word, 0) )  # automatically determines hex from 0x

        else: # ftksim output format, assuming each line is just the word
            out_array.append( int(line, 16) )
    return out_array



# find b0f0 chunks
def Findb0f0(word_list):
    
    event_list = list()
    
    # find b0f0 chunks

    b0f0_indices = list()

    # find b0f0s
    for i, word in enumerate(word_list):
        if word & 0x0000FFFF ==  BEGIN_FRAGMENT_16 or word & 0xFFFF0000 == BEGIN_FRAGMENT_32: # found b0f0
            b0f0_indices.append(i)
    
#    print "b0f0 indices:"
#    print b0f0_indices

    return b0f0_indices

# turn list of words into separate b0f0 separated chunks
def arrayToEvents(word_list, b0f0_indices, name=""):
    
    event_list = list()
    
    # get chunks
    for i, indc in enumerate(b0f0_indices):        
        if i+1 < len(b0f0_indices): # Find next b0f0
            event_list.append( event( word_list[indc: b0f0_indices[i+1]], name ) )
        else: # end of file
            event_list.append( event( word_list[indc:-1], name ) )

    return event_list


def Checklen(test_event):
    if (len(test_event.packet)-30)%28 == 0:
        Trackno = (len(test_event.packet)-30)/28
    else: Trackno = -1

    return Trackno

#Check the format of an event array,  0 debug length assumed
def CheckFormat(test_event, Trackno,b0f0_indices, event_num,text, TFMode=False):  
    test_packet = list()
    for i, pack in enumerate(test_event.packet):
        if TFMode:
            test_packet.append( (0xffff0000 & test_event.packet[i]) >> 16 )
        else:
            test_packet.append(0x0000ffff & test_event.packet[i])

    FormatFlag = False

    if Trackno != -1:	# Packet with expected length
	Headcount = 0
	Trailcount = 0
	count9bda = 0
	i = 0
	while i < len(test_packet):

            if i<4:  #Compare with the Header Class
	    	if test_packet[i] == packetInfo.header[i][0]: 
	            Headcount += 1
		elif test_packet[i] != packetInfo.header[i][0]:
		    text.write("Cannot find "+str(hex(packetInfo.header[i][0]))+" in Header at line "+str( i+b0f0_indices[event_num] ) +"\n")
		

	    if test_packet[i] == 0x00009bda: # Count number of tracks
		count9bda += 1
	    
	    if i > 13 +Trackno*28: #Compare with Trailer Class
	        j = i-14-Trackno*28 # Trailer element number
	        if test_packet[i] == packetInfo.trailer[j][0]:
	    	    Trailcount += 1
		elif test_packet[i] != packetInfo.trailer[j][0] and packetInfo.trailer[j][0] != -1:
		    text.write("Cannot find "+str(hex(packetInfo.trailer[j][0]))+" in Trailer at line "+str( i+b0f0_indices[event_num]) +"\n")
		
    	    i += 1

	if Headcount == 4 and Trailcount == 6: FormatFlag = True 

    else:  # Packet with unexpected length, check existence of key words
    	i = 0
        text.write ("Unexpected packet length: "+str(len(test_packet))+" words\n")
	while i < len(test_packet):
	    if test_packet[i] == 0x0000b0f0:
	    	text.write ("0xb0f0 found\n")
	    elif test_packet[i] == 0x0000cafe:
	    	text.write ("0xcafe found\n")
	    elif test_packet[i] == 0x0000ff12:
	        text.write("0xff12 found\n")
	    elif test_packet[i] == 0x000034ff:
	    	text.write ("0x34ff found\n")
	    elif test_packet[i] == 0x00009bda:
		text.write ("0x9bda found\n")
	    elif test_packet[i] == 0x0000e0da:
	    	text.write ("0xe0da found\n")
	    elif test_packet[i] == 0x0000e0df:
	    	text.write ("0xe0df found\n")
	    elif test_packet[i] == 0x0000a5a5:
	    	text.write ("0xa5a5 found\n")
	    elif test_packet[i] == 0x00005a5a:
	    	text.write ("0x5a5a found\n")
	    elif test_packet[i] == 0x00000e0f:
	    	text.write ("0x0e0f found\n")
	    i += 1
    
    return FormatFlag


def CheckL1ID(test_event,Trackno,L1ID, TFMode):
    test_packet=list()
    L1ID_match = False

    if Trackno == -1:
        L1ID.append(0)
        return L1ID_match

    for i, pack in enumerate(test_event.packet):
        if TFMode:
            test_packet.append( (0xFFFF0000 & test_event.packet[i]) >> 16)
	else:
            test_packet.append(0x0000FFFF & test_event.packet[i])

    headID = (test_packet[6] << 16) + test_packet[7]  # ECR + L1A as in one number
    trailID = (test_packet[18+Trackno*28] << 16) +test_packet[19+Trackno*28]
    # Compare L1ID in Header and Trailer



    if headID == trailID: 
        L1ID.append(headID)
        L1ID_match = True
	
    else: L1ID.append(-1) # L1ID not match -> stored -1 

    return L1ID_match


def Parameter(test_event,Trackno,text):
    test_packet=list()
    for i, pack in enumerate(test_event.packet):
	test_packet.append(0x0000ffff & test_event.packet[i])
    text.write( "Track Parameter:\n") 
    import struct
    for count,i in enumerate(range(20,Trackno*28+14,28)):   # loop through hexlix parameter
    	# Unpack words into numbers
	    temp = HalfToFloat(test_packet[i])
	    string = struct.pack("I",temp)
	    chisq = struct.unpack("f",string)[0]

	    temp = HalfToFloat(test_packet[i+1])
	    string = struct.pack("I",temp)
	    d0 = struct.unpack("f",string)[0]

	    temp = HalfToFloat(test_packet[i+2])
	    string = struct.pack("I",temp)
	    z0 = struct.unpack("f",string)[0]

	    temp = HalfToFloat(test_packet[i+3])
	    string = struct.pack("I",temp)
	    cotth = struct.unpack("f",string)[0]

	    temp = HalfToFloat(test_packet[i+4])
	    string = struct.pack("I",temp)
	    phi0 = struct.unpack("f",string)[0]

	    temp = HalfToFloat(test_packet[i+5])
	    string = struct.pack("I",temp)
	    curv = struct.unpack("f",string)[0]

	    text.write( "\n")
	    text.write("Track "+str(count+1)+"\n")
	    text.write( "chisq = "+str(chisq)+"\n")
	    text.write( "d0    = "+str(d0)+"\n")
	    text.write( "z0    = "+str(z0)+"\n")
	    text.write( "cotth = "+str(cotth)+"\n")
	    text.write( "phi0  = "+str(phi0)+"\n")
	    text.write( "curv  = "+str(curv)+"\n")
	    


# compare two events
def compare(eventA, eventB, doubleFormat = False):
    packetA = eventA.packet
    packetB = eventB.packet

    
    if len(packetA) != len(packetB):
        print "Packet lengths do not match! Event:"+eventA.name+"="+str(len(packetA)) + " Event:"+eventB.name+"="+str(len(packetB))


    if eventA.lvl1id != eventB.lvl1id:
        print "LVL1IDs do not match! Event:"+eventA.name+"="+str(hex(eventA.lvl1id())) + " Event:"+eventB.name+"="+str(hex(eventB.lvl1id()))

    print ""
    print eventA.name + " =? " + eventB.name + "      <-- Expected word"
    nWordsShort = len(packetA) if len(packetA) < len(packetB) else len(packetB)
    nWordsLong = len(packetA) if len(packetA) > len(packetB) else len(packetB)

    inTrackPacket = False
    for i in range(nWordsShort): # first loop through shorter packet

        if doubleFormat:
            maskedWordA = packetA[i] # 32bit format... TODO
            maskedWordB = packetB[i]
        else:
            maskedWordA = (0x0000FFFF & packetA[i]) # 16bit format... TODO
            maskedWordB = (0x0000FFFF & packetB[i])
        
        #debug
        color = ""
        equality = " == "
        if maskedWordA != maskedWordB:
            color = "\033[31m"
            equality = " != "
        wordInfo = ""
        errorInfo = ""
        if not doubleFormat:            
            wordInfo = " <-- " + packetInfo.header[i][1] if i < packetInfo.headerLength else "" # this is in the packet header

            if i > 0 and (i - packetInfo.headerLength) % packetInfo.headerLength == 0: # this is expected to be a track header
                inTrackPacket = True
                wordInfo = " <-- " + packetInfo.trackPacket[ (i-packetInfo.headerLength) % packetInfo.trackPacketLength ][1]
            if inTrackPacket: # we're in a track packet
                wordInfo = " <-- " + packetInfo.trackPacket[ (i-packetInfo.headerLength) % packetInfo.trackPacketLength ][1]
            #if inTrackPacket and (i - packetInfo.headerLength % packetInfo.headerLength ==  packetInfo.headerLength -1 ): # expect to be the end of a track packet
            #    inTrackPacket = False
            
            trailerStart = nWordsShort - packetInfo.trailerLength
            if i>=trailerStart: # expect to have trailer here
                inTrackPacket == False
                wordInfo = " <-- " + packetInfo.trailer[ (i - nWordsShort) % packetInfo.trailerLength  ][1]
            

        print color + "0x{:08x}".format(maskedWordA) + equality + "0x{:08x}".format(maskedWordB) + "\033[0m" + wordInfo + " " + errorInfo
        

    if len(packetA) != len(packetB): # loop through rest of other packet
        for i in range(nWordsShort, nWordsLong):
            maskedWord = 0
            if len(packetA) > len(packetB):

                if doubleFormat:
                    maskedWord = packetA[i] # 32bit format... TODO
                else:
                    maskedWord = (0x0000FFFF & packetA[i]) # 16bit format... TODO
                print "\033[31m0x{:08x}".format(maskedWord) + " != " + "0x        " +"\033[0m" #+ " xor: " + "0x{:08x}".format(  maskedWord^0x0   ) 
            else:
                maskedWord = (0x0000FFFF & packetB[i])
                print "\033[31m0x        " + " != " + "0x{:08x}".format(maskedWord) +"\033[0m" #+ " xor: " + "0x{:08x}".format(  0x0^maskedWord   ) 
                
                


            

def main(args):

#    if sys.version_info[0] < 3 and sys.version_info[1] <7:
#        print "Needs >= Python 2.7"
#        sys.exit(0)

    # Open files

    if args.outputSpybufferFileName and args.inputSpybufferFileName:
        print "Can't run input and output spybuffers at the same time."
        sys.exit()

     
    spybuffer_event_list = list()
    if args.outputSpybufferFileName:
        try:
            spybuffer_file = open(args.outputSpybufferFileName)

            spybuffer_array = textToArray(spybuffer_file, HW_OUT_COL_N )
	    b0f0_list = Findb0f0(spybuffer_array)
            spybuffer_event_list = arrayToEvents(spybuffer_array, b0f0_list, "spybuffer")
            
            print "Found " + str(len(spybuffer_event_list)) + " events in spybuffer"

            spybuffer_file.close()
        except IOError:
            print "Couldn't open " + args.outputSpybufferFileName
            sys.exit()


    if args.inputSpybufferFileName:
        #todo column selection via input arg
        try:
            spybuffer_file = open(args.inputSpybufferFileName)

            spybuffer_array = textToArray(spybuffer_file, TF_COL_N )
	    b0f0_list = Findb0f0(spybuffer_array)
            spybuffer_event_list = arrayToEvents(spybuffer_array, b0f0_list, "spybuffer")
            
            print "Found " + str(len(spybuffer_event_list)) + " events in spybuffer"

            spybuffer_file.close()
        except IOError:
            print "Couldn't open " + args.inputSpybufferFileName
            sys.exit()


            
    ftksim_event_list = list()
    if args.ftkSimFileName != "":
        try:
            ftksim_file = open(args.ftkSimFileName)
            
            ftksim_array = textToArray(ftksim_file, FTKSIM_COL_N )
	    b0f0_list = Findb0f0(ftksim_array)
            ftksim_event_list = arrayToEvents(ftksim_array, b0f0_list, "FTKSim")            
            
            print "Found " + str(len(ftksim_event_list)) + " events in ftksim input"

            ftksim_file.close()
        except IOError:
            print "Couldn't open " + args.ftkSimFileName
            sys.exit()   


    # TODO: print out some usful starter info here.
    #print len(ftksim_event_list[0].packet)

    #spybuffer_event_list[0].Print()
    #print "------"
    
    #ftksim_event_list[0].Print()
    
    #print args.doubleBitWidth

    if args.compare:
        compare(spybuffer_event_list[0], ftksim_event_list[0], args.doubleBitWidth) 


    if args.parse:
        # temp
        TFMode = True


        Track= list()
	L1ID = list()
	Track_tot = 0
	good_count = 0
        FormatText = open("ssb_spybuffer_parse_format.log","w")
        ParaText = open("ssb_spybuffer_parse_helix.log","w")

	FormatText.write("--------------------------------------------------\n") 
	FormatText.write("--------------------------------------------------\n")# Space for overwrite

        for i,event in enumerate(spybuffer_event_list):
            Track.append(Checklen(event))
	    FormatText.write("--------------------------------------------------\n")
            FormatText.write("Event: "+str(i)+" line: "+str(b0f0_list[i])+"\n")

            L1ID_match = CheckL1ID(event,Track[i],L1ID, TFMode)
	    FormatText.write("ECR: "+str(L1ID[i]>>24)+"\nL1A: "+str(L1ID[i]&0x00ffffff)+"\n") # Write ECR and L1A
            Format = CheckFormat(event,Track[i],b0f0_list,i,FormatText, TFMode)
	    Skip = False 

	    if Format and L1ID_match:
		if i != 0 and not(L1ID[i-1] == L1ID[i]-1 or L1ID[i-1]>>24 == (L1ID[i]>>24)-1 ):  # Check if L1ID skip 
		    FormatText.write("\nWARNING:\nL1ID Skip Happen Here!\n\n")
		    Skip = True
		else:
	            FormatText.write("GOOD!\n")
		    good_count += 1
	    elif not(L1ID_match) and Track[i]!=-1: 
	        FormatText.write( "\nWARNING:\nL1ID not match!\n\n")

            if Track[i]>0:
	        Track_tot += Track[i]
     	        ParaText.write( "--------------------------------------------------\n")
	        ParaText.write( "Event: "+str(i+1)+" line: "+str(b0f0_list[i])+"\n")
		if not(Format) or not(L1ID_match) or Skip: 
		    ParaText.write("\nWARNING: Packet maybe broken!\nPlease Check format.log for details\n")
    	        Parameter(event, Track[i],ParaText)


	if Track_tot == 0: ParaText.write("This packet has no Track!\n")

        ParaText.write( "----------------------END-------------------------\n")
        FormatText.write( "---------------------END--------------------------\n")
	FormatText.seek(6)  # Overwrite the beginning
	FormatText.write(str(len(spybuffer_event_list))+" event found, "+ str(good_count)+" good, "+ str(len(spybuffer_event_list) - good_count)+ " bad")

        FormatText.close()
        ParaText.close()


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Parse SSB spybuffers")


    parser.add_argument('-i', '--inputSpybufferFileName', type=str, default="", help="Input spybuffer file")
    parser.add_argument('-o', '--outputSpybufferFileName', type=str, default="", help="Input spybuffer file")
    parser.add_argument('-f', '--ftkSimFileName', type=str, default="", help="FTK sim file")
    parser.add_argument('-c', '--compare', help="Compare spybuffer to ftksim", action="store_true")
    #parser.add_argument('-v', '--validatePacket', help="Validate packet against specifications", action="store_true")
    parser.add_argument('-d', '--doubleBitWidth', help="Turn on 32bit width comparisons, e.g. for input spybuffers", action="store_true")
    parser.add_argument('-p', '--parse', help="Print parse information", action="store_true")

    args = parser.parse_args()
    main(args)

    print "SSB spybuffer parsed"
