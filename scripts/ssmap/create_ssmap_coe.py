#!/bin/env python

import sys
import os

def help():
    print "Usage"
    print ""
    print "  %s SSMAPFILE TOWERNUMBER ISIBL=1 or 0 ISAUX=1 or 0"
    print ""
    print ""
    print ""
    sys.exit()

#
# some configurable constants
#
output_localid_bits_ibl = 6 # the maximum possible localid for IBL SSMap is 43 (even for 32tower configuration)
output_localid_bits_SCT = 9 # the maximum possible localid for SCT is 78 for 32 tower configuration and we need 2 more bits for layer information = so total of 9

#
# Check for arguments
#
if len(sys.argv) != 5:
    help()

#
# parsing arguments
#
ssmap_file_path = sys.argv[1]
tower_number = int(sys.argv[2])
is_ibl = sys.argv[3]
is_aux = sys.argv[4]

#
# parse "ISIBL" option
#
if is_ibl == "1":
    is_ibl = True
else:
    is_ibl = False

#
# parse "ISIBL" option
#
if is_aux == "1":
    is_aux = True
else:
    is_aux = False

#
# open up the moduleidmap file and aggregate the contents into a list
#
ssmap_file = open(ssmap_file_path)
ssmap_lines = [ x.strip() for x in ssmap_file.readlines() ]

#
# the "ssmap" will be a dictionary to hold moduleid -> localid
#
ssmap = {}

#
# parse all the moduleidmap and build up this "ssmap" dictionary
#
for line in ssmap_lines:
    # key is a tuple of (tower, layer, moduleid)
    key = (int(line.split()[0]), int(line.split()[1]), int(line.split()[2]))
    # don't need to build up the "ssmap" for the tower not interested
    if key[0] != tower_number: continue
    # if IBL the second element in the key-tuple will be 0
    if is_ibl:
        if key[1] != 0: continue
        word = int(line.split()[3])
        ssmap[key] = word
    # if SCT the second element in the key-tuple will be either 5, 7, 11
    else:
        if key[1] not in [5, 7, 11]: continue
        word = int(line.split()[3])
        ssmap[key] = word

#
# The ssmap dictionary is built now we need to output them in the .coe format
# .coe format will be essentially the content of the ssmap for each line number
# so the first line will hold the local id matching to the key, (tower, layer, 0)
# and the second line will hold the local id matching to the key, (tower, layer, 1)
# and the second line will hold the local id matching to the key, (tower, layer, 1)
# ...
# and the N-th line will hold the local id matching to the key, (tower, layer, N)
# ...
# ...
# ...
#
#

#
# the maximum possible moduleID = 8192
# Although not necessary right now, in the future we will only have 2 towers
# worth of LUT per FPGA, so the maximum line is limited to 16384
#

#
# each .coe file starts with the following two line
#
#print "memory_initialization_radix=2;"
#print "memory_initialization_vector="

#
# Now printing the content
#
for i in xrange(16384):
    if is_aux:
        if i != 16383:
            if is_ibl:
                if (tower_number, 0, i) in ssmap:
                #print i, ssmap[(tower_number, 0, i)]
                    print '%08x' % (int(("00"+(bin(i)[2:].zfill(14))+"000"+bin(ssmap[(tower_number, 0, i)])[2:].zfill(output_localid_bits_ibl)+""),2))
                else:
                    print '%08x' % (int(("00"+(bin(i)[2:].zfill(14))+"000"+bin(i%64)[2:].zfill(6)+""),2))
            else:
                if (tower_number, 5, i) in ssmap:
                    print '%08x' % (int(("01"+(bin(i)[2:].zfill(14))+"00"+(bin(ssmap[(tower_number, 5, i)])[2:]).zfill(output_localid_bits_SCT-2)+""),2))
                elif (tower_number, 7, i) in ssmap:
                    print '%08x' % (int(("01"+(bin(i)[2:].zfill(14))+"01"+(bin(ssmap[(tower_number, 7, i)])[2:]).zfill(output_localid_bits_SCT-2)+""),2))
                elif (tower_number, 11, i) in ssmap:
                    print '%08x' % (int(("01"+(bin(i)[2:].zfill(14))+"10"+(bin(ssmap[(tower_number, 11, i)])[2:]).zfill(output_localid_bits_SCT-2)+""),2))
                else:
                    print '%08x' % (int(("01"+(bin(i)[2:].zfill(14))+bin(i%64)[2:].zfill(output_localid_bits_SCT)+""),2))
        else:
            if is_ibl:
                if (tower_number, 0, i) in ssmap:
                #print i, ssmap[(tower_number, 0, i)]
                    print '%08x' % (int(("00"+(bin(i)[2:].zfill(14))+"000"+bin(ssmap[(tower_number, 0, i)])[2:].zfill(6)+""),2))
                else:
                    print '%08x' % (int(("00"+(bin(i)[2:].zfill(14))+"000"+bin(i%64)[2:].zfill(output_localid_bits_ibl)+""),2))
            else:
                if (tower_number, 5, i) in ssmap:
                    print '%08x' % (int(("01"+(bin(i)[2:].zfill(14))+"00"+(bin(ssmap[(tower_number, 5, i)])[2:]).zfill(output_localid_bits_SCT-2)+""),2))
                elif (tower_number, 7, i) in ssmap:
                    print '%08x' % (int(("01"+(bin(i)[2:].zfill(14))+"01"+(bin(ssmap[(tower_number, 7, i)])[2:]).zfill(output_localid_bits_SCT-2)+""),2))
                elif (tower_number, 11, i) in ssmap:
                    print '%08x' % (int(("01"+(bin(i)[2:].zfill(14))+"10"+(bin(ssmap[(tower_number, 11, i)])[2:]).zfill(output_localid_bits_SCT-2)+""),2))
                else:
                    print '%08x' % (int(("01"+(bin(i)[2:].zfill(14))+bin(i%64)[2:].zfill(output_localid_bits_SCT)+""),2))
    else:
        if i != 16383:
            if is_ibl:
                if (tower_number, 0, i) in ssmap:
                #print i, ssmap[(tower_number, 0, i)]
                    print '%08x' % (int(("10"+(bin(i)[2:].zfill(14))+"000"+bin(ssmap[(tower_number, 0, i)])[2:].zfill(output_localid_bits_ibl)+""),2))
                else:
                    print '%08x' % (int(("10"+(bin(i)[2:].zfill(14))+"000"+bin(i%64)[2:].zfill(6)+""),2))
            else:
                if (tower_number, 5, i) in ssmap:
                    print '%08x' % (int(("11"+(bin(i)[2:].zfill(14))+"00"+(bin(ssmap[(tower_number, 5, i)])[2:]).zfill(output_localid_bits_SCT-2)+""),2))
                elif (tower_number, 7, i) in ssmap:
                    print '%08x' % (int(("11"+(bin(i)[2:].zfill(14))+"01"+(bin(ssmap[(tower_number, 7, i)])[2:]).zfill(output_localid_bits_SCT-2)+""),2))
                elif (tower_number, 11, i) in ssmap:
                    print '%08x' % (int(("11"+(bin(i)[2:].zfill(14))+"10"+(bin(ssmap[(tower_number, 11, i)])[2:]).zfill(output_localid_bits_SCT-2)+""),2))
                else:
                    print '%08x' % (int(("11"+(bin(i)[2:].zfill(14))+bin(i%64)[2:].zfill(output_localid_bits_SCT)+""),2))
        else:
            if is_ibl:
                if (tower_number, 0, i) in ssmap:
                #print i, ssmap[(tower_number, 0, i)]
                    print '%08x' % (int(("10"+(bin(i)[2:].zfill(14))+"000"+bin(ssmap[(tower_number, 0, i)])[2:].zfill(6)+""),2))
                else:
                    print '%08x' % (int(("10"+(bin(i)[2:].zfill(14))+"000"+bin(i%64)[2:].zfill(output_localid_bits_ibl)+""),2))
            else:
                if (tower_number, 5, i) in ssmap:
                    print '%08x' % (int(("11"+(bin(i)[2:].zfill(14))+"00"+(bin(ssmap[(tower_number, 5, i)])[2:]).zfill(output_localid_bits_SCT-2)+""),2))
                elif (tower_number, 7, i) in ssmap:
                    print '%08x' % (int(("11"+(bin(i)[2:].zfill(14))+"01"+(bin(ssmap[(tower_number, 7, i)])[2:]).zfill(output_localid_bits_SCT-2)+""),2))
                elif (tower_number, 11, i) in ssmap:
                    print '%08x' % (int(("11"+(bin(i)[2:].zfill(14))+"10"+(bin(ssmap[(tower_number, 11, i)])[2:]).zfill(output_localid_bits_SCT-2)+""),2))
                else:
                    print '%08x' % (int(("11"+(bin(i)[2:].zfill(14))+bin(i%64)[2:].zfill(output_localid_bits_SCT)+""),2))
