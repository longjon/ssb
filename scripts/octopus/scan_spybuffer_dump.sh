#!/bin/bash

cat $1 | \
    tail -n +2 | \
    awk \
        -v pstart=0xb0f00000 \
        -v pend=0xe0f00000 \
        -v dfcol=18 \
        -v auxcol=24 \
        -v outfiledf=common_events_df.dat \
        -v outfileaux=common_events_aux.dat \
'
function printarray(a,    i) {
    for (i in a)
        print a[i]
}
function inarray(a, el,    i) {
    for (i in a)
        if (a[i] == el)
            return 1
    return 0;
}
function alen(a,    i, k) {
    k = 0
    for(i in a)
        k++
    return k
}
function minindex(a,    i, k) {
    k = -1
    for(i in a) {
        if (k == -1)
            k = i
        else if (i < k)
            k = i
    }
    return k
}
function maxindex(a,    i, k) {
    k = -1
    for(i in a) {
        if (k == -1)
            k = i
        else if (i > k)
            k = i
    }
    return k
}
function min(a, b) {
    if (a > b)
        return b
    else
        return a
}
function max(a, b) {
    if (a > b)
        return a
    else
        return b
}
BEGIN {
    linecnt = 1
}
{
    if ($dfcol==pstart) {
        wait_df_l1=1
        wait_df_l1_cnt=0
        df_start_inc[df_start_cnt++] = NR
    }
    if (wait_df_l1==1) {
        if (wait_df_l1_cnt < 3)
            wait_df_l1_cnt += 1
        else {
            wait_df_l1 = 0
            wait_df_l1_cnt = 0
            df_l1[df_l1_cnt++] = $dfcol
            df_start[$dfcol] = df_start_inc[df_start_cnt-1]
        }
    }
    if (df_l1_cnt > df_end_cnt) {
        if ($dfcol==pend)
            df_end[df_l1[df_end_cnt++]] = NR
    }

    if ($auxcol==pstart) {
        wait_aux_l1=1
        wait_aux_l1_cnt=0
        aux_start_inc[aux_start_cnt++] = NR
    }
    if (wait_aux_l1==1) {
        if (wait_aux_l1_cnt < 3)
            wait_aux_l1_cnt += 1
        else {
            wait_aux_l1 = 0
            wait_aux_l1_cnt = 0
            aux_l1[aux_l1_cnt++] = $auxcol
            aux_start[$auxcol] = aux_start_inc[aux_start_cnt-1]
        }
    }
    if (aux_l1_cnt > aux_end_cnt) {
        if ($auxcol==pend)
            aux_end[aux_l1[aux_end_cnt++]] = NR
    }
    dfdata[linecnt] = $dfcol
    auxdata[linecnt++] = $auxcol
}
END {
    common_l1_ids=0
    for (i in aux_l1)
        if (inarray(df_l1, aux_l1[i])==1)
            common_l1_ids += 1
    print alen(df_l1) " DF L1 IDs and " alen(aux_l1) " AUX L1 IDs found"
    print ""
    if (common_l1_ids > 0)
        print common_l1_ids " L1 IDs are common:"
    else
        print "No common L1 IDs."
    for (i in aux_l1)
        if (inarray(df_l1, aux_l1[i])==1)
            print aux_l1[i]
    print ""
    print "DF L1 IDs found:"
    printarray(df_l1)
    if (df_end_cnt < df_l1_cnt) {
        print ""
        print "There are incomplete events."
    }
    print ""
    print "AUX L1 IDs found:"
    printarray(aux_l1)
    if (aux_end_cnt < aux_l1_cnt) {
        print ""
        print "There are incomplete events."
    }
    if (common_l1_ids > 0) {
        print ""
        overlap_start = max(minindex(df_start), minindex(aux_start))
        overlap_end = min(maxindex(df_end), maxindex(aux_end))
        if (overlap_start < overlap_end) {
            print "Common events for DF: line " df_start[overlap_start] " to line " df_end[overlap_end] + 1
            print "Common events for AUX: line " aux_start[overlap_start] " to line " aux_end[overlap_end] + 1
            for (i = df_start[overlap_start]; i < df_end[overlap_end] + 1; i += 1)
                print dfdata[i] >outfiledf
            print "DF output for common events written to " outfiledf
            for (i = aux_start[overlap_start]; i < aux_end[overlap_end] + 1; i += 1)
                print auxdata[i] >outfileaux
            print "AUX output for common events written to " outfileaux
        } else {
            print "Common events for DF: line " df_start[overlap_start] ", through start of file, to line " df_end[overlap_end] + 1
            print "Common events for AUX: line " aux_start[overlap_start] ", through start of file, to line " aux_end[overlap_end] + 1
            for (i = df_start[overlap_start]; i < df_end[NR-3]; i += 1)
                print dfdata[i] >outfiledf
            for (i = df_start[1]; i < df_end[overlap_end]; i += 1)
                print dfdata[i] >>outfiledf
            print "DF output for common events written to " outfiledf
            for (i = aux_start[overlap_start]; i < aux_end[NR-3]; i += 1)
                print auxdata[i] >outfileaux
            for (i = aux_start[1]; i < aux_end[overlap_end]; i += 1)
                print auxdata[i] >>outfileaux
            print "AUX output for common events written to " outfileaux
        }
    } else {
        print "Removed output files because of no common events"
        system("rm " outfiledf)
        system("rm " outfileaux)
    }
}
'
