#! /bin/bash

source slots.sh

echo Stopping Octopus
vme_poke 0x${OCTOPUS_CODE}00460c
sleep 0.5
vme_poke 0x${OCTOPUS_CODE}004610
vme_poke 0x${OCTOPUS_CODE}004614


echo Stopping EXTF0
vme_poke 0x${SSB_CODE}00060c
sleep 0.5
vme_poke 0x${SSB_CODE}000610
vme_poke 0x${SSB_CODE}000614


# two primaries
#echo Stopping EXTF1
#vme_poke 0x${SSB_CODE}00160c
#sleep 0.5
#vme_poke 0x${SSB_CODE}001610
#vme_poke 0x${SSB_CODE}001614

echo Stopping HW
vme_poke 0x${SSB_CODE}00460c
sleep 0.5
vme_poke 0x${SSB_CODE}004610
vme_poke 0x${SSB_CODE}004614
