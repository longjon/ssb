#!/bin/bash

source slots.sh

## Check if the arguments are valid
if [ "$#" -lt "0" ] || [ "$#" -gt "1" ]
then
    echo
    echo Incorrect number of arguments
    echo
    echo OPTIONAL PARAMETER 1: To disable the loading of Test Vectors use the fast option 
    echo "EXAMPLE:     ./configure.sh         ## Configure and Load" 
    echo "EXAMPLE:     ./configure.sh fast    ## Configure Only"
    echo
    exit 1
fi
if [ "$#" -eq "1" ] && [ "$1" != "fast" ]
then
    echo
    echo Unrecognized parameter $1
    echo
    echo OPTIONAL PARAMETER 1: To disable the loading of Test Vectors use the fast option 
    echo "EXAMPLE:     ./configure.sh         ## Configure and Load" 
    echo "EXAMPLE:     ./configure.sh fast    ## Configure Only"
    echo
    exit 1
fi


./configure_octopus.sh $OCTOPUS_SLOT $1
./configure_extf.sh $SSB_SLOT 0 $1

# Two primaries
#./configure_extf.sh 12 1 $1

./configure_hw.sh  $SSB_SLOT
vme_poke 0x${SSB_CODE}004600