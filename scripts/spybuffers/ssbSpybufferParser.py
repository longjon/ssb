#!/usr/bin/env python

"""
Script to compare SSB spybuffers to FTKSim and perform other checks.

"""



import argparse, sys
import re

# Tower number
towerN = 0 # todo put in

# linkID
TRACKHEADER = 0xbda

# Input spybuffer columns
TF_COL_N = 0
DFA_IN_COL_N = 1
DFB_IN_COL_N = 2
AUXA_IN_COL_N = 3
AUXB_IN_COL_N = 4
DFA_OUT_COL_N = 5
DFB_OUT_COL_N = 6
AUXA_OUT_COL_N = 7
AUXB_OUT_COL_N = 8

# output spybuffer columns
HW_OUT_COL_N = 0 #jdl was 1
FTKSIM_COL_N = 0

# EXTF packets
TF_LVL1ID_N = 6

# Hitwarrior packets
HW_LVL1ID_N = 6

# Key words
BEGIN_FRAGMENT_16 = 0xB0F0
BEGIN_FRAGMENT_32 = 0xB0F00000

END_DATA_16 = 0XE0DA



# holds one track, inside of an event
class track:
    def __init__(self, packet):        
        self.packet = packet

    def getSector(self):
        return (0xFFFF & self.packet[1])

    def getLayerMap(self):
        return (0xFFFF & self.packet[3])

    def getPacket(self):
        return self.packet

    def Print(self):
        for i in self.packet:
            print "0x{:08x}".format(i)

    # return true if track is OK
    def validate(self):
        if len(self.packet) != packetInfo16.trackPacketLength:
            print "Bad track length: "+str(len(self.packet)) + " expected " + str(packetInfo16.trackPacketLength)
            return False

        return True

class header:
    def __init__(self, packet):
        self.packet = packet

    def getPacket(self):
        return self.packet

    def Print(self):
        for i in self.packet:
            print "0x{:08x}".format(i)

    # return true if header is OK
    def validate(self):
        if len(self.packet) !=packetInfo16.headerLength:
            print "Bad header length: "+str(len(self.packet)) + " expected " + str(packetInfo16.headerLength)
            return False

        return True

class trailer:
    def __init__(self, packet):
        self.packet = packet

    def getPacket(self):
        return self.packet

    def Print(self):
        for i in self.packet:
            print "0x{:08x}".format(i)

    # return true if trailer is OK
    def validate(self):
        if len(self.packet) !=packetInfo16.trailerLength:
            print "Bad trailer length: "+str(len(self.packet)) + " expected " + str(packetInfo16.trailerLength)
            return False

        return True



# Event, holds one packet
class event:

    def __init__(self, packet, name=""):
        self.packet = packet
        self.name = name

        self.tracks = self.getTracks()

        self.header = self.getHeader()
        self.trailer = self.getTrailer()


    # split into track chunks by bda
    def getTracks(self):
        tracks = list()

        # loop through words
        startTrack = -1
        endTrack = -1
        for i, word in enumerate(self.packet):
            if (word & 0xFFF) == TRACKHEADER or (word & 0xFFFF == END_DATA_16): 
                if startTrack == -1:
                    startTrack = i
                elif endTrack == -1:
                    endTrack = i                
                    tracks.append(track(self.packet[startTrack:endTrack]))
                    startTrack = i
                    endTrack = -1

        return tracks


    # get header
    def getHeader(self):        
        #return header(self.packet[0:packetInfo16.headerLength]) # old way assuming size
        for i, word in enumerate(self.packet):
            if (word & 0xFFFF) == 0xE0DA or (word & 0xFFF) == TRACKHEADER: # find start of trailer or track
                return header(self.packet[0:i])
        return header([])

    # get trailer
    def getTrailer(self):
        for i,word in enumerate(self.packet):
            if (word & 0xFFFF) == 0xE0DA: # start of trailer
                return trailer(self.packet[i:])
        return trailer([])

    # Return lvl1id
    def lvl1id(self):
        lvl1id = -1

        if self.packet[0] & 0xFFFF0000 == BEGIN_FRAGMENT_32:
            lvl1id = self.packet[TF_LVL1ID_N] # todo: this only works for the TF, not 32bit lines
        elif self.packet[0] & 0x0000FFFF == BEGIN_FRAGMENT_16:
            lvl1id = ( (0xFFFF & self.packet[HW_LVL1ID_N]) << 16 ) + 0xFFFF & self.packet[HW_LVL1ID_N+1]

        return lvl1id

    def Print(self):
        for i in self.packet:
            print "0x{:08x}".format(i)
            

    # return true if event passes checks
    def validate(self):

        # check for repeated words
        prevWord = 0
        for i, word in enumerate(self.packet):
            if i == 0: continue # skip forward one

            if word == prevWord and word != 0x0 and word !=0xf0000000: # ignore repeated 0s as they will show up
                print "Repeated word: 0x{:08x}".format(word)
            prevWord = word
            


        headerCheck = self.header.validate()
        trailerCheck = self.trailer.validate()
        
        trackCheck = True
        for track in self.tracks:
            trackCheck = trackCheck and track.validate()

        return  headerCheck and trailerCheck and trackCheck
        




# 16-bit word packet info
class packetInfo16:

    headerLength = 14
    header = list()
    #            (word, description)
    header.append( (0xB0F0, "Beginning of header" )  )
    header.append( (0xCAFE, "" )  )
    header.append( (0xFF12, "" )  )
    header.append( (0x34FF, "" )  )
    header.append( (-1, "Run Number" )  )
    header.append( (-1, "Run Number" )  )
    header.append( (-1, "Lvl1ID" )  )
    header.append( (-1, "Lvl1ID" )  )
    header.append( (-1, "Reserved" )  )
    header.append( (-1, "Reserved" )  )
    header.append( (-1, "Reserved" )  )
    header.append( (-1, "Reserved" )  )
    header.append( (-1, "Reserved" )  )
    header.append( (-1, "Reserved" )  )

    trackPacketLength = 28
    trackPacket = list()
    trackPacket.append( (0x0BDA, "Link ID + Track Packet (BDA)" ) )
    trackPacket.append( (-1, "Sector Number" ) )
    trackPacket.append( (-1, "Tower Number" ) )
    trackPacket.append( (-1, "Layer Map" ) )
    trackPacket.append( (-1, "Road Number" ) )
    trackPacket.append( (-1, "Road Number" ) )
    trackPacket.append( (-1, "Chi2" ) )
    trackPacket.append( (-1, "D0" ) )
    trackPacket.append( (-1, "Z0" ) )
    trackPacket.append( (-1, "Coth" ) )
    trackPacket.append( (-1, "Phi0" ) )
    trackPacket.append( (-1, "Curv" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )
    trackPacket.append( (-1, "Hit" ) )

    trailerLength = 16
    trailer = list()
    trailer.append( (0xE0DA, "End of Data") )
    trailer.append( (-1, "Len of debug block") )
    #trailer.append( (-1, "Debug info") )
    #trailer.append( (-1, "Debug info") )
    trailer.append( (0xE0DF, "E0DF") )
    trailer.append( (-1, "Len of debug block") )
    trailer.append( (-1, "LVL1ID") )
    trailer.append( (-1, "LVL1ID") )
    trailer.append( (-1, "Error Flag") )
    trailer.append( (-1, "Error Flag") )
    trailer.append( (-1, "Reserved") )
    trailer.append( (-1, "Reserved") )
    trailer.append( (-1, "Reserved") )
    trailer.append( (-1, "Reserved") )
    trailer.append( (0xE0F0, "E0F0") )
    trailer.append( (0xA5A5, "A5A5") )
    trailer.append( (0x5A5A, "5A5A") )
    trailer.append( (0x0E0F, "0E0F") )



### ASCII column to array, i.e., input text file to array
def textToArray(f_ascii, n_col , TFMode = False):

    out_array = list()

    f_ascii.seek(0)

    for line in f_ascii:
        if "Dump the spybuffers" in line: # something to skip first line of ssb spybuffers
            continue

        if 'word' in line and '0x' in line: #SSB spybuffer dump format # jdl
            line_text_words = re.findall("(0x\S*)",line)

            text_word = line_text_words[n_col]
            
            hex_word = int(text_word, 0) # automatically determines hex from 0x
            if (TFMode): hex_word = (hex_word >> 16) # strip off right 16 most bits to re-align tf spy

            out_array.append( hex_word )  # automatically determines hex from 0x

        else: # ftksim output format, assuming each line is just the word
            out_array.append( int(line, 16) )
    
    return out_array



# turn list of words into separate b0f0 separated chunks
def arrayToEvents(word_list, name=""):
    
    event_list = list()
    
    # find b0f0 chunks

    b0f0_indices = list()

    # find b0f0s
    for i, word in enumerate(word_list):
        if word & 0x0000FFFF ==  BEGIN_FRAGMENT_16 or word & 0xFFFF0000 == BEGIN_FRAGMENT_32: # found b0f0
            b0f0_indices.append(i)

    # get chunks
    for i, indc in enumerate(b0f0_indices):        
        if i+1 < len(b0f0_indices): # Find next b0f0
            event_list.append( event( word_list[indc: b0f0_indices[i+1]], name ) )
        else: # end of file
            event_list.append( event( word_list[indc:], name ) )

    #print "b0f0 indices:"
    #print b0f0_indices
    return event_list


def findMatchingTrackBySector(trackA, sortedTracks):

    for t in sortedTracks:
        if trackA.getSector() == t.getSector():

            #if trackA.getLayerMap() == t.getLayerMap(): # todo turnon layermap matching again
            #    return t.getPacket()

            # remove track from list #new jdl
            sortedTracks.remove(t)

            return t.getPacket()

    return []




def compareNew(eventA, eventB, doubleFormat = False):

    print eventA.name + " =? " + eventB.name + "      <-- Expected word"

    ### Header
    headerA = eventA.header.getPacket() # grab list of words for header
    headerB = eventB.header.getPacket()

    # print some basic checks
    if len(headerA) !=len(headerB):
        print "Header lengths do not match!"

    if len(headerA) != packetInfo16.headerLength:
        print eventA.name + " header length of " + str(len(headerA)) + " does not match " + str(packetInfo16.headerLength)
    if len(headerB) != packetInfo16.headerLength:
        print eventB.name + " header length of " + str(len(headerB)) + " does not match " + str(packetInfo16.headerLength)

    nWordsShort = len(headerA) if len(headerA) < len(headerB) else len(headerB)
    nWordsLong = len(headerA) if len(headerA) > len(headerB) else len(headerB)

    # loop through shorter header
    for i in range(nWordsShort):

        if doubleFormat:
            maskedWordA = headerA[i] # 32bit format...
            maskedWordB = headerB[i]
        else:
            maskedWordA = (0x0000FFFF & headerA[i]) # 16bit format...
            maskedWordB = (0x0000FFFF & headerB[i])

        color = ""
        equality = " == "
        wordInfo = ""
        errorInfo = ""
        if maskedWordA != maskedWordB:
            color = "\033[31m"
            equality = " != "

        if not doubleFormat:            
            wordInfo = " <-- " + packetInfo16.header[i][1] if i < packetInfo16.headerLength else "" # this is in the packet header

        print color + "0x{:08x}".format(maskedWordA) + equality + "0x{:08x}".format(maskedWordB) + "\033[0m" + wordInfo + " " + errorInfo

    if len(headerA) != len(headerB): # loop through rest of other header
        for i in range(nWordsShort, nWordsLong):
            maskedWord = 0
            if len(headerA) > len(headerB):

                if doubleFormat:
                    maskedWord = headerA[i] # 32bit format... TODO
                else:
                    maskedWord = (0x0000FFFF & headerA[i]) # 16bit format... TODO
                print "\033[31m0x{:08x}".format(maskedWord) + " != " + "0x        " +"\033[0m" #+ " xor: " + "0x{:08x}".format(  maskedWord^0x0   ) 
            else:
                maskedWord = (0x0000FFFF & headerB[i])
                print "\033[31m0x        " + " != " + "0x{:08x}".format(maskedWord) +"\033[0m" #+ " xor: " + "0x{:08x}".format(  0x0^maskedWord   ) 

    ### Tracks
    sortedTracksA = sorted(eventA.tracks, key=lambda track: (track.getSector(), -track.getLayerMap()))
    sortedTracksB = sorted(eventB.tracks, key=lambda track: (track.getSector(), -track.getLayerMap()))


    if len(sortedTracksA) !=len(sortedTracksB):
        print "NTracks does not match!"

    nTracksShort = len(sortedTracksA) if len(sortedTracksA) < len(sortedTracksB) else len(sortedTracksB)
    nTracksLong = len(sortedTracksA) if len(sortedTracksA) > len(sortedTracksB) else len(sortedTracksB)
    
    # loop through the long list of tracks and try to match short list of tracks to it, the track matcher returns a list of size 0 when it doesn't find a match
    for t in range(nTracksLong):
        
        if len(sortedTracksA) > len(sortedTracksB):
            trackA = sortedTracksA[t].getPacket()
            trackB = findMatchingTrackBySector(sortedTracksA[t], sortedTracksB)
        else:
            trackB = sortedTracksB[t].getPacket()
            trackA = findMatchingTrackBySector(sortedTracksB[t], sortedTracksA)

        # print some basic checks
        if len(trackA)==0 or len(trackB)==0:
            print "Missing Track"
        else:
            if len(trackA) !=len(trackB):
                print "Track lengths do not match!"
                
            if len(trackA) != packetInfo16.trackPacketLength:
                print eventA.name + " track length of " + str(len(trackA)) + " does not match " + str(packetInfo16.trackPacketLength)
            if len(trackB) != packetInfo16.trackPacketLength:
                print eventB.name + " track length of " + str(len(trackB)) + " does not match " + str(packetInfo16.trackPacketLength)


        nWordsShort = len(trackA) if len(trackA) < len(trackB) else len(trackB)
        nWordsLong = len(trackA) if len(trackA) > len(trackB) else len(trackB)
    
        for i in range(nWordsShort):
            if doubleFormat:
                maskedWordA = trackA[i] # 32bit format...
                maskedWordB = trackB[i]
            else:
                maskedWordA = (0x0000FFFF & trackA[i]) # 16bit format...
                maskedWordB = (0x0000FFFF & trackB[i])
            
            color = ""
            equality = " == "
            wordInfo = ""
            errorInfo = ""
            if maskedWordA != maskedWordB:
                color = "\033[31m"
                equality = " != "
            
            if not doubleFormat:            
                wordInfo = " <-- " + packetInfo16.trackPacket[i][1] if i < packetInfo16.trackPacketLength else "" # this is in the packet header
    
            print color + "0x{:08x}".format(maskedWordA) + equality + "0x{:08x}".format(maskedWordB) + "\033[0m" + wordInfo + " " + errorInfo
        
        if len(trackA) != len(trackB): # loop through rest of other track
            for i in range(nWordsShort, nWordsLong):
                maskedWord = 0
                if len(trackA) > len(trackB):
            
                    if doubleFormat:
                        maskedWord = trackA[i] # 32bit format... TODO
                    else:
                        maskedWord = (0x0000FFFF & trackA[i]) # 16bit format... TODO
                    print "\033[31m0x{:08x}".format(maskedWord) + " != " + "          " +"\033[0m" #+ " xor: " + "0x{:08x}".format(  maskedWord^0x0   ) 
                else:
                    maskedWord = (0x0000FFFF & trackB[i])
                    print "\033[31m          " + " != " + "0x{:08x}".format(maskedWord) +"\033[0m" #+ " xor: " + "0x{:08x}".format(  0x0^maskedWord   )


    # loop over unmatched tracks
    remainingTracks = list()
    if len(sortedTracksA) > len(sortedTracksB):
        remainingTracks = sortedTracksB
    else:
        remainingTracks = sortedTracksA

    for t in remainingTracks:
        print "Missing Track"
        packet = t.getPacket()
        color = "\033[31m"
        equality = " != "
        for i in range(len(packet)):
            maskedWord = (0x0000FFFF & packet[i])
            if len(sortedTracksA) > len(sortedTracksB):
                print "\033[31m          " + " != " + "0x{:08x}".format(maskedWord) +"\033[0m" #+ " xor: " + "0x{:08x}".format(  0x0^maskedWord   )
            else:
                print "\033[31m0x{:08x}".format(maskedWord) + " != " + "          " +"\033[0m" #+ " xor: " + "0x{:08x}".format(  maskedWord^0x0   ) 


    ### Trailer
    trailerA = eventA.trailer.getPacket()
    trailerB = eventB.trailer.getPacket()

    if len(trailerA) !=len(trailerB):
        print "Trailer lengths do not match!"

    if len(trailerA) != packetInfo16.trailerLength:
        print eventA.name + " trailer length of " + str(len(trailerA)) + " does not match  " + str(packetInfo16.trailerLength)
    if len(trailerB) != packetInfo16.trailerLength:
        print eventB.name + " trailer length of " + str(len(trailerB)) + " does not match " + str(packetInfo16.trailerLength)

    nWordsShort = len(trailerA) if len(trailerA) < len(trailerB) else len(trailerB)
    nWordsLong = len(trailerA) if len(trailerA) > len(trailerB) else len(trailerB)

    for i in range(nWordsShort):

        if doubleFormat:
            maskedWordA = trailerA[i] # 32bit format...
            maskedWordB = trailerB[i]
        else:
            maskedWordA = (0x0000FFFF & trailerA[i]) # 16bit format...
            maskedWordB = (0x0000FFFF & trailerB[i])

        color = ""
        equality = " == "
        wordInfo = ""
        errorInfo = ""
        if maskedWordA != maskedWordB:
            color = "\033[31m"
            equality = " != "

        if not doubleFormat:            
            wordInfo = " <-- " + packetInfo16.trailer[i][1] if i < packetInfo16.trailerLength else "" # this is in the packet trailer

        print color + "0x{:08x}".format(maskedWordA) + equality + "0x{:08x}".format(maskedWordB) + "\033[0m" + wordInfo + " " + errorInfo

    if len(trailerA) != len(trailerB): # loop through rest of other trailer
        for i in range(nWordsShort, nWordsLong):
            maskedWord = 0
            if len(trailerA) > len(trailerB):

                if doubleFormat:
                    maskedWord = trailerA[i] # 32bit format... TODO
                else:
                    maskedWord = (0x0000FFFF & trailerA[i]) # 16bit format... TODO
                print "\033[31m0x{:08x}".format(maskedWord) + " != " + "          " +"\033[0m" #+ " xor: " + "0x{:08x}".format(  maskedWord^0x0   ) 
            else:
                maskedWord = (0x0000FFFF & trailerB[i])
                print "\033[31m          " + " != " + "0x{:08x}".format(maskedWord) +"\033[0m" #+ " xor: " + "0x{:08x}".format(  0x0^maskedWord   ) 



##############################
# Main script calling functions above
##############################

def main(args):

    if sys.version_info[0] < 3 and sys.version_info[1] <7:
        print "Needs >= Python 2.7"
        sys.exit(0)

    if args.outputSpybufferFileName and args.inputSpybufferFileName:
        print "Can't run input and output spybuffers at the same time."
        sys.exit()
        

    ### Open text files and convert to arrays of events
    spybuffer_event_list = list()
    if args.outputSpybufferFileName:
        try:
            spybuffer_file = open(args.outputSpybufferFileName)

            spybuffer_array = textToArray(spybuffer_file, HW_OUT_COL_N )
            spybuffer_event_list = arrayToEvents(spybuffer_array, "spybuffer")
            
            spybuffer_file.close()
        except IOError:
            print "Couldn't open " + args.outputSpybufferFileName
            sys.exit()


    if args.inputSpybufferFileName:
        #todo column selection via input arg
        try:
            spybuffer_file = open(args.inputSpybufferFileName)

            spybuffer_array = textToArray(spybuffer_file, TF_COL_N, args.TFMode )
            spybuffer_event_list = arrayToEvents(spybuffer_array, "spybuffer")
            
            spybuffer_file.close()
        except IOError:
            print "Couldn't open " + args.inputSpybufferFileName
            sys.exit()


            
    ftksim_event_list = list()
    if args.ftkSimFileName != "":
        try:
            ftksim_file = open(args.ftkSimFileName)
            
            ftksim_array = textToArray(ftksim_file, FTKSIM_COL_N )
            ftksim_event_list = arrayToEvents(ftksim_array, "FTKSim")            
            
            ftksim_file.close()
        except IOError:
            print "Couldn't open " + args.ftkSimFileName
            sys.exit()   


    ## Basic info
    print "Found " + str(len(spybuffer_event_list)) + " events in the spybuffer file"
    spybuffer_ntracks = list()
    spybuffer_ntracks_occurance = list()
    for event in spybuffer_event_list:
        spybuffer_ntracks.append(len(event.tracks))
    spybuffer_ntracks_unique = list(set(spybuffer_ntracks))
    for i in spybuffer_ntracks_unique:
        spybuffer_ntracks_occurance.append( spybuffer_ntracks.count(i))
    print "Entries: " + " ".join(map(str,spybuffer_ntracks_occurance))
    print "nTracks: " + " ".join(map(str,spybuffer_ntracks_unique))


    print "Found " + str(len(ftksim_event_list)) + " events in the ftksim file"
    ftksim_ntracks = list()
    ftksim_ntracks_occurance = list()
    for event in ftksim_event_list:
        ftksim_ntracks.append(len(event.tracks))
    ftksim_ntracks_unique = list(set(ftksim_ntracks))
    for i in ftksim_ntracks_unique:
        ftksim_ntracks_occurance.append( ftksim_ntracks.count(i))
    print "Entries: " + " ".join(map(str,ftksim_ntracks_occurance))
    print "nTracks: " + " ".join(map(str,ftksim_ntracks_unique))


        





    if args.validate:
        # spyevents
        print "Found " + str(len(spybuffer_event_list)) + " events in spybuffer"
        for i, evnt in enumerate(spybuffer_event_list):
            # the last event might be cut off, so skip it
            if (i == len(spybuffer_event_list) -1): continue

            if not evnt.validate():
                print "Event "+str(i)+" is bad"
                #evnt.Print()

        # ftksim event
        print "Found " + str(len(ftksim_event_list)) + " events in ftksim input"
        for i, evnt in enumerate(ftksim_event_list):
            if not evnt.validate():
                print "Event "+str(i)+" is bad"
                #evnt.Print()

    # todo make the event number a cmd line arg
    if args.compareNew:
        compareNew(spybuffer_event_list[0], ftksim_event_list[0], args.doubleBitWidth) 

    if args.printBuffer:
        compare(spybuffer_event_list[0], spybuffer_event_list[0], args.doubleBitWidth)  # dumb hack for now TODO (only looks at one event)


        
        print result_txt

    

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Parse SSB spybuffers")


    parser.add_argument('-i', '--inputSpybufferFileName', type=str, default="", help="Input spybuffer file")
    parser.add_argument('-o', '--outputSpybufferFileName', type=str, default="", help="Input spybuffer file")
    parser.add_argument('-f', '--ftkSimFileName', type=str, default="", help="FTK sim file")
    parser.add_argument('-V', '--validate', help="Run some validation checks on the ftksim and spybuffer events", action="store_true")
    parser.add_argument('-cN', '--compareNew', help="Compare spybuffer to ftksim", action="store_true")
    parser.add_argument('-TF', '--TFMode', help="TF spy mode, strip lowest 16bits", action="store_true")
    parser.add_argument('-d', '--doubleBitWidth', help="Turn on 32bit width comparisons, e.g. for input spybuffers", action="store_true")
    parser.add_argument('-p', '--printBuffer', help="Print", action="store_true")


    args = parser.parse_args()
    main(args)

    print "I'm finished"
