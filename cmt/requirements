###############################
package ssb
author  Lulu.Liu@cern.ch,Nicoletta.Garelli@cern.ch
manager Lulu.Liu@cern.ch,Nicoletta.Garelli@cern.ch
###############################

###########################
# Dependencies
###########################
use ftkvme
use ftkcommon
use TDAQPolicy
use vme_rcc
use Boost * TDAQCExternal
use ROOT  * DQMCExternal
use dal
use genconfig
use DFConfiguration
use is
use ers
use oh
#use eformat
#use EventStorage  
###########################


###########################################################
# Compilation macros
###########################################################
# Examples:
macro app_libs          "-lboost_program_options-$(boost_libsuffix) -lboost_iostreams-$(boost_libsuffix) -lftkvme -lers -ldaq-df-dal -lftkcommon"
macro app_cflags        "-DSTANDALONE "
macro tdaq_libs         "-lvme_rcc -leformat  -loh -lis -lers -lDataReader -lrcc_time_stamp "


###########################################################
# DAL generation
#   - generation of C++ OKS interface from xml schema file
###########################################################
macro     generate-config-include-dirs  "${TDAQ_INST_PATH}/share/data/dal \ 
	  				 ${TDAQ_INST_PATH}/share/data/DFConfiguration/"

document  generate-config ssb-dal-src   -s=../schema/     \
                                        namespace="daq::ftk::dal"   \
                                        include="ssb/dal"  \
                                        Readout_Ssb.schema.xml"

library	ssb-dal				$(lib_opts) $(bin)ssb-dal-src.tmp/*.cpp
macro	ssb-dal_dependencies		ssb-dal-src
macro	ssb-dal_shlibflags		"-ldaq-df-dal "




###########################################################
# IS generation
#   - generation of C++ IS interface from xml schema file
###########################################################
macro gisdir $(bin)/ssb/dal
action          is_generation_cn    "mkdir -p $(gisdir); is_generator.sh -cpp  -d $(gisdir) -n -p ftk ../schema/ssb_is.schema.xml"


######
public
######

#### Create temporary file containing a string (named cmtversion) that is build using 
#### svn info command. The string can be printed out including version.ixx in the code 
action pkg-version 'echo -n "const char* cmtversion = R\"FtkDelim("  >  version.ixx; \
                    echo "CMT tag: $(version)"                       >> version.ixx; \
                    svn info ..                                      >> version.ixx; \
                    echo ")FtkDelim\";"                              >> version.ixx; '

##########################################################
# ssb library
#   It is supposed to contain ONLY the functions neeeded by higher level 
#   applications and libraries (es: ReadOutModule)
##########################################################
library ssb -suffix=_lib " ssb.cxx \
	    		   ssb_fpga.cxx \
	    		   ssb_fpga_proc.cxx \
	    		   ssb_fpga_hitwarrior.cxx \
    			   ssb_board.cxx \
    			   ssb_const_load_from_txt.cxx \ 
 			   ssb_ssmap_load_from_txt.cxx \ 
                           ssb_octopus_load.cxx \ 
			  "

macro ssb_dependencies   "ssb-dal is_generation_cn ftkcommon ftkvme"
macro ssb_shlibflags     "-lstdc++ -lftkvme -lftkcommon -lohroot -lrdbconfig -loks -lrdb -ldaq-df-dal "
macro ssb_cppflags       "-DWORD64BIT"


#####################################################
## ReadoutModuleSsb library
#####################################################
library ReadoutModule_Ssb  " ReadoutModuleSsb.cxx "
macro ReadoutModule_Ssb_dependencies "ssb ssb-dal is_generation_cn pkg-version"
macro ReadoutModule_Ssb_shlibflags   "-lftkvme -lohroot -lssb -ldaq-df-dal -lftkcommon -lssb-dal -lROSCore"
macro ReadoutModule_Ssb_cppflags     " -DWORD64BIT"

##################################################
## Stand-alone applications 
##################################################
application ssb_vme_block_example_main		"ssb_vme_block_example.cxx"
macro ssb_vme_block_example_main_pp_cppflags 	"$(app_cflags)"
macro ssb_vme_block_example_mainlinkopts     	"$(app_libs)"

application ssb_spybuffer_dump_main    		"ssb_spybuffer_dump.cxx"
macro ssb_spybuffer_dump_main_pp_cppflags 	"$(app_cflags)"
macro ssb_spybuffer_dump_mainlinkopts     	"$(app_libs)"

application ssb_const_load_from_txt_app	"ssb_const_load_from_txt_app.cxx"
macro ssb_const_load_from_txt_app_pp_cppflags  "$(app_cflags)"
macro ssb_const_load_from_txt_applinkopts      "$(app_libs) -lssb"
macro ssb_const_load_from_txt_app_dependencies "ssb" 

application ssb_ssmap_load_from_txt_app	"ssb_ssmap_load_from_txt_app.cxx"
macro ssb_ssmap_load_from_txt_app_pp_cppflags  "$(app_cflags)"
macro ssb_ssmap_load_from_txt_applinkopts      "$(app_libs) -lssb"
macro ssb_ssmap_load_from_txt_app_dependencies "ssb" 

application ssb_octopus_load_app	"ssb_octopus_load_app.cxx"
macro ssb_octopus_load_app_pp_cppflags  "$(app_cflags)"
macro ssb_octopus_load_applinkopts      "$(app_libs) -lssb"
macro ssb_octopus_load_app_dependencies "ssb" 

application ssb_status_main             "ssb_status_main.cxx"
macro ssb_status_main_pp_cppflags       "$(app_cflags)"
macro ssb_status_mainlinkopts           "$(app_libs)"

application ssb_svfplayer             "ssb_svfplayer.cxx"
macro ssb_svfplayer_pp_cppflags       "$(app_cflags)"
macro ssb_svfplayerlinkopts           "$(app_libs)"

###################################
##  Installation 
###################################

# Disable unused install patterns 
ignore_pattern inst_headers_bin_auto
ignore_pattern inst_docs_auto
ignore_pattern inst_scripts_auto
ignore_pattern inst_idl_auto

# Libraries
apply_pattern   install_libs    files=" libssb-dal.so libssb.so libReadoutModule_Ssb.so"


# Applications
# Example
apply_pattern   install_apps  files= "ssb_vme_block_example_main \
                                      ssb_spybuffer_dump_main \
                                      ssb_const_load_from_txt_app\
				      ssb_ssmap_load_from_txt_app\
				      ssb_octopus_load_app\	      
				      ssb_status_main\
				      ssb_svfplayer"


# Schema: OKS and IS
apply_pattern   install_db_files	name=data \ 
					src_dir=../schema \
					target_dir=schema \
					files="*.schema.xml"


