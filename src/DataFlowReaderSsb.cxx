#include "ssb/DataFlowReaderSsb.h"
#include "ftkcommon/ReadSRFromISVectors.h"
#include "ssb/ssb_vme_addr.h"
#include "ssb/ssb_defines.h"

//OnlineServices to check if running within a partition
#include <ipc/partition.h>
#include <ipc/core.h>


// ers
#include <ers/ers.h>
#include "ftkcommon/exceptions.h"


using namespace std;
using namespace daq::ftk;

/************************************************************/
DataFlowReaderSsb::DataFlowReaderSsb(std::shared_ptr<OHRawProvider<>> ohRawProvider, bool forceIS) 
   : DataFlowReader(ohRawProvider, forceIS)
/************************************************************/
{

    // Read in some variables from OKS
    n_EXTF_FPGAs = 0;        
}


/***************************************************************************************************************/
void DataFlowReaderSsb::init(const string& deviceName, const string& partitionName, const string& isServerName)
/***************************************************************************************************************/
{
  DataFlowReader::init(deviceName, partitionName, isServerName);
  DataFlowReader::name_dataflow();
  m_theSSBSummary = std::make_shared<FtkDataFlowSummarySSBNamed>(m_ipcp,name_dataflow());
  m_theSummary= std::static_pointer_cast<FtkDataFlowSummaryNamed>(m_theSSBSummary);
     
  ERS_LOG("Initializing SSB dataflowsummary: "<<name_dataflow()<<" :pointer");
       
}


/************************************************************/
vector<string> DataFlowReaderSsb::getISVectorNames()
/************************************************************/
{
  //Here you define the list of elements to be read from IS 
  //(should be consistent with what StatusRegisterSsbFactory publishes)
  vector<string> srv_names;

  srv_names.push_back("EXTF0_sr_v");
  srv_names.push_back("EXTF1_sr_v");
  srv_names.push_back("EXTF2_sr_v");
  srv_names.push_back("EXTF3_sr_v");
  srv_names.push_back("SSBHW_sr_v");

  return srv_names;
}



///************************************************************/
//void DataFlowReaderSSb::get_in_link_status(std::vector<int64_t> &srv, u_int sb_entry)
///************************************************************/
//{
//  // EXTF0 QSFP: Input from DF
//    // 12000
//    srv.push_back(readRegister(I1_QSFP    + CHANNEL0 +    sb_entry, 1));
//    srv.push_back(readRegister(I1_QSFP    + CHANNEL1 +    sb_entry, 1));
//    srv.push_back(readRegister(I1_QSFP    + CHANNEL2 +    sb_entry, 1));
//    srv.push_back(readRegister(I1_QSFP    + CHANNEL3 +    sb_entry, 1));
//
//}

//************************************************************/
void DataFlowReaderSsb::getLinkInStatus(std::vector<int64_t>& srv)
/************************************************************/
{
    
    // 0 is ON, 1 is OFF, 2 is Errors
    for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) {


        if (m_enabled_FPGAs[fpga] == true) {
            u_int N_EXTF_Links   = readRegister(SSB_EXTF_LINKS,fpga);  // total links to DFA, DFB, AUXA, AUXB

            int dfa_link  = ( N_EXTF_Links      & 0x1 ? 0 : 1);  // link status to DFA
            int dfb_link  = ( N_EXTF_Links >> 3 & 0x1 ? 0 : 1);  // link status to DFB
            int auxa_link = ( N_EXTF_Links >> 6 & 0x1 ? 0 : 1); // link status to AUXA
            int auxb_link = ( N_EXTF_Links >> 9 & 0x1 ? 0 : 1); // link status to AUXB
            
            dfa_link  = (  N_EXTF_Links >> 2 & 0x1  ? dfa_link   : 2);  // link status to DFA
            dfb_link  = (  N_EXTF_Links >> 5 & 0x1  ? dfb_link   : 2);  // link status to DFB
            auxa_link = (  N_EXTF_Links >> 8 & 0x1  ? auxa_link  : 2); // link status to AUXA
            auxb_link = (  N_EXTF_Links >> 11 & 0x1 ? auxb_link  : 2); // link status to AUXB        

            srv.push_back(dfa_link);
            srv.push_back(dfb_link);
            srv.push_back(auxa_link);
            srv.push_back(auxb_link);
        } else {
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
        }
        
    }
  
}


//************************************************************/
void DataFlowReaderSsb::getLinkOutStatus(std::vector<int64_t>& srv)
/************************************************************/
{
  
  srv.push_back(-1);
  
}

/************************************************************/
void DataFlowReaderSsb::getFifoInBusy(vector<int64_t>& srv)
/************************************************************/
{
  for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) { 
    if (m_enabled_FPGAs[fpga] == true) {
        srv.push_back((readRegister(SSB_EXTF_FULLREG,fpga)     ) & 0x1); // dfa
        srv.push_back((readRegister(SSB_EXTF_FULLREG,fpga) >>1 ) & 0x1); // dfb
        srv.push_back((readRegister(SSB_EXTF_FULLREG,fpga) >>2 ) & 0x1); // auxa
        srv.push_back((readRegister(SSB_EXTF_FULLREG,fpga) >>3 ) & 0x1); // auxb
        srv.push_back((readRegister(SSB_EXTF_FULLREG,fpga) >>8 ) & 0x1); // exp
        srv.push_back((readRegister(SSB_EXTF_FULLREG,fpga) >>9 ) & 0x1); // hw
        
    } else {
        srv.push_back(-1);
        srv.push_back(-1);
        srv.push_back(-1);
        srv.push_back(-1);
        srv.push_back(-1);
        srv.push_back(-1);
    }
  }
}


/************************************************************/
void DataFlowReaderSsb::getFifoOutBusy(vector<int64_t>& srv)
/************************************************************/
{
  srv.push_back((readRegister(SSB_HW_FULLREG,4)) & 0x1); // Hitwarrior
}

//************************************************************/
void DataFlowReaderSsb::getFifoInBusyCumulative( vector<int64_t>& srv)
/************************************************************/
{
  
   for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) { 
     if (m_enabled_FPGAs[fpga] == true) {
         srv.push_back((readRegister(SSB_EXTF_COUNTBPDFA,fpga)));
         srv.push_back((readRegister(SSB_EXTF_COUNTBPDFB,fpga)));
         srv.push_back((readRegister(SSB_EXTF_COUNTBPAUXA,fpga)));
         srv.push_back((readRegister(SSB_EXTF_COUNTBPAUXB,fpga)));
         srv.push_back((readRegister(SSB_EXTF_COUNTBPEXP,fpga)));
         srv.push_back((readRegister(SSB_EXTF_COUNTBPHW,fpga)));
     } else {
         srv.push_back(-1 );
         srv.push_back(-1 );
         srv.push_back(-1 );
         srv.push_back(-1 );
         srv.push_back(-1 );
         srv.push_back(-1 );
     }
  }
  
}


void DataFlowReaderSsb::getFifoOutBusyCumulative(vector<int64_t>& srv)
/************************************************************/
{  

    uint32_t busyC= readRegister(SSB_HW_COUNTBP,4) - 0xfbc00000;

    srv.push_back(busyC);
}

/************************************************************/
void DataFlowReaderSsb::getFifoInBusyFraction(std::vector<float>& srv, uint32_t option)
/************************************************************/
{

   for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) { 
     if (m_enabled_FPGAs[fpga] == true) {
         uint32_t freq = (m_EXTF_Freq->at(fpga) != 0 ? m_EXTF_Freq->at(fpga) : 1);

         srv.push_back(float(readRegister(SSB_EXTF_DFABPFRAC,fpga))  / ( (*m_OldFW)[fpga] ? SSB_EXTF_BP_DIVISOR : freq) );
         srv.push_back(float(readRegister(SSB_EXTF_DFBBPFRAC,fpga))  / ( (*m_OldFW)[fpga] ? SSB_EXTF_BP_DIVISOR : freq) );
         srv.push_back(float(readRegister(SSB_EXTF_AUXABPFRAC,fpga)) / ( (*m_OldFW)[fpga] ? SSB_EXTF_BP_DIVISOR : freq) );
         srv.push_back(float(readRegister(SSB_EXTF_AUXBBPFRAC,fpga)) / ( (*m_OldFW)[fpga] ? SSB_EXTF_BP_DIVISOR : freq) );
     } else {
         srv.push_back(-1 );
         srv.push_back(-1 );
         srv.push_back(-1 );
         srv.push_back(-1 );
     }
  }


}

/************************************************************/
void DataFlowReaderSsb::getFifoOutBusyFraction(std::vector<float>& srv, uint32_t option)
/************************************************************/
{
    srv.push_back(float(readRegister(SSB_HW_FLIC_BPFRAC, 4)) / SSB_HW_BP_DIVISOR);    
}

/************************************************************/
void DataFlowReaderSsb::getFifoInEmptyFraction(std::vector<float>& srv, uint32_t option)
/************************************************************/
{
    for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) {
        if (m_enabled_FPGAs[fpga] == true && m_SRSBs.size()!=0) {
    
            if (!m_SRSBs[fpga]) continue;
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::stream_fifo_datacount1) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::stream_fifo_datacount2) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::stream_fifo_datacount3) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::stream_fifo_datacount4) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::df_module_id_ibl__datacount) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::df_module_id_sct1__datacount) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::df_module_id_sct2__datacount) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::df_ibl_hit_col_coord__datacount) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::df_ibl_hit_row_coord__datacount) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::df_sct_hit1_coord__datacount) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::df_sct_hit2_coord__datacount) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::aux_module_id_ibl__datacount) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::aux_module_id_sct__datacount) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::aux_ibl_hit_col_coord__datacount) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::aux_ibl_hit_row_coord__datacount) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::aux_sct_hit_coord__datacount) );
            
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::TF_outfifo_datacount) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::exmem_nom_sector_data_count) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::exmem_pix_sector_data_count) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::exmem_sct_sector_data_count) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::exmem_trans_data_count) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::TF_header_fifo_datacount) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::TF_trailer_fifo_datacount) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::TF_nom_count_datacount) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::TF_pix_count_datacount) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::TF_sct_count_datacount) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::ssid_fifo_datacount) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::hcm_fifo_datacount) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount0) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount1) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount2) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount3) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount4) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount5) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount6) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount7) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount8) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount9) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount10) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount11) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount12) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount13) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount14) );
            srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_extf::hlm_fifo_datacount15) );
        } else {
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);


        }
    }

    

}


/************************************************************/
void DataFlowReaderSsb::getFifoOutEmptyFraction(std::vector<float>& srv, uint32_t option)
/************************************************************/
{
    int fpga = 4; 
    srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_hw::extf0_fifo) );
    srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_hw::extf1_fifo) );
    srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_hw::extf2_fifo) );
    srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_hw::extf3_fifo) );
    srv.push_back(m_SRSBs[fpga]->getStatus<float>(ssb_srsb_contents_hw::flic_fifo) );
}


/************************************************************/
void DataFlowReaderSsb::getL1id(vector<int64_t>& srv)
/************************************************************/
{


    // Do we want to publish a dummy value for non enabled fpgas? maybe not since lvl1id can scan all of the values
    for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) { 
        if (m_enabled_FPGAs[fpga] == true) {
            srv.push_back((readRegister(SSB_LVLID_DFA, fpga)));
            srv.push_back((readRegister(SSB_LVLID_DFB, fpga)));
            srv.push_back((readRegister(SSB_LVLID_AUXA, fpga)));
            srv.push_back((readRegister(SSB_LVLID_AUXB, fpga)));
        } else {
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
        }        
    }

    srv.push_back((readRegister(SSB_HW_SE_PREV_LVL1ID,4))); 
}

/************************************************************/
void DataFlowReaderSsb::getL1id_error(vector<int64_t>& srv)
/************************************************************/
{

    for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) {
        if (m_enabled_FPGAs[fpga] == true) {
            srv.push_back((readRegister(SSB_ERR_LVLID_DFA, fpga)));
            srv.push_back((readRegister(SSB_ERR_LVLID_DFB, fpga)));
            srv.push_back((readRegister(SSB_ERR_LVLID_AUXA, fpga)));
            srv.push_back((readRegister(SSB_ERR_LVLID_AUXB, fpga)));
        } else {
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
        }
    }

    srv.push_back((readRegister(SSB_HW_SE_ERR_COUNTER_LVL1ID_0,4)));
    srv.push_back((readRegister(SSB_HW_SE_ERR_COUNTER_LVL1ID_1,4)));
    srv.push_back((readRegister(SSB_HW_SE_ERR_COUNTER_LVL1ID_2,4)));
    srv.push_back((readRegister(SSB_HW_SE_ERR_COUNTER_LVL1ID_3,4)));


}

/************************************************************/
void  DataFlowReaderSsb::getEventRate(std::vector<float>& srv)
/************************************************************/
{
    for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) {
        if (m_enabled_FPGAs[fpga] == true) srv.push_back( float(readRegister(SSB_EXTF_RATEIN , fpga)) / SSB_EXTF_RATE_DENOMINATOR  ); 
        else                               srv.push_back( -1 ); 
    }

    srv.push_back( float(readRegister(SSB_HW_EVENT_RATE , 4)) / SSB_HW_RATE_DENOMINATOR );

}


/************************************************************/
void DataFlowReaderSsb::getNEventsIn(vector<int64_t>& srv)
/************************************************************/
{
    for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) { 
        if (m_enabled_FPGAs[fpga] == true) srv.push_back((readRegister(SSB_EXTF_EVENTSIN, fpga)));
        else                               srv.push_back( -1 );
    }
}


/************************************************************/
void DataFlowReaderSsb::getNTracksOut(vector<int64_t>& srv)
/************************************************************/
{  

  for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) { 
      if (m_enabled_FPGAs[fpga] == true) {
          srv.push_back((readRegister(SSB_EXTF_TRACKSNOM,fpga)));
          srv.push_back((readRegister(SSB_EXTF_TRACKSPIX,fpga)));
          srv.push_back((readRegister(SSB_EXTF_TRACKSSCT,fpga)));
      } else {
          srv.push_back(-1);
          srv.push_back(-1);
          srv.push_back(-1);          
      }
  }

  srv.push_back((readRegister(SSB_HW_TRACKSOUT, 4)));

}

/************************************************************/
void DataFlowReaderSsb::getNTracksOutRecovery(vector<int64_t>& srv)
/************************************************************/
{  

  for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) { 
      if (m_enabled_FPGAs[fpga] == true) {
          srv.push_back(m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::Count_SelRecIBL));
          srv.push_back(m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::Count_SelRecSCT1));
          srv.push_back(m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::Count_SelRecSCT2));
          srv.push_back(m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::Count_SelRecSCT3));
      } else {
          srv.push_back(-1);
          srv.push_back(-1);
          srv.push_back(-1);          
          srv.push_back(-1);   
      }
  }

}


/************************************************************/
void DataFlowReaderSsb::getTrackRate(vector<float>& srv)
/************************************************************/
{

  for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) { 
      if (m_enabled_FPGAs[fpga] == true) {
          srv.push_back( float(readRegister(SSB_EXTF_RATENOM,fpga))/ SSB_EXTF_RATE_DENOMINATOR);
          srv.push_back( float(readRegister(SSB_EXTF_RATEPIX,fpga))/ SSB_EXTF_RATE_DENOMINATOR);
          srv.push_back( float(readRegister(SSB_EXTF_RATESCT,fpga))/ SSB_EXTF_RATE_DENOMINATOR);
      } else {
          srv.push_back(-1);
          srv.push_back(-1);
          srv.push_back(-1);          
      }
  }

  srv.push_back( float(readRegister(SSB_HW_TRACK_RATE, 4)) / SSB_HW_RATE_DENOMINATOR );


}

/************************************************************/
void DataFlowReaderSsb::getHWTemperature(vector<float> &srv)
/************************************************************/
{
    srv.push_back( 503.975* readRegister(SSB_HW_TEMPERATURE,4)/4096 - 273.15 ); 
}

/************************************************************/
void DataFlowReaderSsb::getHWVCCINT(vector<float> &srv)
/************************************************************/
{
    srv.push_back( 3.*  readRegister(SSB_HW_VCCINT,4)/4096 );
}

/************************************************************/
void DataFlowReaderSsb::getHWVCCAUX(vector<float> &srv)
/************************************************************/
{
   srv.push_back( 3.*  readRegister(SSB_HW_VCCAUX,4)/4096 );
}

/************************************************************/
void DataFlowReaderSsb::getHWVCCBRAM(vector<float> &srv)
/************************************************************/
{
    srv.push_back( 3.*  readRegister(SSB_HW_VCCBRAM,4)/4096 );
}

/************************************************************/
void DataFlowReaderSsb::getHWVPVN(vector<float> &srv)
/************************************************************/
{
    srv.push_back( 3.*  readRegister(SSB_HW_VPVN,4)/4096 );
}

/************************************************************/
void DataFlowReaderSsb::getHWVREFP(vector<float> &srv)
/************************************************************/
{
    srv.push_back( 3.*  readRegister(SSB_HW_VREFP,4)/4096 );
}

/************************************************************/
void DataFlowReaderSsb::getHWVREFN(vector<float> &srv)
/************************************************************/
{
    srv.push_back( 3.*  readRegister(SSB_HW_VREFN,4)/4096 );
}

/************************************************************/
void DataFlowReaderSsb::getFpgaTemperature(vector<int64_t>& srv)  
/************************************************************/
{
    
  for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) { 
      if (m_enabled_FPGAs[fpga] == true) srv.push_back( (503.975 *  (readRegister(SSB_EXTF_TEMPERATURE,fpga)) / 4096 ) - 273.15);
      else srv.push_back(-1);
  }
      srv.push_back( (503.975 *  readRegister(SSB_HW_TEMPERATURE,4) / 4096 ) - 273.15);

}

/************************************************************/
void DataFlowReaderSsb::getNEventsProcessed(vector<int64_t>& srv)
/************************************************************/
{
  srv.push_back((readRegister(SSB_HW_EVENTSOUT, 4)));
}


/************************************************************/
void DataFlowReaderSsb::getLinkErrorsCount(vector<int64_t>& srv)  
/************************************************************/
{
  for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) { 
      if (m_enabled_FPGAs[fpga] == true) {
          u_int N_EXTF_DF_LINK_DISPARITY_ERRORS  = readRegister(SSB_EXTF_DF_LINK_DISPARITY_ERRORS,fpga  );
          u_int N_EXTF_DFA_LINK_DISPARITY_ERRORS = (N_EXTF_DF_LINK_DISPARITY_ERRORS & 0xFFFF0000) >> 16;
          u_int N_EXTF_DFB_LINK_DISPARITY_ERRORS = (N_EXTF_DF_LINK_DISPARITY_ERRORS & 0xFFFF);
              
          u_int N_EXTF_AUX_LINK_DISPARITY_ERRORS  = readRegister( SSB_EXTF_AUX_LINK_DISPARITY_ERRORS,fpga  );
          u_int N_EXTF_AUXA_LINK_DISPARITY_ERRORS = (N_EXTF_AUX_LINK_DISPARITY_ERRORS & 0xFFFF0000) >> 16;
          u_int N_EXTF_AUXB_LINK_DISPARITY_ERRORS = (N_EXTF_AUX_LINK_DISPARITY_ERRORS & 0xFFFF);
              
          u_int N_EXTF_DF_LINK_NONINTABLE_ERRORS  = readRegister( SSB_EXTF_DF_LINK_NONINTABLE_ERRORS,fpga );
          u_int N_EXTF_DFA_LINK_NONINTABLE_ERRORS = (N_EXTF_DF_LINK_NONINTABLE_ERRORS & 0xFFFF0000) >> 16;
          u_int N_EXTF_DFB_LINK_NONINTABLE_ERRORS = (N_EXTF_DF_LINK_NONINTABLE_ERRORS & 0xFFFF);
              
          u_int N_EXTF_AUX_LINK_NONINTABLE_ERRORS  = readRegister( SSB_EXTF_AUX_LINK_NONINTABLE_ERRORS,fpga );
          u_int N_EXTF_AUXA_LINK_NONINTABLE_ERRORS = (N_EXTF_AUX_LINK_NONINTABLE_ERRORS & 0xFFFF0000) >> 16;
          u_int N_EXTF_AUXB_LINK_NONINTABLE_ERRORS = (N_EXTF_AUX_LINK_NONINTABLE_ERRORS & 0xFFFF);
          
          // Disparity
          srv.push_back(N_EXTF_DFA_LINK_DISPARITY_ERRORS );
          if (m_enabled_Channels[2*fpga]) srv.push_back(N_EXTF_DFB_LINK_DISPARITY_ERRORS );
          else                            srv.push_back(-1 );
          srv.push_back(N_EXTF_AUXA_LINK_DISPARITY_ERRORS );
          if (m_enabled_Channels[2*fpga + 1]) srv.push_back(N_EXTF_AUXB_LINK_DISPARITY_ERRORS );
          else                                srv.push_back(-1);

          // 8b10b encoding errors
          srv.push_back(N_EXTF_DFA_LINK_NONINTABLE_ERRORS );
          if (m_enabled_Channels[2*fpga]) srv.push_back(N_EXTF_DFB_LINK_NONINTABLE_ERRORS );
          else                            srv.push_back(-1 );          
          srv.push_back(N_EXTF_AUXA_LINK_NONINTABLE_ERRORS );
          if (m_enabled_Channels[2*fpga + 1]) srv.push_back(N_EXTF_AUXB_LINK_NONINTABLE_ERRORS );
          else                                srv.push_back(-1);

      } else {
          srv.push_back(-1);
          srv.push_back(-1);
          srv.push_back(-1);
          srv.push_back(-1);

          srv.push_back(-1);
          srv.push_back(-1);
          srv.push_back(-1);
          srv.push_back(-1);
      }
  }

}

/************************************************************/
void DataFlowReaderSsb::getEXTFResets(vector<int64_t>& srv)  
/************************************************************/
{
  for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) { 
      if (m_enabled_FPGAs[fpga] == true) {
          srv.push_back((readRegister(SSB_EXTF_RESET, fpga)));
      }else {
          srv.push_back(-1);
      }
  }
}


/************************************************************/
void DataFlowReaderSsb::getNEventsProcSyncEngine(vector<int64_t>& srv)
/************************************************************/
{
    for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) { 
        if (m_enabled_FPGAs[fpga] == true) {
            srv.push_back((readRegister(SSB_EXTF_SYNC_PROC_EVENTS_COUNT_DFA, fpga)));
            srv.push_back((readRegister(SSB_EXTF_SYNC_PROC_EVENTS_COUNT_DFB, fpga)));
            srv.push_back((readRegister(SSB_EXTF_SYNC_PROC_EVENTS_COUNT_AUXA, fpga)));
            srv.push_back((readRegister(SSB_EXTF_SYNC_PROC_EVENTS_COUNT_AUXB, fpga)));            
        }
        else{
            srv.push_back( -1 );
            srv.push_back( -1 );
            srv.push_back( -1 );
            srv.push_back( -1 );
        }
    }

}

/************************************************************/
void DataFlowReaderSsb::getNPacketsDiscardedSync(vector<int64_t>& srv)
/************************************************************/
{
    for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) { 
        if (m_enabled_FPGAs[fpga] == true) {
            srv.push_back((readRegister(SSB_EXTF_SYNC_DISC_EVENTS_COUNT_DFA, fpga)));
            srv.push_back((readRegister(SSB_EXTF_SYNC_DISC_EVENTS_COUNT_DFB, fpga)));
            srv.push_back((readRegister(SSB_EXTF_SYNC_DISC_EVENTS_COUNT_AUXA, fpga)));
            srv.push_back((readRegister(SSB_EXTF_SYNC_DISC_EVENTS_COUNT_AUXB, fpga)));            
        }
        else{
            srv.push_back( -1 );
            srv.push_back( -1 );
            srv.push_back( -1 );
            srv.push_back( -1 );
        }
    }

}


/************************************************************/
void DataFlowReaderSsb::getPacketCompletionTime(std::vector<int64_t>& srv)
/************************************************************/
{
    srv.push_back( -1 );
}

void DataFlowReaderSsb::getFifoInOverflowFlag(std::vector<int64_t>& srv)
/************************************************************/
{
    for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) {
        if (m_enabled_FPGAs[fpga] == true && m_SRSBs.size()!=0 && m_SRSBs[fpga]) {

            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::stream_fifo_datacount1) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::stream_fifo_datacount2) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::stream_fifo_datacount3) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::stream_fifo_datacount4) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::df_module_id_ibl__datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::df_module_id_sct1__datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::df_module_id_sct2__datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::df_ibl_hit_col_coord__datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::df_ibl_hit_row_coord__datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::df_sct_hit1_coord__datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::df_sct_hit2_coord__datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::aux_module_id_ibl__datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::aux_module_id_sct__datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::aux_ibl_hit_col_coord__datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::aux_ibl_hit_row_coord__datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::aux_sct_hit_coord__datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::TF_outfifo_datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::exmem_nom_sector_data_count) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::exmem_pix_sector_data_count) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::exmem_sct_sector_data_count) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::exmem_trans_data_count) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::TF_header_fifo_datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::TF_trailer_fifo_datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::TF_nom_count_datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::TF_pix_count_datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::TF_sct_count_datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::ssid_fifo_datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hcm_fifo_datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount0) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount1) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount2) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount3) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount4) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount5) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount6) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount7) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount8) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount9) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount10) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount11) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount12) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount13) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount14) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount15) ) );



        } else {
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);

        }
    }

}


void DataFlowReaderSsb::getFifoInUnderflowFlag(std::vector<int64_t>& srv)
/************************************************************/
{
    for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) {
        if (m_enabled_FPGAs[fpga] == true && m_SRSBs.size()!=0 && m_SRSBs[fpga]) {

            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::stream_fifo_datacount1) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::stream_fifo_datacount2) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::stream_fifo_datacount3) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::stream_fifo_datacount4) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::df_module_id_ibl__datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::df_module_id_sct1__datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::df_module_id_sct2__datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::df_ibl_hit_col_coord__datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::df_ibl_hit_row_coord__datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::df_sct_hit1_coord__datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::df_sct_hit2_coord__datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::aux_module_id_ibl__datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::aux_module_id_sct__datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::aux_ibl_hit_col_coord__datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::aux_ibl_hit_row_coord__datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::aux_sct_hit_coord__datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::TF_outfifo_datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::exmem_nom_sector_data_count) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::exmem_pix_sector_data_count) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::exmem_sct_sector_data_count) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::exmem_trans_data_count) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::TF_header_fifo_datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::TF_trailer_fifo_datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::TF_nom_count_datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::TF_pix_count_datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::TF_sct_count_datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::ssid_fifo_datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hcm_fifo_datacount) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount0) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount1) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount2) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount3) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount4) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount5) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount6) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount7) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount8) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount9) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount10) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount11) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount12) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount13) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount14) ) );
            srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hlm_fifo_datacount15) ) );

        } else {
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
            srv.push_back(-1);
        }
    }

}




/************************************************************/
void DataFlowReaderSsb::getFifoOutOverflowFlag(std::vector<int64_t>& srv)
/************************************************************/
{
    int fpga = 4; 
    srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_hw::extf0_fifo) ) );
    srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_hw::extf1_fifo) ) );
    srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_hw::extf2_fifo) ) );
    srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_hw::extf3_fifo) ) );
    srv.push_back( bool(SSB_SRSB_FIFO_OVERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_hw::flic_fifo) ) );
}

/************************************************************/
void DataFlowReaderSsb::getFifoOutUnderflowFlag(std::vector<int64_t>& srv)
/************************************************************/
{
    int fpga = 4; 
    srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_hw::extf0_fifo) ) );
    srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_hw::extf1_fifo) ) );
    srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_hw::extf2_fifo) ) );
    srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_hw::extf3_fifo) ) );
    srv.push_back( bool(SSB_SRSB_FIFO_UNDERFLOW_MASK & m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_hw::flic_fifo) ) );


}

/************************************************************/
void DataFlowReaderSsb::getMaxEvtProcessingTime(std::vector<int64_t>& srv)
/************************************************************/
{

    for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) {
        if (m_enabled_FPGAs[fpga] == true && m_SRSBs.size()!=0 && m_SRSBs[fpga]) {
            srv.push_back( m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::processing_time_max_r) );
            
        } else {
            srv.push_back( -1 );
        }
    }
    srv.push_back( m_SRSBs[4]->getStatusRaw(ssb_srsb_contents_hw::processing_time_max) );
}

/************************************************************/
void DataFlowReaderSsb::getMinEvtProcessingTime(std::vector<int64_t>& srv)
/************************************************************/
{

    for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) {
        if (m_enabled_FPGAs[fpga] == true && m_SRSBs.size()!=0 && m_SRSBs[fpga]) {
            srv.push_back( m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::processing_time_min_r) );
            
        } else {
            srv.push_back( -1 );
        }
    }
    srv.push_back( m_SRSBs[4]->getStatusRaw(ssb_srsb_contents_hw::processing_time_min) );
}

/************************************************************/
void DataFlowReaderSsb::getIBLHitRate(std::vector<float>& srv)
/************************************************************/
{

    for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) {
        if (m_enabled_FPGAs[fpga] == true && m_SRSBs.size()!=0 && m_SRSBs[fpga]) {

            srv.push_back( float(m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hxmpp_hlm_IBL_hitrate)) / SSB_EXTF_RATE_DENOMINATOR );
            
        } else {
            srv.push_back( -1 );
        }
    }
}

/************************************************************/
void DataFlowReaderSsb::getSCT5HitRate(std::vector<float>& srv)
/************************************************************/
{

    for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) {
        if (m_enabled_FPGAs[fpga] == true && m_SRSBs.size()!=0 && m_SRSBs[fpga]) {

            srv.push_back( float(m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hxmpp_hlm_SCT1_hitrate))/SSB_EXTF_RATE_DENOMINATOR );
            
        } else {
            srv.push_back( -1 );
        }
    }
}


/************************************************************/
void DataFlowReaderSsb::getSCT7HitRate(std::vector<float>& srv)
/************************************************************/
{

    for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) {
        if (m_enabled_FPGAs[fpga] == true && m_SRSBs.size()!=0 && m_SRSBs[fpga]) {

            srv.push_back( float(m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hxmpp_hlm_SCT2_hitrate))/SSB_EXTF_RATE_DENOMINATOR );
            
        } else {
            srv.push_back( -1 );
        }
    }
}


/************************************************************/
void DataFlowReaderSsb::getSCT11HitRate(std::vector<float>& srv)
/************************************************************/
{

    for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) {
        if (m_enabled_FPGAs[fpga] == true && m_SRSBs.size()!=0 && m_SRSBs[fpga]) {

            srv.push_back( float(m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::hxmpp_hlm_SCT3_hitrate))/SSB_EXTF_RATE_DENOMINATOR );
            
        } else {
            srv.push_back( -1 );
        }
    }
}





/************************************************************/
void DataFlowReaderSsb::getMaxBackPressureTime(std::vector<int64_t>& srv)
/************************************************************/
{
    for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) {
        if (m_enabled_FPGAs[fpga] == true && m_SRSBs.size()!=0 && m_SRSBs[fpga]) {

            srv.push_back( m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::max_BP_dfa) );
            srv.push_back( m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::max_BP_dfb) );
            srv.push_back( m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::max_BP_auxa) );
            srv.push_back( m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::max_BP_auxb) );

        } else {
            srv.push_back( -1 );
            srv.push_back( -1 );
            srv.push_back( -1 );
            srv.push_back( -1 );
        }
    }

}

/************************************************************/
void DataFlowReaderSsb::getFreezeCount(std::vector<int64_t>& srv)
/************************************************************/
{
    srv.push_back( -1 );
}

/************************************************************/
void DataFlowReaderSsb::getErrorCount(std::vector<int64_t>& srv)
/************************************************************/
{
    srv.push_back( -1 );
}

/************************************************************/
void DataFlowReaderSsb::getDataTruncationCount(std::vector<int64_t>& srv)
/************************************************************/
{
    srv.push_back( -1 );
}



/************************************************************/
void DataFlowReaderSsb::getEXPState(vector<int64_t>& srv)
/************************************************************/
{
    for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) { 
        if (m_enabled_FPGAs[fpga] == true) {
            u_int state = readRegister(SSB_EXTF_EXP_STATE, fpga);

            srv.push_back( (state >> 0) & 0xfff );
            srv.push_back( (state >> 12) & 0xf );
            srv.push_back( (state >> 16) & 0xf );
            srv.push_back( (state >> 20) & 0xf );            
            srv.push_back( (state >> 24) & 0x3f );            
        }
        else{
            srv.push_back( -1 );
            srv.push_back( -1 );
            srv.push_back( -1 );
            srv.push_back( -1 );
            srv.push_back( -1 );
        }
    }

}

/************************************************************/
void DataFlowReaderSsb::getReadyState(vector<int64_t>& srv)
/************************************************************/
{
    for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) { 
        if (m_enabled_FPGAs[fpga] == true) {
            u_int state = readRegister(SSB_EXTF_READY, fpga);

            srv.push_back( (state >> 0) & 0x1 );
            srv.push_back( (state >> 1) & 0x1 );
            srv.push_back( (state >> 2) & 0x1 );
            srv.push_back( (state >> 3) & 0x1 );            
        }
        else{
            srv.push_back( -1 );
            srv.push_back( -1 );
            srv.push_back( -1 );
            srv.push_back( -1 );
        }
    }

}


/************************************************************/
void DataFlowReaderSsb::getMasterState(vector<int64_t>& srv)
/************************************************************/
{
    for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) { 
        if (m_enabled_FPGAs[fpga] == true) {
            u_int state = readRegister(SSB_EXTF_MASTER_STATE, fpga);

            srv.push_back( (state >> 0) & 0x1 );
            srv.push_back( (state >> 1) & 0x1 );
            srv.push_back( (state >> 2) & 0x1 );
            srv.push_back( (state >> 3) & 0x1 );            
            srv.push_back( (state >> 4) & 0x1 );            
            srv.push_back( (state >> 5) & 0x1 );            
            srv.push_back( (state >> 6) & 0x1 );            
            srv.push_back( (state >> 7) & 0x1 );            
        }
        else{
            srv.push_back( -1 );
            srv.push_back( -1 );
            srv.push_back( -1 );
            srv.push_back( -1 );
            srv.push_back( -1 );
            srv.push_back( -1 );
            srv.push_back( -1 );
            srv.push_back( -1 );
        }
    }

}

/************************************************************/
void DataFlowReaderSsb::getFWVersion(vector<int64_t>& srv)
/************************************************************/
{
    // EXTF
    for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) {
        if (m_enabled_FPGAs[fpga] == true) {
            u_int githash = readRegister(SSB_EXTF_FWHASH, fpga);
            u_int date = readRegister(SSB_EXTF_FWDATE, fpga);

            srv.push_back( githash );
            srv.push_back( date );
        }
        else{
            srv.push_back( -1 );
            srv.push_back( -1 );
        }
    }

    // HW
    u_int githash = readRegister(SSB_HW_FWHASH, 4);
    u_int date = readRegister(SSB_HW_FWDATE, 4);
    
    srv.push_back( githash );
    srv.push_back( date  );

}


/************************************************************/
void DataFlowReaderSsb::getMaxModulesPerEvent(vector<int64_t>& srv)
/************************************************************/
{

    for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) {
        if (m_enabled_FPGAs[fpga] == true && m_SRSBs.size()!=0 && m_SRSBs[fpga]) {

            srv.push_back( m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::dfa_max_IBL_mod_per_event) );
            srv.push_back( m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::dfa_max_SCT_mod_per_event) );
            srv.push_back( m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::dfb_max_IBL_mod_per_event) );
            srv.push_back( m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::dfb_max_SCT_mod_per_event) );

        } else {
            srv.push_back( -1 );
            srv.push_back( -1 );
            srv.push_back( -1 );
            srv.push_back( -1 );
        }
    }


}

/************************************************************/
void DataFlowReaderSsb::getModuleRate(std::vector<float>& srv)
/************************************************************/
{
    
    for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) {
        if (m_enabled_FPGAs[fpga] == true && m_SRSBs.size()!=0 && m_SRSBs[fpga]) {

            srv.push_back( float(m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::dfa_IBL_mod_rate)) / SSB_EXTF_RATE_DENOMINATOR );
            srv.push_back( float(m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::dfa_SCT_mod_rate)) / SSB_EXTF_RATE_DENOMINATOR );
            srv.push_back( float(m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::dfb_IBL_mod_rate)) / SSB_EXTF_RATE_DENOMINATOR );
            srv.push_back( float(m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::dfb_SCT_mod_rate)) / SSB_EXTF_RATE_DENOMINATOR );

        } else {
            srv.push_back( -1 );
            srv.push_back( -1 );
            srv.push_back( -1 );
            srv.push_back( -1 );
        }
    }



}

/************************************************************/
void DataFlowReaderSsb::getMaxClustersPerEvent(vector<int64_t>& srv)
/************************************************************/
{

    for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) {
        if (m_enabled_FPGAs[fpga] == true && m_SRSBs.size()!=0 && m_SRSBs[fpga]) {

            srv.push_back( m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::dfa_max_IBL_clusters_per_event) );
            srv.push_back( m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::dfa_max_SCT_clusters_per_event) );
            srv.push_back( m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::dfb_max_IBL_clusters_per_event) );
            srv.push_back( m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::dfb_max_SCT_clusters_per_event) );

        } else {
            srv.push_back( -1 );
            srv.push_back( -1 );
            srv.push_back( -1 );
            srv.push_back( -1 );
        }
    }


}

/************************************************************/
void DataFlowReaderSsb::getClusterRate(std::vector<float>& srv)
/************************************************************/
{

    for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) {
        if (m_enabled_FPGAs[fpga] == true && m_SRSBs.size()!=0 && m_SRSBs[fpga]) {

            srv.push_back( float(m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::dfa_IBL_cluster_rate)) / SSB_EXTF_RATE_DENOMINATOR );
            srv.push_back( float(m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::dfa_SCT_cluster_rate)) / SSB_EXTF_RATE_DENOMINATOR );
            srv.push_back( float(m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::dfb_IBL_cluster_rate)) / SSB_EXTF_RATE_DENOMINATOR );
            srv.push_back( float(m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::dfb_SCT_cluster_rate)) / SSB_EXTF_RATE_DENOMINATOR );

        } else {
            srv.push_back( -1 );
            srv.push_back( -1 );
            srv.push_back( -1 );
            srv.push_back( -1 );
        }
    }


}

/************************************************************/
void DataFlowReaderSsb::getMaxTracksPerEvent(vector<int64_t>& srv)
/************************************************************/
{

    for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) {
        if (m_enabled_FPGAs[fpga] == true && m_SRSBs.size()!=0 && m_SRSBs[fpga]) {
            srv.push_back( m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::track_count_max) );
        } else {
            srv.push_back( -1 );
        }
    }
}



/************************************************************/
void DataFlowReaderSsb::getHitTrackingLimits(vector<int64_t>& srv)
/************************************************************/
{

    for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) {
        if (m_enabled_FPGAs[fpga] == true && m_SRSBs.size()!=0 && m_SRSBs[fpga]) {
            srv.push_back( m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::aux_track_limit_hit_cnt) );
            srv.push_back( m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::tf_track_limit_hit_cnt) );
            srv.push_back( m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::tf_fit_limit_hit_nom_cnt) );
            srv.push_back( m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::tf_fit_limit_hit_pix_cnt) );
            srv.push_back( m_SRSBs[fpga]->getStatusRaw(ssb_srsb_contents_extf::tf_fit_limit_hit_sct_cnt) );
        } else {
            srv.push_back( -1 );
            srv.push_back( -1 );
            srv.push_back( -1 );
            srv.push_back( -1 );
            srv.push_back( -1 );
        }
    }
}





/************************************************************/
void DataFlowReaderSsb::HistoHelper(vector<int64_t>& srv, uint32_t sourceid)
/************************************************************/
{
    // single function for code that gets used a lot

    if (m_histos.size() > 0)
    {
        
        for (auto &it : m_histos) // std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t >
        {
            // Find the spybuffer
            if (it.second == sourceid)
            {
                bool pushedBackSomething = false;
                std::vector<uint32_t> histo = it.first->getBuffer();
                for (uint i = 0; i < histo.size(); i++)
                {
                    if (m_PublishOnlyNonZeroBins && histo[i] > 0)
                    {
                        pushedBackSomething = true;
                        srv.push_back(histo[i]);                     
                    }
                    else srv.push_back(histo[i]); 
                }
                if (m_PublishOnlyNonZeroBins && !pushedBackSomething) srv.push_back(-1); // if everything is 0 push back a -1
                return;
            }
                
        }

        // failed to find spybuffer                
        std::stringstream message;
        SourceIDSpyBuffer sid = daq::ftk::decode_SourceIDSpyBuffer(sourceid);
        std::stringstream fpga;
        if (sid.position == daq::ftk::Position::IN) fpga << "EXTF";
        else fpga << "HW";
        message << "Failed to retrive histogram for " << fpga.str() << " " << std::hex << sourceid;
        daq::ftk::FTKMonitoringError excp(ERS_HERE, name_ftk(), message.str());
        ers::warning(excp);        

        srv.push_back(-1);
        return;
    } else {
        srv.push_back(-1);
        return;
    }


}

/************************************************************/
void DataFlowReaderSsb::getTrackHistogram(vector<int64_t>& srv, int n_fpga)
/************************************************************/
{
    daq::ftk::Position extf_hw = daq::ftk::Position::IN; // EXTF is IN

    uint32_t sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_TRACK_HISTO , extf_hw);


    HistoHelper(srv, sourceid);
}

/************************************************************/
void DataFlowReaderSsb::getAuxHistogram(vector<int64_t>& srv, int n_fpga)
/************************************************************/
{
    daq::ftk::Position extf_hw = daq::ftk::Position::IN; // EXTF is IN

    uint32_t sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_AUX_HISTO , extf_hw);


    HistoHelper(srv, sourceid);

}

/************************************************************/
void DataFlowReaderSsb::getDFHistogram(vector<int64_t>& srv, int n_fpga)
/************************************************************/
{
    daq::ftk::Position extf_hw = daq::ftk::Position::IN; // EXTF is IN

    uint32_t sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_DF_HISTO , extf_hw);


    HistoHelper(srv, sourceid);

}

/************************************************************/
void DataFlowReaderSsb::getSkewHistogram(vector<int64_t>& srv, int n_fpga)
/************************************************************/
{
    daq::ftk::Position extf_hw = daq::ftk::Position::IN; // EXTF is IN

    uint32_t sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_SKEW_HISTO , extf_hw);


    HistoHelper(srv, sourceid);

}

/************************************************************/
void DataFlowReaderSsb::getOutPacketLenHistogram(vector<int64_t>& srv, int n_fpga)
/************************************************************/
{
    daq::ftk::Position extf_hw = daq::ftk::Position::IN; // EXTF is IN

    uint32_t sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_OUTPACKETLEN_HISTO , extf_hw);


    HistoHelper(srv, sourceid);

}

/************************************************************/
void DataFlowReaderSsb::getDFA_PH_TimeoutHistogram(vector<int64_t>& srv, int n_fpga)
/************************************************************/
{
    daq::ftk::Position extf_hw = daq::ftk::Position::IN; // EXTF is IN

    uint32_t sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_DFAPH_TIMEOUT_HISTO , extf_hw);


    HistoHelper(srv, sourceid);

}

/************************************************************/
void DataFlowReaderSsb::getDFB_PH_TimeoutHistogram(vector<int64_t>& srv, int n_fpga)
/************************************************************/
{
    daq::ftk::Position extf_hw = daq::ftk::Position::IN; // EXTF is IN

    uint32_t sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_DFBPH_TIMEOUT_HISTO , extf_hw);


    HistoHelper(srv, sourceid);


}

/************************************************************/
void DataFlowReaderSsb::getAUXA_PH_TimeoutHistogram(vector<int64_t>& srv, int n_fpga)
/************************************************************/
{
    daq::ftk::Position extf_hw = daq::ftk::Position::IN; // EXTF is IN

    uint32_t sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_AUXAPH_TIMEOUT_HISTO , extf_hw);


    HistoHelper(srv, sourceid);

}

/************************************************************/
void DataFlowReaderSsb::getAUXB_PH_TimeoutHistogram(vector<int64_t>& srv, int n_fpga)
/************************************************************/
{
    daq::ftk::Position extf_hw = daq::ftk::Position::IN; // EXTF is IN

    uint32_t sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_AUXBPH_TIMEOUT_HISTO , extf_hw);


    HistoHelper(srv, sourceid);

}

/************************************************************/
void DataFlowReaderSsb::getL1IDSkipHistogram(vector<int64_t>& srv, int n_fpga)
/************************************************************/
{
    daq::ftk::Position extf_hw = daq::ftk::Position::IN; // EXTF is IN

    uint32_t sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_L1ID_SKIP_HISTO , extf_hw);


    HistoHelper(srv, sourceid);

}

/************************************************************/
void DataFlowReaderSsb::getTFFIFO_L1IDSkipHistogram(vector<int64_t>& srv, int n_fpga)
/************************************************************/
{
    daq::ftk::Position extf_hw = daq::ftk::Position::IN; // EXTF is IN

    uint32_t sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_TFFIFOL1ID_SKIP_HISTO , extf_hw);


    HistoHelper(srv, sourceid);

}


/************************************************************/
void DataFlowReaderSsb::getDFAL1IDSkipHistogram(vector<int64_t>& srv, int n_fpga)
/************************************************************/
{
    daq::ftk::Position extf_hw = daq::ftk::Position::IN; // EXTF is IN

    uint32_t sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_DFAL1ID_SKIP_HISTO , extf_hw);


    HistoHelper(srv, sourceid);

}

/************************************************************/
void DataFlowReaderSsb::getDFBL1IDSkipHistogram(vector<int64_t>& srv, int n_fpga)
/************************************************************/
{
    daq::ftk::Position extf_hw = daq::ftk::Position::IN; // EXTF is IN

    uint32_t sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_DFBL1ID_SKIP_HISTO , extf_hw);


    HistoHelper(srv, sourceid);

}

/************************************************************/
void DataFlowReaderSsb::getAUXAL1IDSkipHistogram(vector<int64_t>& srv, int n_fpga)
/************************************************************/
{
    daq::ftk::Position extf_hw = daq::ftk::Position::IN; // EXTF is IN

    uint32_t sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_AUXAL1ID_SKIP_HISTO , extf_hw);


    HistoHelper(srv, sourceid);

}

/************************************************************/
void DataFlowReaderSsb::getAUXBL1IDSkipHistogram(vector<int64_t>& srv, int n_fpga)
/************************************************************/
{
    daq::ftk::Position extf_hw = daq::ftk::Position::IN; // EXTF is IN

    uint32_t sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_AUXBL1ID_SKIP_HISTO , extf_hw);


    HistoHelper(srv, sourceid);

}

/************************************************************/
void DataFlowReaderSsb::getLayerMapHistogram(vector<int64_t>& srv, int n_fpga)
/************************************************************/
{

    daq::ftk::Position extf_hw = daq::ftk::Position::IN; // EXTF is IN

    uint32_t sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_LAYERMAP_HISTO , extf_hw);

    HistoHelper(srv, sourceid);

}

/************************************************************/
void DataFlowReaderSsb::getHitsPerLayerHistogramDF(vector<int64_t>& srv, int n_fpga, int layer)
/************************************************************/
{
    srv.push_back(-1);
}

/************************************************************/
void DataFlowReaderSsb::getHitsPerLayerHistogramEXP(vector<int64_t>& srv, int n_fpga, int layer)
/************************************************************/
{
    srv.push_back(-1);
}

//hitwarrior histos
void DataFlowReaderSsb::getHWL1IDSkipHistogram(vector<int64_t>& srv)
/************************************************************/
{
    daq::ftk::Position extf_hw = daq::ftk::Position::OUT; // HW is OUT
    uint n_fpga = 0;  // since the hw has it's own bit,use 0 to free up address space
    uint32_t sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_HWL1ID_SKIP_HISTO , extf_hw);

    HistoHelper(srv, sourceid);
}

void DataFlowReaderSsb::getHWNTrackHistogram(vector<int64_t>& srv)
/************************************************************/
{
    daq::ftk::Position extf_hw = daq::ftk::Position::OUT; // HW is OUT
    uint n_fpga = 0; // since the hw has it's own bit,use 0 to free up address space 
    uint32_t sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_HW_NTRACK_HISTO , extf_hw);

    HistoHelper(srv, sourceid);
}


void DataFlowReaderSsb::getHWChi2Histogram(vector<int64_t>& srv)
/************************************************************/
{
    daq::ftk::Position extf_hw = daq::ftk::Position::OUT; // HW is OUT
    uint n_fpga = 0; // since the hw has it's own bit,use 0 to free up address space 
    uint32_t sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_HW_CHI2_HISTO , extf_hw);

    HistoHelper(srv, sourceid);
}

void DataFlowReaderSsb::getHWD0Histogram(vector<int64_t>& srv)
/************************************************************/
{
    daq::ftk::Position extf_hw = daq::ftk::Position::OUT; // HW is OUT
    uint n_fpga = 0; // since the hw has it's own bit,use 0 to free up address space 
    uint32_t sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_HW_D0_HISTO , extf_hw);

    HistoHelper(srv, sourceid);
}

void DataFlowReaderSsb::getHWZ0Histogram(vector<int64_t>& srv)
/************************************************************/
{
    daq::ftk::Position extf_hw = daq::ftk::Position::OUT; // HW is OUT
    uint n_fpga = 0; // since the hw has it's own bit,use 0 to free up address space 
    uint32_t sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_HW_Z0_HISTO , extf_hw);

    HistoHelper(srv, sourceid);
}


void DataFlowReaderSsb::getHWCotthHistogram(vector<int64_t>& srv)
/************************************************************/
{
    daq::ftk::Position extf_hw = daq::ftk::Position::OUT; // HW is OUT
    uint n_fpga = 0; // since the hw has it's own bit,use 0 to free up address space 
    uint32_t sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_HW_COTTH_HISTO , extf_hw);

    HistoHelper(srv, sourceid);
}

void DataFlowReaderSsb::getHWPhi0Histogram(vector<int64_t>& srv)
/************************************************************/
{
    daq::ftk::Position extf_hw = daq::ftk::Position::OUT; // HW is OUT
    uint n_fpga = 0; // since the hw has it's own bit,use 0 to free up address space 
    uint32_t sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_HW_PHI0_HISTO , extf_hw);

    HistoHelper(srv, sourceid);
}

void DataFlowReaderSsb::getHWCurvHistogram(vector<int64_t>& srv)
/************************************************************/
{
    daq::ftk::Position extf_hw = daq::ftk::Position::OUT; // HW is OUT
    uint n_fpga = 0; // since the hw has it's own bit,use 0 to free up address space 
    uint32_t sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_HW_CURV_HISTO , extf_hw);

    HistoHelper(srv, sourceid);
}

void DataFlowReaderSsb::getHWLayerMapHistogram(vector<int64_t>& srv)
/************************************************************/
{
    daq::ftk::Position extf_hw = daq::ftk::Position::OUT; // HW is OUT
    uint n_fpga = 0; // since the hw has it's own bit,use 0 to free up address space 
    uint32_t sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_HW_LAYERMAP_HISTO , extf_hw);

    HistoHelper(srv, sourceid);
}




/************************************************************/
void DataFlowReaderSsb::publishExtraHistos(uint32_t option)
/************************************************************/
{
  //here you can publish extra distributions, that will end up on OH and can be added to DQMF visualisation

  std::vector<int64_t> srv;
  std::vector<float> srvf;

  srv.clear();
  getNEventsIn(srv); 
  publishSingleTH1F(srv,"NEventsIn");
  m_theSSBSummary->NEventsIn = srv;

  srv.clear();
  getNTracksOut(srv); 
  publishSingleTH1F(srv,"NTracksOut");
  m_theSSBSummary->NTracksOut = srv;

  srv.clear();
  getNTracksOutRecovery(srv); 
  publishSingleTH1F(srv,"NTracksOutRecovery");
  m_theSSBSummary->NTracksOutRecovery = srv;

  srvf.clear();
  getTrackRate(srvf); 
  publishSingleTH1F(srvf,"TrackRate");
  m_theSSBSummary->TrackRate = srvf;

  srvf.clear();
  getHWTemperature(srvf);
  publishSingleTH1F(srvf,"HW_temperature");
  m_theSSBSummary->HW_temperature = srvf;

  srvf.clear();
  getHWVCCINT(srvf);
  publishSingleTH1F(srvf,"HW_VCCINT");
  m_theSSBSummary->HW_VCCINT =  srvf;

  srvf.clear();
  getHWVCCAUX(srvf);
  publishSingleTH1F(srvf,"HW_VCCAUX");
  m_theSSBSummary->HW_VCCAUX = srvf;

  srvf.clear();
  getHWVCCBRAM(srvf);
  publishSingleTH1F(srvf,"HW_VCCBRAM");
  m_theSSBSummary->HW_VCCBRAM =  srvf;

  srvf.clear();
  getHWVPVN(srvf);
  publishSingleTH1F(srvf,"HW_VPVN");
  m_theSSBSummary->HW_VPVN = srvf;

  srvf.clear();
  getHWVREFP(srvf);
  publishSingleTH1F(srvf,"HW_VREFVP");
  m_theSSBSummary->HW_VREFVP = srvf;

  srvf.clear();
  getHWVREFN(srvf);
  publishSingleTH1F(srvf,"HW_VREFVN");
  m_theSSBSummary->HW_VREFVN = srvf;

  srv.clear();
  getL1id_error(srv); 
  publishSingleTH1F(srv,"L1id_error");
  m_theSSBSummary->L1id_error = srv;

  srv.clear();
  getFifoInBusyCumulative(srv); 
  publishSingleTH1F(srv,"FifoInBusyCumulative");
  m_theSSBSummary->FifoInBusyCumulative = srv;

  srv.clear();
  getFifoOutBusyCumulative(srv); 
  publishSingleTH1F(srv,"FifoOutBusyCumulative");
  m_theSSBSummary->FifoOutBusyCumulative = srv;

  srvf.clear();
  getEventRate(srvf); 
  publishSingleTH1F(srvf,"SsbEventRate"); // can go to event rate?
  m_theSSBSummary->SsbEventRate = srvf;

  srv.clear();
  getLinkErrorsCount(srv); 
  publishSingleTH1F(srv,"LinkErrorsCount"); // should go to getdataerrors?
  m_theSSBSummary->LinkErrorsCount = srv;

  srv.clear();
  getEXTFResets(srv); 
  publishSingleTH1F(srv,"EXTFResets");
  m_theSSBSummary->EXTFResets = srv;

  srv.clear();
  getNEventsProcSyncEngine(srv); 
  publishSingleTH1F(srv,"NEventsProcSyncEngine");  
  m_theSSBSummary->NEventsProcSyncEngine = srv;

  srv.clear();
  getEXPState(srv);
  publishSingleTH1F(srv, "EXPState");
  m_theSSBSummary->EXPState = srv;

  srv.clear();
  getReadyState(srv);
  publishSingleTH1F(srv, "ReadyState");
  m_theSSBSummary->ReadyState = srv;

  srv.clear();
  getMasterState(srv);
  publishSingleTH1F(srv, "MasterState");
  m_theSSBSummary->MasterState = srv;

  srv.clear();
  getFWVersion(srv);
  publishSingleTH1F(srv, "FWVersion");
  m_theSSBSummary->FWVersion = srv;

  srv.clear();
  getFifoInUnderflowFlag(srv);
  publishSingleTH1F(srv, "FifoInUnderflowFlag");
  m_theSSBSummary->FifoInUnderflowFlag = srv;

  srv.clear();
  getFifoOutUnderflowFlag(srv);
  publishSingleTH1F(srv, "FifoOutUnderflowFlag");
  m_theSSBSummary->FifoOutUnderflowFlag = srv;

  srv.clear();
  getMinEvtProcessingTime(srv);
  publishSingleTH1F(srv, "MinEvtProcessingTime");
  m_theSSBSummary->MinEvtProcessingTime = srv;

  srvf.clear();
  getIBLHitRate(srvf);
  publishSingleTH1F(srvf, "IBLHitRate");
  m_theSSBSummary->IBLHitRate = srvf;

  srvf.clear();
  getSCT5HitRate(srvf);
  publishSingleTH1F(srvf, "SCT5HitRate");
  m_theSSBSummary->SCT5HitRate = srvf;

  srvf.clear();
  getSCT7HitRate(srvf);
  publishSingleTH1F(srvf, "SCT7HitRate");
  m_theSSBSummary->SCT7HitRate = srvf;

  srvf.clear();
  getSCT11HitRate(srvf);
  publishSingleTH1F(srvf, "SCT11HitRate");
  m_theSSBSummary->SCT11HitRate = srvf;

  srv.clear();
  getMaxModulesPerEvent(srv);
  publishSingleTH1F(srv, "MaxModulesPerEvent");
  m_theSSBSummary->MaxModulesPerEvent = srv;

  srvf.clear();
  getModuleRate(srvf);
  publishSingleTH1F(srvf, "ModuleRate");
  m_theSSBSummary->ModuleRate = srvf;

  srv.clear();
  getMaxClustersPerEvent(srv);
  publishSingleTH1F(srv, "MaxClustersPerEvent");
  m_theSSBSummary->MaxClustersPerEvent = srv;

  srvf.clear();
  getClusterRate(srvf);
  publishSingleTH1F(srvf, "ClusterRate");
  m_theSSBSummary->ClusterRate = srvf;
  
  srv.clear();
  getMaxTracksPerEvent(srv);
  publishSingleTH1F(srv, "MaxTracksPerEvent");
  m_theSSBSummary->MaxTracksPerEvent = srv;

  srv.clear();
  getHitTrackingLimits(srv);
  publishSingleTH1F(srv, "HitTrackingLimits");
  m_theSSBSummary->HitTrackingLimits = srv;

  // Histograms from FW
  stringstream ss;
  // Loop through 0-3
  for (int fpga = 0; fpga <n_EXTF_FPGAs ; fpga++) {
      // Check if the extf is actually enabled
      srv.clear();
      if (m_enabled_FPGAs[fpga] == true) getTrackHistogram(srv, fpga);      
      else srv.push_back(-1);
      ss << "TrackHistogram" << fpga;
      publishSingleTH1F(srv, ss.str());
      switch(fpga){
      case 0: m_theSSBSummary->TrackHistogram0 = srv;
      case 1: m_theSSBSummary->TrackHistogram1 = srv;
      case 2: m_theSSBSummary->TrackHistogram2 = srv;
      case 3: m_theSSBSummary->TrackHistogram3 = srv;
      }
      
      ss.str("");

      srv.clear();
      if (m_enabled_FPGAs[fpga] == true) getAuxHistogram(srv, fpga);
      else srv.push_back(-1);
      ss << "AuxHistogram" << fpga;
      publishSingleTH1F(srv, ss.str());
      switch(fpga){
      case 0: m_theSSBSummary->AuxHistogram0 = srv;
      case 1: m_theSSBSummary->AuxHistogram1 = srv;
      case 2: m_theSSBSummary->AuxHistogram2 = srv;
      case 3: m_theSSBSummary->AuxHistogram3 = srv;
      }
      ss.str("");

      srv.clear();
      if (m_enabled_FPGAs[fpga] == true) getDFHistogram(srv, fpga);
      else srv.push_back(-1);
      ss << "DFHistogram" << fpga;
      publishSingleTH1F(srv, ss.str());
      switch(fpga){
      case 0: m_theSSBSummary->DFHistogram0 = srv;
      case 1: m_theSSBSummary->DFHistogram1 = srv;
      case 2: m_theSSBSummary->DFHistogram2 = srv;
      case 3: m_theSSBSummary->DFHistogram3 = srv;
      }
      ss.str("");

      srv.clear();
      if (m_enabled_FPGAs[fpga] == true) getSkewHistogram(srv, fpga);
      else srv.push_back(-1);
      ss << "SkewHistogram" << fpga;
      publishSingleTH1F(srv, ss.str());
      switch(fpga){
      case 0: m_theSSBSummary->SkewHistogram0 = srv;
      case 1: m_theSSBSummary->SkewHistogram1 = srv;
      case 2: m_theSSBSummary->SkewHistogram2 = srv;
      case 3: m_theSSBSummary->SkewHistogram3 = srv;
      }
      ss.str("");

      srv.clear();
      if (m_enabled_FPGAs[fpga] == true) getOutPacketLenHistogram(srv, fpga);
      else srv.push_back(-1);
      ss << "OutPacketLenHistogram" << fpga;
      publishSingleTH1F(srv, ss.str());
      switch(fpga){
      case 0: m_theSSBSummary->OutPacketLenHistogram0 = srv;
      case 1: m_theSSBSummary->OutPacketLenHistogram1 = srv;
      case 2: m_theSSBSummary->OutPacketLenHistogram2 = srv;
      case 3: m_theSSBSummary->OutPacketLenHistogram3 = srv;
      }
      ss.str("");

      srv.clear();
      if (m_enabled_FPGAs[fpga] == true) getDFA_PH_TimeoutHistogram(srv, fpga);
      else srv.push_back(-1);
      ss << "DFA_PH_TimeoutHistogram" << fpga;
      publishSingleTH1F(srv, ss.str());
      switch(fpga){
      case 0: m_theSSBSummary->DFA_PH_TimeoutHistogram0 = srv;
      case 1: m_theSSBSummary->DFA_PH_TimeoutHistogram1 = srv;
      case 2: m_theSSBSummary->DFA_PH_TimeoutHistogram2 = srv;
      case 3: m_theSSBSummary->DFA_PH_TimeoutHistogram3 = srv;
      }
      ss.str("");

      srv.clear();
      if (m_enabled_FPGAs[fpga] == true) getDFB_PH_TimeoutHistogram(srv, fpga);
      else srv.push_back(-1);
      ss << "DFB_PH_TimeoutHistogram" << fpga;
      publishSingleTH1F(srv, ss.str());
      switch(fpga){
      case 0: m_theSSBSummary->DFB_PH_TimeoutHistogram0 = srv;
      case 1: m_theSSBSummary->DFB_PH_TimeoutHistogram1 = srv;
      case 2: m_theSSBSummary->DFB_PH_TimeoutHistogram2 = srv;
      case 3: m_theSSBSummary->DFB_PH_TimeoutHistogram3 = srv;
      }
      ss.str("");

      srv.clear();
      if (m_enabled_FPGAs[fpga] == true) getAUXA_PH_TimeoutHistogram(srv, fpga);
      else srv.push_back(-1);
      ss<< "AUXA_PH_TimeoutHistogram" << fpga;
      publishSingleTH1F(srv, ss.str());
      switch(fpga){
      case 0: m_theSSBSummary->AUXA_PH_TimeoutHistogram0 = srv;
      case 1: m_theSSBSummary->AUXA_PH_TimeoutHistogram1 = srv;
      case 2: m_theSSBSummary->AUXA_PH_TimeoutHistogram2 = srv;
      case 3: m_theSSBSummary->AUXA_PH_TimeoutHistogram3 = srv;
      }
      ss.str("");

      srv.clear();
      if (m_enabled_FPGAs[fpga] == true) getAUXB_PH_TimeoutHistogram(srv, fpga);
      else srv.push_back(-1);
      ss << "AUXB_PH_TimeoutHistogram" << fpga;
      publishSingleTH1F(srv, ss.str());
      switch(fpga){
      case 0: m_theSSBSummary->AUXB_PH_TimeoutHistogram0 = srv;
      case 1: m_theSSBSummary->AUXB_PH_TimeoutHistogram1 = srv;
      case 2: m_theSSBSummary->AUXB_PH_TimeoutHistogram2 = srv;
      case 3: m_theSSBSummary->AUXB_PH_TimeoutHistogram3 = srv;
      }
      ss.str("");

      srv.clear();
      if (m_enabled_FPGAs[fpga] == true) getL1IDSkipHistogram(srv, fpga);
      else srv.push_back(-1);
      ss << "L1IDSkipHistogram" << fpga;
      publishSingleTH1F(srv, ss.str());
      switch(fpga){
      case 0: m_theSSBSummary->L1IDSkipHistogram0 = srv;
      case 1: m_theSSBSummary->L1IDSkipHistogram1 = srv;
      case 2: m_theSSBSummary->L1IDSkipHistogram2 = srv;
      case 3: m_theSSBSummary->L1IDSkipHistogram3 = srv;
      }
      ss.str("");

      srv.clear();
      if (m_enabled_FPGAs[fpga] == true) getTFFIFO_L1IDSkipHistogram(srv, fpga);
      else srv.push_back(-1);
      ss << "TFFIFO_L1IDSkipHistogram" << fpga;
      publishSingleTH1F(srv, ss.str());
      switch(fpga){
      case 0: m_theSSBSummary->TFFIFO_L1IDSkipHistogram0 = srv;
      case 1: m_theSSBSummary->TFFIFO_L1IDSkipHistogram1 = srv;
      case 2: m_theSSBSummary->TFFIFO_L1IDSkipHistogram2 = srv;
      case 3: m_theSSBSummary->TFFIFO_L1IDSkipHistogram3 = srv;
      }
      ss.str("");

      srv.clear();
      if (m_enabled_FPGAs[fpga] == true) getDFAL1IDSkipHistogram(srv, fpga);
      else srv.push_back(-1);
      ss << "DFAL1IDSkipHistogram" << fpga;
      publishSingleTH1F(srv, ss.str());
      switch(fpga){
      case 0: m_theSSBSummary->DFAL1IDSkipHistogram0 = srv;
      case 1: m_theSSBSummary->DFAL1IDSkipHistogram1 = srv;
      case 2: m_theSSBSummary->DFAL1IDSkipHistogram2 = srv;
      case 3: m_theSSBSummary->DFAL1IDSkipHistogram3 = srv;
      }
      ss.str("");

      srv.clear();
      if (m_enabled_FPGAs[fpga] == true) getDFBL1IDSkipHistogram(srv, fpga);
      else srv.push_back(-1);
      ss << "DFBL1IDSkipHistogram" << fpga;
      publishSingleTH1F(srv, ss.str());
      switch(fpga){
      case 0: m_theSSBSummary->DFBL1IDSkipHistogram0 = srv;
      case 1: m_theSSBSummary->DFBL1IDSkipHistogram1 = srv;
      case 2: m_theSSBSummary->DFBL1IDSkipHistogram2 = srv;
      case 3: m_theSSBSummary->DFBL1IDSkipHistogram3 = srv;
      }
      ss.str("");

      srv.clear();
      if (m_enabled_FPGAs[fpga] == true) getAUXAL1IDSkipHistogram(srv, fpga);
      else srv.push_back(-1);
      ss<< "AUXAL1IDSkipHistogram" << fpga;
      publishSingleTH1F(srv, ss.str());
      switch(fpga){
      case 0: m_theSSBSummary->AUXAL1IDSkipHistogram0 = srv;
      case 1: m_theSSBSummary->AUXAL1IDSkipHistogram1 = srv;
      case 2: m_theSSBSummary->AUXAL1IDSkipHistogram2 = srv;
      case 3: m_theSSBSummary->AUXAL1IDSkipHistogram3 = srv;
      }
      ss.str("");

      srv.clear();
      if (m_enabled_FPGAs[fpga] == true) getAUXBL1IDSkipHistogram(srv, fpga);
      else srv.push_back(-1);
      ss << "AUXBL1IDSkipHistogram" << fpga;
      publishSingleTH1F(srv, ss.str());
      switch(fpga){
      case 0: m_theSSBSummary->AUXBL1IDSkipHistogram0 = srv;
      case 1: m_theSSBSummary->AUXBL1IDSkipHistogram1 = srv;
      case 2: m_theSSBSummary->AUXBL1IDSkipHistogram2 = srv;
      case 3: m_theSSBSummary->AUXBL1IDSkipHistogram3 = srv;
      }
      ss.str("");
      
      srv.clear();
      if (m_enabled_FPGAs[fpga] == true) getLayerMapHistogram(srv, fpga);
      else srv.push_back(-1);
      ss << "LayerMapHistogram" << fpga;
      publishSingleTH1F(srv, ss.str());
      switch(fpga){
      case 0: m_theSSBSummary->LayerMapHistogram0 = srv;
      case 1: m_theSSBSummary->LayerMapHistogram1 = srv;
      case 2: m_theSSBSummary->LayerMapHistogram2 = srv;
      case 3: m_theSSBSummary->LayerMapHistogram3 = srv;
      }
      ss.str("");
      


          
  }

  // Hitwarrior histograms
  srv.clear();
  getHWL1IDSkipHistogram(srv);
  publishSingleTH1F(srv, "HWL1IDSkipHistogram");
  m_theSSBSummary->HWL1IDSkipHistogram = srv;

  srv.clear();
  getHWNTrackHistogram(srv);
  publishSingleTH1F(srv, "HWNTrackHistogram");
  m_theSSBSummary->HWNTrackHistogram = srv;

  srv.clear();
  getHWChi2Histogram(srv);
  publishSingleTH1F(srv, "HWchi2Histogram");
  m_theSSBSummary->HWchi2Histogram = srv;

  srv.clear();
  getHWD0Histogram(srv);
  publishSingleTH1F(srv, "HWd0Histogram");
  m_theSSBSummary->HWd0Histogram = srv;

  srv.clear();
  getHWZ0Histogram(srv);
  publishSingleTH1F(srv, "HWz0Histogram");
  m_theSSBSummary->HWz0Histogram = srv;

  srv.clear();
  getHWCotthHistogram(srv);
  publishSingleTH1F(srv, "HWcotthHistogram");
  m_theSSBSummary->HWcotthHistogram = srv;

  srv.clear();
  getHWPhi0Histogram(srv);
  publishSingleTH1F(srv, "HWphi0Histogram");
  m_theSSBSummary->HWphi0Histogram = srv;

  srv.clear();
  getHWCurvHistogram(srv);
  publishSingleTH1F(srv, "HWcurvHistogram");
  m_theSSBSummary->HWcurvHistogram = srv;

  srv.clear();
  getHWLayerMapHistogram(srv);
  publishSingleTH1F(srv, "HWLayerMapHistogram");
  m_theSSBSummary->HWLayerMapHistogram = srv;
  
}
