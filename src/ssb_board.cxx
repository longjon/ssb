#include <iostream>

#include "ssb/ssb_board.h"
#include <ssb/SsbFitConstants.hh>
#include <ssb/SSBVMEloader.h>

#include <ftkcommon/FtkCool.hh>

#include <chrono>
#include <thread>
//#include <malloc.h>


namespace daq {
  namespace ftk {

      ssb_board::ssb_board(uint32_t slot, uint32_t LinkID) 
      : m_slot(slot),
        m_LinkID(LinkID),
	m_fpga_vmeif(NULL),
	m_FPGAs(0)       
    {
      m_enabled_fpgas = {0,0,0,0,0};
      m_enabled_channels = {0,0,0,0,0,0,0,0};

      m_EXTF_Freq   = std::unique_ptr< std::vector<uint32_t> >(new std::vector<uint32_t>);
      *m_EXTF_Freq = {100000000,100000000,100000000,100000000};
      m_EXTF_Old_FW = std::unique_ptr< std::vector<bool> >(new std::vector<bool>);
      *m_EXTF_Old_FW = {false, false, false, false};

      m_SRSBs = {0,0,0,0,0};

      ERS_LOG( "ssb_board::constructor: Done");
    }


    void ssb_board::setup(const dal::ReadoutModule_Ssb * module_dal)
    {
      ERS_LOG( "ssb::setup() method" );
      
      // Read FPGAs
      std::string fpga_type;
      uint32_t fpga_bay;

      m_conf = module_dal;

      m_name = module_dal->UID();
      for(std::vector<const daq::ftk::dal::Ssb_Fpga*>::const_iterator it = module_dal->get_FPGAs().begin();
          it != module_dal->get_FPGAs().end();
          it++ )
	{

	  fpga_type = (*it)->get_Type();
	  fpga_bay = (*it)->get_Id();
	  ERS_LOG( "FPGA = " << std::hex << fpga_bay << std::dec << "  " << fpga_type << "  " << (*it)->get_Enable() );	 

	  if( (*it)->get_Enable() ) {
            if( (*it)->get_Type()=="EXTF" || (*it)->get_Type()=="HITWARRIOR" ){ m_enabled_fpgas[fpga_bay]=true;}
	    if( (*it)->get_Type()=="EXTF" ) {              
	      std::shared_ptr<ftk::ssb_fpga> fpga_temp=std::make_shared<ftk::ssb_fpga_proc>(m_slot, (*it)->get_Id() );
	      fpga_temp->setup(*it);
              std::stringstream ss;
              ss << m_name <<"-EXTF-"<<fpga_bay;


              // Print info about using COOL
              if (m_conf->get_UseCool()){
                  std::stringstream message;
                  message << "Using COOL DB to load constants";
                  daq::ftk::ftkException e(ERS_HERE, fpga_temp->name_ftk(), message.str());
                  ers::info(e);
              } else { // txt based loading              
                  // Check for empty string paths
                  fpga_temp->setName(ss.str() );
                  if ((*it)->get_EXPPath() ==""){
                      daq::ftk::EmptyStringParameter exp(ERS_HERE, fpga_temp->name_ftk(), "EXP Constant");
                      if (daq::ftk::isFatalMode()) throw exp;
                      else ers::error(exp);
                  }

                  if ((*it)->get_TFPath() ==""){
                      daq::ftk::EmptyStringParameter exp(ERS_HERE, fpga_temp->name_ftk(), "TF Constant");
                      if (daq::ftk::isFatalMode()) throw exp;
                      else ers::error(exp);
                  }
              }

              fpga_temp->set_LoadTFConstants(false); 
              fpga_temp->set_LoadEXPConstants(false); 
              if ((*it)->get_SSMapAUXPix() ==""){
                  daq::ftk::EmptyStringParameter exp(ERS_HERE, fpga_temp->name_ftk(), "SSMapAUXPix");
                  if (daq::ftk::isFatalMode()) throw exp;
                  else ers::error(exp); 
              }

              if ((*it)->get_SSMapAUXSCT() ==""){
                  daq::ftk::EmptyStringParameter exp(ERS_HERE, fpga_temp->name_ftk(), "SSMapAUXSCT");
                  if (daq::ftk::isFatalMode()) throw exp;
                  else ers::error(exp);
              }

              if ((*it)->get_SSMapDFPix() ==""){
                  daq::ftk::EmptyStringParameter exp(ERS_HERE, fpga_temp->name_ftk(), "SSMapDFPix");
                  if (daq::ftk::isFatalMode()) throw exp;
                  else ers::error(exp);
              }

              if ((*it)->get_SSMapDFSCT() ==""){
                  daq::ftk::EmptyStringParameter exp(ERS_HERE, fpga_temp->name_ftk(), "SSMapDFSCT");
                  if (daq::ftk::isFatalMode()) throw exp;
                  else ers::error(exp);
              }

	      fpga_temp->set_LinkID( m_LinkID );

              // Check that configuration uses a good tower.
              if ( (*it)->get_Tower() < 1 || (*it)->get_Tower() > 64) {
                  std::stringstream message;
                  message << "A valid tower must be specified [1,64]!";
                  daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str());
                  if (daq::ftk::isFatalMode()) throw excp;
                  else ers::error(excp);
              }

              
	      m_FPGAs.push_back(fpga_temp);

              // build list for dataflow so we can ignore channels in the reader publish
              m_enabled_channels[2*fpga_bay]     = !(*it)->get_IgnoreDFB();
              m_enabled_channels[2*fpga_bay + 1] = !(*it)->get_IgnoreAUXB();
              
              // setup SRSB
              std::shared_ptr<ssb_srsb> srsb_temp = std::make_shared<ssb_srsb>(fpga_temp);
              m_SRSBs[fpga_bay] = srsb_temp;
	    }
	    else if( (*it)->get_Type()=="HITWARRIOR" ) { 
	      std::shared_ptr<ftk::ssb_fpga> fpga_temp_2=std::make_shared<ftk::ssb_fpga_hitwarrior>(m_slot, (*it)->get_Id()); 
	      fpga_temp_2->setup(*it);
              std::stringstream ss;
              ss << m_name <<"-HW";
              fpga_temp_2->setName(ss.str() );

	      m_FPGAs.push_back(fpga_temp_2);

              // setup SRSB // requires some changesin ssb_fpga
              std::shared_ptr<ssb_srsb> srsb_temp = std::make_shared<ssb_srsb>(fpga_temp_2, false);
              m_SRSBs[(*it)->get_Id()] = srsb_temp;
	    }
	    else if( (*it)->get_Type()=="VMEIF" ) { 
	      m_fpga_vmeif=VMEManager::global().ssb(m_slot, (*it)->get_Id()); 
	      m_fpga_vmeif->disableBlockTransfers(false); // use real block transfer
	    }
	  } 
	}
      if(!m_fpga_vmeif) {
          std::stringstream message;
          message << "Impossible to continue, no FPGA_VMEIF configured!";
          daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str());
          if (daq::ftk::isFatalMode()) throw excp;
          else ers::error(excp);
      }

    }


    void ssb_board::configure(unsigned int runNumber)
    {
        ERS_LOG( "ssb::configure() method" );

        bool throwBadChecksum = false;

        std::map<uint32_t, std::unique_ptr<SsbFitConstants> > db_constants; // stores tower to constants map
        // Cool database setup

        std::string dbConn;
        std::string dbTag;
        const std::vector<string> &db_conf=m_conf->get_HiddenParams();
        // TODO clean this up with global FTK database connections
        if(db_conf.size()==2 && m_conf->get_UseCool()) {
            dbConn=db_conf[0];
            dbTag=db_conf[1];
        } else if (m_conf->get_UseCool())
        {
            std::stringstream message;
            message << "Missing database configuration info";
            daq::ftk::ftkException e(ERS_HERE, name_ftk(), message.str());
            ers::error(e);            
        }

        FtkCool cool;
        if (m_conf->get_UseCool())
        {
            // hack to avoid COOL crashes when conccurently loading boards
            std::this_thread::sleep_for(std::chrono::seconds(( m_slot % 4 ))); // wait a fixed different time before opening COOL connection

            cool = FtkCool(dbConn,dbTag,runNumber);
        }

        // Loop over all of the FPGAs and configure them
        if( m_FPGAs.size()!=0 ){

           // loop over all fgpas
           for(uint32_t ifpga=0; ifpga<m_FPGAs.size(); ifpga++){

               // define up here out of scope so we only need to compute them once
               uint32_t sw_tf_checksum  = 0x0; 
               uint32_t sw_exp_checksum = 0x0; 

               // Main Memory checksum
               if (!m_FPGAs[ifpga]->isHitWarrior()){
                   m_FPGAs[ifpga]->set_LoadTFConstants( false );
                   m_FPGAs[ifpga]->set_LoadEXPConstants( false );
                   
                   // If we're in the running state, we probably got shutdown improperly, get to configured first
                   if ( m_FPGAs[ifpga]->getRCState() == SSB_RC_RUNNING )
                   {
                       std::stringstream message;
                       message << "SSB in slot "<<m_slot<<" fpga(" << m_FPGAs[ifpga]->get_m_fpgaID() << ") is in running state, but trying to configure.  Forcing EXTF to configured state.";
                       daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str());
                       ers::warning(excp);
                       
                       m_FPGAs[ifpga]->configure(runNumber, db_constants, false); // load TF/EXP constants is false, so it won't load anything here
                   }
                   
                   // JDL TODO // what kind of exception handling is needed for COOL?  Do the checksumming
                   // Get fpga tower, check if we already have constants from db, if not pull
                   if (m_conf->get_UseCool())
                   {

                       std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
                       // Check if we already pulled and computed the constants for this tower
                       if (db_constants.find(m_FPGAs[ifpga]->getTower()) == db_constants.end())
                       {                                                 
                           storage::variant bank12;
                           storage::variant sectorMap;
                           std::unique_ptr<SsbFitConstants> ssb_constants;
                           
                           // TODO this needs a check that it returned something
                           cool.read(bank12,FtkCool::FtkConfigType::CORRGEN_RAW_12L_GCON,m_FPGAs[ifpga]->getTower());
                           cool.read(sectorMap,FtkCool::FtkConfigType::SECTORS_RAW_8L_CONN,m_FPGAs[ifpga]->getTower());
                           db_constants[m_FPGAs[ifpga]->getTower()] = std::make_unique<SsbFitConstants>(bank12,sectorMap);
                           db_constants[m_FPGAs[ifpga]->getTower()]->calculate();
                       }
                       std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
                       std::chrono::duration<double> time_span = t2 - t1;
                       ERS_LOG( " Scalability: >>>> Retrieved and computed SSB constants from COOL for tower " << m_FPGAs[ifpga]->getTower() <<" in "<< time_span.count() << " seconds. <<<< " );
                       

                   }
                   

                   // Grab info from FPGA
                   (*m_EXTF_Freq)[ m_FPGAs[ifpga]->get_m_fpgaID() ] = m_FPGAs[ifpga]->getFWFreq();
                   (*m_EXTF_Old_FW)[ m_FPGAs[ifpga]->get_m_fpgaID() ] = m_FPGAs[ifpga]->isOldFW();

                   // Get hardware checksum
                   uint32_t hw_tf_checksum=0x0;
                   uint32_t hw_exp_checksum=0x0;
                   std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
                   m_FPGAs[ifpga]->getHWChecksums(hw_exp_checksum, hw_tf_checksum);
                   std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
                   std::chrono::duration<double> time_span = t2 - t1;
                   ERS_LOG( " Scalability: >>>> Retrieved HW checksums in " << time_span.count() << " seconds. <<<< " );

                   ERS_LOG( "SSB HW checksum TF,EXP for EXTF("<<ifpga<<"):" << std::hex << hw_tf_checksum << ", " <<hw_exp_checksum);

                   // Get software checksum
                   if (m_conf->get_UseCool())
                   {
                       if (db_constants.find(m_FPGAs[ifpga]->getTower()) == db_constants.end())
                       {
                           std::stringstream message;
                           message << "No SsbFitConstants found in map for tower " << m_FPGAs[ifpga]->getTower()<<". Can't load constants or compute checksums.";
                           daq::ftk::ftkException e(ERS_HERE, name_ftk(), message.str());
                           ers::error(e);
                       }

                       std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
                       sw_exp_checksum = db_constants[m_FPGAs[ifpga]->getTower()]->checkSumEXP();
                       sw_tf_checksum = db_constants[m_FPGAs[ifpga]->getTower()]->checkSumTF();
                       std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
                       std::chrono::duration<double> time_span = t2 - t1;
                       ERS_LOG( " Scalability: >>>> Retrieved SW checksums from COOL in " << time_span.count() << " seconds. <<<< " );
                       
                   } else {
                       m_FPGAs[ifpga]->getSWChecksums(sw_exp_checksum, sw_tf_checksum);
                   }
                   ERS_LOG("SSB SW checksum TF,EXP for EXTF("<<ifpga<<"):" << std::hex << sw_tf_checksum << ", " <<sw_exp_checksum);
         
                   // Extrapolator constants
                   if (sw_exp_checksum != hw_exp_checksum){
                       if (daq::ftk::isLoadMode()) {
                           std::stringstream message;
                           message << "SSB checksum mismatch: slot "<<m_slot<<" FPGA "<< ifpga << " EXP, board="<<std::hex <<hw_exp_checksum <<" expected=" << sw_exp_checksum<<".  Will load constants";
                           daq::ftk::ftkException e(ERS_HERE, name_ftk(), message.str());
                           daq::ftk::FTKWrongChecksum excp(ERS_HERE, name_ftk(), sw_exp_checksum, hw_exp_checksum, "EXTF", m_FPGAs[ifpga]->get_EXPConf(), e);
                           ers::warning(excp);
                          
                           m_FPGAs[ifpga]->set_LoadEXPConstants( true );
                       }
                       else{
                           std::stringstream message;
                           message << "SSB checksum mismatch FATAL: slot "<<m_slot<<" FPGA " << ifpga << " EXP, board="<<std::hex <<hw_exp_checksum <<" expected=" << sw_exp_checksum<<".  isLoadMode is Off!";
                           daq::ftk::ftkException e(ERS_HERE, name_ftk(), message.str());
                           daq::ftk::FTKWrongChecksum excp(ERS_HERE, name_ftk(), sw_exp_checksum, hw_exp_checksum, "EXTF", m_FPGAs[ifpga]->get_EXPConf(), e);
                           throwBadChecksum = true;
                           ers::error(excp);
                       }
                   }
         
                   // TF constants
                   if (sw_tf_checksum != hw_tf_checksum){
                       if (daq::ftk::isLoadMode()) {
                           std::stringstream message;
                           message << "SSB checksum mismatch: slot "<<m_slot<<" FPGA " << ifpga << " TF, board="<<std::hex<<hw_tf_checksum <<" expected=" << sw_tf_checksum<<".  Will load constants";
                           daq::ftk::ftkException e(ERS_HERE, name_ftk(), message.str());
                           daq::ftk::FTKWrongChecksum excp(ERS_HERE, name_ftk(), sw_tf_checksum, hw_tf_checksum, "EXTF", m_FPGAs[ifpga]->get_TFConf(), e);
                           ers::warning(excp);

                           m_FPGAs[ifpga]->set_LoadTFConstants( true );
                       }
                       else{
                           std::stringstream message;
                           message << "SSB checksum mismatch FATAL: slot "<<m_slot<<" FPGA " << ifpga << " TF, board="<<std::hex<<hw_tf_checksum <<" expected=" << sw_tf_checksum<<".  isLoadMode is Off!";
                           daq::ftk::ftkException e(ERS_HERE, name_ftk(), message.str());
                           daq::ftk::FTKWrongChecksum excp(ERS_HERE, name_ftk(), sw_tf_checksum, hw_tf_checksum, "EXTF", m_FPGAs[ifpga]->get_TFConf(), e);
                           throwBadChecksum = true;
                           ers::error(excp);
                       }
                   }
               } // End EXTF Checksum block

               // ***********************************
               // Configure the FPGA and load constants if needed
               m_FPGAs[ifpga]->configure(runNumber, db_constants, m_conf->get_UseCool()); 
               // ***********************************

               // Check checksums again if loading constants.  FATAL if still bad
               if (!m_FPGAs[ifpga]->isHitWarrior() && (m_FPGAs[ifpga]->get_LoadEXPConstants() || m_FPGAs[ifpga]->get_LoadTFConstants())){

                   // Get hardware checksum
                   uint32_t hw_tf_checksum=0x0;
                   uint32_t hw_exp_checksum=0x0;
                   m_FPGAs[ifpga]->getHWChecksums(hw_exp_checksum, hw_tf_checksum);


                   ERS_LOG( "SSB HW checksum TF,EXP:" << std::hex << hw_tf_checksum << ", " <<hw_exp_checksum);

                   //ERS_LOG("SSB SW checksum TF,EXP:" << sw_tf_checksum << ", " <<sw_exp_checksum);
         
                   // Extrapolator constants
                   if (sw_exp_checksum != hw_exp_checksum){
                       std::stringstream message;
                       message << "SSB checksum FATAL: slot "<<m_slot<<" FPGA "<< ifpga << " EXP, board="<<std::hex <<hw_exp_checksum <<" expected=" << sw_exp_checksum<<".";
                       daq::ftk::ftkException e(ERS_HERE, name_ftk(), message.str());
                       daq::ftk::FTKWrongChecksum excp(ERS_HERE, name_ftk(), sw_exp_checksum, hw_exp_checksum, "EXTF", m_FPGAs[ifpga]->get_EXPConf(), e);
                       throwBadChecksum = true;
                       ers::error(excp);
                   }
         
                   // TF constants
                   if (sw_tf_checksum != hw_tf_checksum){
                       std::stringstream message;
                       message << "SSB checksum FATAL: slot "<<m_slot<<" FPGA " << ifpga << " TF, board="<<std::hex<<hw_tf_checksum <<" expected=" << sw_tf_checksum<<".";
                       daq::ftk::ftkException e(ERS_HERE, name_ftk(), message.str());
                       daq::ftk::FTKWrongChecksum excp(ERS_HERE, name_ftk(), sw_tf_checksum, hw_tf_checksum, "EXTF", m_FPGAs[ifpga]->get_TFConf(), e);
                       throwBadChecksum = true;
                       ers::error(excp);
                   }                   
               }               
           } // end of fpga loop

           if (throwBadChecksum){
               daq::ftk::FTKWrongChecksum excp(ERS_HERE, name_ftk(), 0, 0, "At least one checksum (see errors)", "");
               if (daq::ftk::isFatalMode()) throw excp;
               else ers::error(excp);
           }
                      
        } // end if n_fpga !=0

        /*
        std::cout<<"During, just before config() returns"<<std::endl;
        struct mallinfo during =  mallinfo();
        std::cout << "ASDF: Total during: "<<during.arena<<std::endl;
        std::cout<< "during uordblks: "<<during.uordblks <<std::endl;
        std::cout<< "during fordblks: "<<during.fordblks <<std::endl;
        */

         // add some sanity checks on the FPGA_VMEIF via m_fpga_vmeif
    }


    void ssb_board::connect()
    {
      ERS_LOG( "ssb::connect() method" );
      if( m_FPGAs.size()!=0 ){
        for(uint32_t ifpga=0; ifpga<m_FPGAs.size(); ifpga++){
          m_FPGAs[ifpga]->connect(); 
          
          // Poke to make sure spybuffers aren't frozen
          m_FPGAs[ifpga]->unfreezeSpybuffer(true);
          
        }
      }

      checkLinks();

    }


    void ssb_board::prepareForRun()
    {
      ERS_LOG( "ssb::prepareForRun() method" );
      if( m_FPGAs.size()!=0 ){
        for(uint32_t ifpga=0; ifpga<m_FPGAs.size(); ifpga++){
          m_FPGAs[ifpga]->prepareForRun(); 
        }
      }
    }


   void ssb_board::stop()
    {
      ERS_LOG( "ssb::stop() method" );
      if( m_FPGAs.size()!=0 ){
        for(uint32_t ifpga=0; ifpga<m_FPGAs.size(); ifpga++){
          m_FPGAs[ifpga]->stop(); 
        }
      }
    }


    void ssb_board::disconnect()
    {
      ERS_LOG( "ssb::disconnect() method" );
      if( m_FPGAs.size()!=0 ){
        for(uint32_t ifpga=0; ifpga<m_FPGAs.size(); ifpga++){
          m_FPGAs[ifpga]->disconnect(); 
        }
      }
    }


    void ssb_board::unconfigure()
    {
      ERS_LOG( "ssb::unconfigure() method" );
      if( m_FPGAs.size()!=0 ){
        for(uint32_t ifpga=0; ifpga<m_FPGAs.size(); ifpga++){
          m_FPGAs[ifpga]->unconfigure(); 
        }
      }
    }




      void ssb_board::freezeSpybuffers()
      {
          ERS_LOG( "ssb::freezeSpybuffers() method" );
          if( m_FPGAs.size()!=0 ){
              for(uint32_t ifpga=0; ifpga<m_FPGAs.size(); ifpga++){
                  m_FPGAs[ifpga]->freezeSpybuffer(); 
                  
              }
          }
      }

      void ssb_board::unfreezeSpybuffers(bool force)
      {
          ERS_LOG( "ssb::unfreezeSpybuffers() method" );
          if( m_FPGAs.size()!=0 ){
              for(uint32_t ifpga=0; ifpga<m_FPGAs.size(); ifpga++){
                  m_FPGAs[ifpga]->unfreezeSpybuffer(force); 
                  
              }
          }
      }

      void ssb_board::freezeSRSB()
      {
          ERS_LOG( "ssb::freezeSRSB() method" );
          if( m_FPGAs.size()!=0 ){
              for(uint32_t ifpga=0; ifpga<m_FPGAs.size(); ifpga++){
                  m_FPGAs[ifpga]->freezeSRSB(); 
                  
              }
              
          }
      }

      void ssb_board::unfreezeSRSB(bool force)
      {
          ERS_LOG( "ssb::unfreezeSRSB() method" );
          if( m_FPGAs.size()!=0 ){
              for(uint32_t ifpga=0; ifpga<m_FPGAs.size(); ifpga++){
                  m_FPGAs[ifpga]->unfreezeSRSB(force); 
                  
              }
          }
      }



      void ssb_board::checkLinks()
      {
          bool throwError = false;
          if( m_FPGAs.size()!=0 ){
              for(uint32_t ifpga=0; ifpga<m_FPGAs.size(); ifpga++){
    
                  u_int LinkStatus = 0;
                  bool retVal = m_FPGAs[ifpga]->getLinkStatus(LinkStatus);
                  if (retVal == false) continue; // Skip checking if there was a VME error

                  if (!m_FPGAs[ifpga]->isHitWarrior()){
                      if (!(LinkStatus & AUX_A_MASK) && (ifpga==0 || ifpga==1) ) { // EXTF 0 and 1 are on AUXA
                          std::stringstream ss;
                          ss << "SSB slot=" <<m_slot<<", FPGA=" << ifpga << ": AUXA link is down.";
                          //daq::ftk::ftkException excp(ERS_HERE, name_ftk(), ss.str());
                          daq::ftk::FTKLinkDown excp(ERS_HERE, m_FPGAs[ifpga]->name_ftk(), "AUXA ", "");
                          ers::error(excp);
                          throwError = true;
                      }
    
                      if (!(LinkStatus & AUX_B_MASK) && (ifpga==2 || ifpga==3) ) {// EXTF 2 and 3 are on AUXB
                          std::stringstream ss;
                          ss << "SSB slot=" <<m_slot<<", FPGA=" << ifpga << ": AUXB link is down.";
                          daq::ftk::FTKLinkDown excp(ERS_HERE, m_FPGAs[ifpga]->name_ftk(), "AUXB ", "");
                          //daq::ftk::ftkException excp(ERS_HERE, name_ftk(), ss.str());
                          ers::error(excp);
                          throwError = true;
                      }
    
                      if (!(LinkStatus & DF_MASK)) {
                          std::stringstream ss;
                          ss << "SSB slot=" <<m_slot<<", FPGA=" << ifpga << ": DF link is down."; 
                          //daq::ftk::ftkException excp(ERS_HERE, name_ftk(), ss.str());
                          daq::ftk::FTKLinkDown excp(ERS_HERE, m_FPGAs[ifpga]->name_ftk(), "DF ", "");
                          ers::error(excp);
                          throwError = true;
                      }  
                  } // only EXTF
              } // loop over fpgas

              if (throwError) {
                  daq::ftk::FTKLinkDown excp(ERS_HERE, name_ftk(), "", "At least one unmasked link down.");
                  if (daq::ftk::isFatalMode()) throw excp;
                  else ers::error(excp);
              }
          }
      }
      
      
      void ssb_board::checkFWVersion()
      {
          bool throwBadFWVersion = false;
          
          // Loop over all of the FPGAs and configure them
          if( m_FPGAs.size()!=0 ){
              // loop over all fgpas
              for(uint32_t ifpga=0; ifpga<m_FPGAs.size(); ifpga++){
                  // Firmware version check
                  if ( (m_FPGAs[ifpga]->getFWHash() != m_FPGAs[ifpga]->getFWHashOKS()) || (m_FPGAs[ifpga]->getFWDate() !=m_FPGAs[ifpga]->getFWDateOKS()) ) {
                      std::stringstream message;
                      message << "SSB FW mismatch: slot " << m_slot << " FPGA " << ifpga << (m_FPGAs[ifpga]->isHitWarrior() ? " HW" : " EXTF") << ", board = " 
                              << std::hex<<m_FPGAs[ifpga]->getFWHash() << "+"<< m_FPGAs[ifpga]->getFWDate() << " expected = " << std::hex << m_FPGAs[ifpga]->getFWHashOKS() << "+" <<m_FPGAs[ifpga]->getFWDateOKS();

                      daq::ftk::ftkException e(ERS_HERE, name_ftk(), message.str());
                      daq::ftk::WrongFwVersion excp(ERS_HERE, name_ftk(), (m_FPGAs[ifpga]->isHitWarrior() ? " HW" : " EXTF"), ifpga, m_FPGAs[ifpga]->getFWHash(),  m_FPGAs[ifpga]->getFWHashOKS(), e);  // this exception misses a place to put the date
                      throwBadFWVersion = true;
                      ers::error(excp);
                  }
                  else{
                      std::stringstream message;
                      message <<m_FPGAs[ifpga]->name_ftk() << (m_FPGAs[ifpga]->isHitWarrior() ? " HW" : " EXTF") << " FW version (hash+date): " << std::hex<<m_FPGAs[ifpga]->getFWHash() << "+" << m_FPGAs[ifpga]->getFWDate();
                      daq::ftk::ftkException l(ERS_HERE, name_ftk(), message.str());
                      ers::log(l);
                  }
                  
              } // end FPGA loop        
              
              if (throwBadFWVersion){
                  daq::ftk::WrongFwVersion excp(ERS_HERE, name_ftk(), "At least one FPGA (see errors)", 0, 0, 0);
                  if (daq::ftk::isFatalMode()) throw excp;
                  else ers::error(excp);
              }
          }      

      }




      const std::vector< std::shared_ptr<ssb_srsb> > ssb_board::getSRSB(bool update)
      {
          for (uint i=0; i<m_SRSBs.size(); i++) // Ask to update the SRSBs from board into memory
          {
              if (m_SRSBs[i] && update) m_SRSBs[i]->update();
          }
          return m_SRSBs;
      }




      std::vector< std::pair<std::shared_ptr<daq::ftk::SpyBuffer>, uint32_t> > ssb_board::getSpybuffers()
      {


          std::vector< std::pair<std::shared_ptr<daq::ftk::SpyBuffer>, uint32_t> > return_pairs; 
          std::vector< std::pair<std::shared_ptr<daq::ftk::SpyBuffer>, uint32_t> > main_spybuffers; 
          std::vector< std::pair<std::shared_ptr<daq::ftk::SpyBuffer>, uint32_t> > histo_spybuffers; 
          std::vector< std::pair<std::shared_ptr<daq::ftk::SpyBuffer>, uint32_t> > srsbs; 

          uint32_t sourceid = 0;
          daq::ftk::Position extf_hw;
          uint32_t n_fpga = 0;
          
          if( m_FPGAs.size()!=0 ){
              freezeSpybuffers(); // always freeze
              for(uint32_t ifpga=0; ifpga<m_FPGAs.size(); ifpga++){
                  
                  

                  extf_hw = m_FPGAs[ifpga]->isHitWarrior() ? daq::ftk::Position::OUT : daq::ftk::Position::IN; // 0 in for EXTF and 1 out for HW
                  n_fpga = m_FPGAs[ifpga]->isHitWarrior() ? 0 : m_FPGAs[ifpga]->get_m_fpgaID(); // EXTF 0-3 and HW 4.  This may later need to change to deal with separate spybuffer columns as their own 'spybuffer'.  Normally the HW is FPGA #4, but since there is a separate bit for in/out we'll set this to 0 so we have more sourceids
                  
                  sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga + SSB_SOURCEID_SPYBUFFER, extf_hw);

                  // Add main spybuffer dump
                  std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > spybuffer_pair = std::make_pair(m_FPGAs[ifpga]->getSpybufferSSB(), sourceid);                  
                  main_spybuffers.push_back( spybuffer_pair );                  

              }
              unfreezeSpybuffers(); // unfreeze only if there wasn't a board freeze
          }

          // get histo spybuffers
          histo_spybuffers = getSpybuffersHistos();

          // get SRSBs
          srsbs = getSRSB_emon(); 
          
          // merge vectors
          return_pairs.reserve(main_spybuffers.size() + histo_spybuffers.size());
          return_pairs.insert( return_pairs.end(), main_spybuffers.begin(), main_spybuffers.end() );
          return_pairs.insert( return_pairs.end(), histo_spybuffers.begin(), histo_spybuffers.end() );
          return_pairs.insert( return_pairs.end(), srsbs.begin(), srsbs.end() );


          return return_pairs;
      }



      std::vector< std::pair<std::shared_ptr<daq::ftk::SpyBuffer>, uint32_t> > ssb_board::getSpybuffersHistos()
      {

          std::vector< std::pair<std::shared_ptr<daq::ftk::SpyBuffer>, uint32_t> > return_pairs; 
          uint32_t sourceid = 0;
          daq::ftk::Position extf_hw;
          uint32_t n_fpga = 0;
          
          if( m_FPGAs.size()!=0 ){
              freezeSpybuffers(); // always freeze
              for(uint32_t ifpga=0; ifpga<m_FPGAs.size(); ifpga++){
                                    

                  extf_hw = m_FPGAs[ifpga]->isHitWarrior() ? daq::ftk::Position::OUT : daq::ftk::Position::IN; // 0 in for EXTF and 1 out for HW
                  n_fpga = m_FPGAs[ifpga]->isHitWarrior() ? 0 : m_FPGAs[ifpga]->get_m_fpgaID(); // EXTF 0-3 and HW 4.  This may later need to change to deal with separate spybuffer columns as their own 'spybuffer'.  Normally the HW is FPGA #4, but since there is a separate bit for in/out we'll set this to 0 so we have more sourceids

                  // EXTF Histo spybuffers
                  if (!m_FPGAs[ifpga]->isHitWarrior()) 
                  {

                      // DF histo
                      sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_DF_HISTO , extf_hw);
                      std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > dfhisto_pair = std::make_pair(m_FPGAs[ifpga]->getSpybuffer_histo( (m_FPGAs[ifpga]->isOldFW() ? SSB_EXTF_DFHISTO_OLD : SSB_EXTF_DFHISTO), SSB_HISTO_NWORDS, SSB_HISTO_BLT ), sourceid);

                      // AUX histo
                      sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_AUX_HISTO , extf_hw);
                      return_pairs.push_back( dfhisto_pair );
                      std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > auxhisto_pair = std::make_pair(m_FPGAs[ifpga]->getSpybuffer_histo( (m_FPGAs[ifpga]->isOldFW() ? SSB_EXTF_AUXHISTO_OLD : SSB_EXTF_AUXHISTO), SSB_HISTO_NWORDS, SSB_HISTO_BLT ), sourceid);
                      return_pairs.push_back( auxhisto_pair );

                      // Track histo
                      sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_TRACK_HISTO , extf_hw);
                      std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > trackhisto_pair = std::make_pair(m_FPGAs[ifpga]->getSpybuffer_histo(   ( m_FPGAs[ifpga]->isOldFW() ? SSB_EXTF_TRACKHISTO_OLD : SSB_EXTF_TRACKHISTO), SSB_HISTO_NWORDS, SSB_HISTO_BLT   ), sourceid);
                      return_pairs.push_back( trackhisto_pair );

                      // Out packet len histo
                      sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_OUTPACKETLEN_HISTO , extf_hw);
                      std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > outpacketlenhisto_pair = std::make_pair(m_FPGAs[ifpga]->getSpybuffer_histo( (m_FPGAs[ifpga]->isOldFW() ? SSB_EXTF_OUT_PACKET_LEN_HISTO_OLD:SSB_EXTF_OUT_PACKET_LEN_HISTO), SSB_HISTO_NWORDS, SSB_HISTO_BLT ), sourceid);
                      return_pairs.push_back( outpacketlenhisto_pair );

                      // skew histo
                      sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_SKEW_HISTO , extf_hw);
                      std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > skewhisto_pair = std::make_pair(m_FPGAs[ifpga]->getSpybuffer_histo(SSB_EXTF_SKEWHISTO, SSB_HISTO_NWORDS, SSB_HISTO_BLT), sourceid);
                      return_pairs.push_back( skewhisto_pair );

                      // dfa packet handler timeout histo
                      sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_DFAPH_TIMEOUT_HISTO , extf_hw);
                      std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > dfaphtimeouthisto_pair = std::make_pair(m_FPGAs[ifpga]->getSpybuffer_histo(SSB_EXTF_DFA_PH_TIMEOUT_HISTO, SSB_HISTO_NWORDS, SSB_HISTO_BLT), sourceid);
                      return_pairs.push_back( dfaphtimeouthisto_pair );

                      // dfb packet handler timeout histo
                      sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_DFBPH_TIMEOUT_HISTO , extf_hw);
                      std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > dfbphtimeouthisto_pair = std::make_pair(m_FPGAs[ifpga]->getSpybuffer_histo(SSB_EXTF_DFB_PH_TIMEOUT_HISTO, SSB_HISTO_NWORDS, SSB_HISTO_BLT), sourceid);
                      return_pairs.push_back( dfbphtimeouthisto_pair );

                      // auxa packet handler timeout histo
                      sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_AUXAPH_TIMEOUT_HISTO , extf_hw);
                      std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > auxaphtimeouthisto_pair = std::make_pair(m_FPGAs[ifpga]->getSpybuffer_histo(SSB_EXTF_AUXA_PH_TIMEOUT_HISTO, SSB_HISTO_NWORDS, SSB_HISTO_BLT), sourceid);
                      return_pairs.push_back( auxaphtimeouthisto_pair );

                      // auxb packet handler timeout histo
                      sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_AUXBPH_TIMEOUT_HISTO , extf_hw);
                      std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > auxbphtimeouthisto_pair = std::make_pair(m_FPGAs[ifpga]->getSpybuffer_histo(SSB_EXTF_AUXB_PH_TIMEOUT_HISTO, SSB_HISTO_NWORDS, SSB_HISTO_BLT), sourceid);
                      return_pairs.push_back( auxbphtimeouthisto_pair );

                      // global l1id skip histo
                      sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_L1ID_SKIP_HISTO , extf_hw);
                      std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > l1idskiphisto_pair = std::make_pair(m_FPGAs[ifpga]->getSpybuffer_histo(SSB_EXTF_L1ID_SKIP_HISTO, SSB_HISTO_NWORDS, SSB_HISTO_BLT), sourceid);
                      return_pairs.push_back( l1idskiphisto_pair );


                      // TF FIFO l1id skip histo
                      sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_TFFIFOL1ID_SKIP_HISTO , extf_hw);
                      std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t >TFFIFOl1idskiphisto_pair = std::make_pair(m_FPGAs[ifpga]->getSpybuffer_histo(SSB_EXTF_TFFIFO_L1ID_SKIP_HISTO, SSB_HISTO_NWORDS, SSB_HISTO_BLT), sourceid);
                      return_pairs.push_back(TFFIFOl1idskiphisto_pair);

                      // DFA l1id skip histo
                      sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_DFAL1ID_SKIP_HISTO , extf_hw);
                      std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > dfal1idskiphisto_pair = std::make_pair(m_FPGAs[ifpga]->getSpybuffer_histo(SSB_EXTF_DFA_L1ID_SKIP_HISTO, SSB_HISTO_NWORDS, SSB_HISTO_BLT), sourceid);
                      return_pairs.push_back( dfal1idskiphisto_pair );

                      // DFB l1id skip histo
                      sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_DFBL1ID_SKIP_HISTO , extf_hw);
                      std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > dfbl1idskiphisto_pair = std::make_pair(m_FPGAs[ifpga]->getSpybuffer_histo(SSB_EXTF_DFB_L1ID_SKIP_HISTO, SSB_HISTO_NWORDS, SSB_HISTO_BLT), sourceid);
                      return_pairs.push_back( dfbl1idskiphisto_pair );

                      // AUXA l1id skip histo
                      sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_AUXAL1ID_SKIP_HISTO , extf_hw);
                      std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > auxal1idskiphisto_pair = std::make_pair(m_FPGAs[ifpga]->getSpybuffer_histo(SSB_EXTF_AUXA_L1ID_SKIP_HISTO, SSB_HISTO_NWORDS, SSB_HISTO_BLT), sourceid);
                      return_pairs.push_back( auxal1idskiphisto_pair );

                      // AUXB l1id skip histo
                      sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_AUXBL1ID_SKIP_HISTO , extf_hw);
                      std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > auxbl1idskiphisto_pair = std::make_pair(m_FPGAs[ifpga]->getSpybuffer_histo(SSB_EXTF_AUXB_L1ID_SKIP_HISTO, SSB_HISTO_NWORDS, SSB_HISTO_BLT), sourceid);
                      return_pairs.push_back( auxbl1idskiphisto_pair );

                      // Layer map histo
                      sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_LAYERMAP_HISTO , extf_hw);
                      std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > layermaphisto_pair = std::make_pair(m_FPGAs[ifpga]->getSpybuffer_histo(SSB_EXTF_LAYERMAPHISTO, SSB_HISTO_NWORDS, SSB_HISTO_BIG_BLT), sourceid);
                      return_pairs.push_back( layermaphisto_pair );
                      

                  }
                  // HW histograms
                  else {

                      // NTracks Histo
                      sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_HW_NTRACK_HISTO , extf_hw);
                      std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > hwntrackshisto_pair = std::make_pair(m_FPGAs[ifpga]->getSpybuffer_histo(SSB_HW_NTRACKS_HISTO, SSB_HISTO_NWORDS, SSB_HISTO_NWORDS), sourceid); // the nBLT value for this histo is different than the rest
                      return_pairs.push_back( hwntrackshisto_pair );

                      // HW l1id skip skip histo
                      sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_HWL1ID_SKIP_HISTO , extf_hw);
                      std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > hwl1idskiphisto_pair = std::make_pair(m_FPGAs[ifpga]->getSpybuffer_histo(SSB_HW_L1ID_SKIP_HISTO, SSB_HISTO_NWORDS, SSB_HISTO_BLT), sourceid);
                      return_pairs.push_back( hwl1idskiphisto_pair );

                      // HW track chi2 histo
                      sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_HW_CHI2_HISTO , extf_hw);
                      std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > hwchi2histo_pair = std::make_pair(m_FPGAs[ifpga]->getSpybuffer_histo(SSB_HW_CHI2_HISTO, SSB_HISTO_NWORDS, SSB_HISTO_BLT), sourceid);
                      return_pairs.push_back( hwchi2histo_pair );

                      // HW track D0 histo
                      sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_HW_D0_HISTO , extf_hw);
                      std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > hwd0histo_pair = std::make_pair(m_FPGAs[ifpga]->getSpybuffer_histo(SSB_HW_D0_HISTO, SSB_HISTO_NWORDS, SSB_HISTO_BLT), sourceid);
                      return_pairs.push_back( hwd0histo_pair );

                      // HW track Z0 histo
                      sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_HW_Z0_HISTO , extf_hw);
                      std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > hwz0histo_pair = std::make_pair(m_FPGAs[ifpga]->getSpybuffer_histo(SSB_HW_Z0_HISTO, SSB_HISTO_NWORDS, SSB_HISTO_BLT), sourceid);
                      return_pairs.push_back( hwz0histo_pair );

                      // HW track COTTH histo
                      sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_HW_COTTH_HISTO , extf_hw);
                      std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > hwcotthhisto_pair = std::make_pair(m_FPGAs[ifpga]->getSpybuffer_histo(SSB_HW_COTTH_HISTO, SSB_HISTO_NWORDS, SSB_HISTO_BLT), sourceid);
                      return_pairs.push_back( hwcotthhisto_pair );

                      // HW track PHI0 histo
                      sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_HW_PHI0_HISTO , extf_hw);
                      std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > hwphi0histo_pair = std::make_pair(m_FPGAs[ifpga]->getSpybuffer_histo(SSB_HW_PHI0_HISTO, SSB_HISTO_NWORDS, SSB_HISTO_BLT), sourceid);
                      return_pairs.push_back( hwphi0histo_pair );

                      // HW track CURV histo
                      sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_HW_CURV_HISTO , extf_hw);
                      std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > hwcurvhisto_pair = std::make_pair(m_FPGAs[ifpga]->getSpybuffer_histo(SSB_HW_CURV_HISTO, SSB_HISTO_NWORDS, SSB_HISTO_BLT), sourceid);
                      return_pairs.push_back( hwcurvhisto_pair );

                      // HW Layermap histo
                      sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_HW_LAYERMAP_HISTO , extf_hw);
                      std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > hwlayermaphisto_pair = std::make_pair(m_FPGAs[ifpga]->getSpybuffer_histo(SSB_HW_LAYERMAP_HISTO, SSB_HISTO_NWORDS, SSB_HISTO_BIG_BLT), sourceid);
                      return_pairs.push_back( hwlayermaphisto_pair );

                  }

              }
              unfreezeSpybuffers(); // unfreeze only if there wasn't a board freeze
          }
          
          return return_pairs;
      }






      std::vector< std::pair<std::shared_ptr<daq::ftk::SpyBuffer>, uint32_t> > ssb_board::getSRSB_emon()
      {

          std::vector< std::pair<std::shared_ptr<daq::ftk::SpyBuffer>, uint32_t> > return_pairs; 
          uint32_t sourceid = 0;
          daq::ftk::Position extf_hw;
          uint32_t n_fpga = 0;
          
          // Grab updated srsb
          const std::vector< std::shared_ptr<ssb_srsb> > srsbs = getSRSB();

          if( m_FPGAs.size()!=0 ){
              for(uint32_t ifpga=0; ifpga<m_FPGAs.size(); ifpga++){
                                    
                  extf_hw = m_FPGAs[ifpga]->isHitWarrior() ? daq::ftk::Position::OUT : daq::ftk::Position::IN; // 0 in for EXTF and 1 out for HW
                  n_fpga = m_FPGAs[ifpga]->isHitWarrior() ? 0 : m_FPGAs[ifpga]->get_m_fpgaID(); // EXTF 0-3 and HW 4.  This may later need to change to deal with separate spybuffer columns as their own 'spybuffer'.  Normally the HW is FPGA #4, but since there is a separate bit for in/out we'll set this to 0 so we have more sourceids

                  // EXTF
                  if (!m_FPGAs[ifpga]->isHitWarrior()) 
                  {
                      sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_SRSB , extf_hw);
                      std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > srsb_pair = std::make_pair( m_FPGAs[ifpga]->getSRSB_emon(SSB_ADDR_SRSB) , sourceid);
                      return_pairs.push_back( srsb_pair );            

                  } else { // hitwarrior

                      sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::SSB, m_slot, n_fpga+SSB_SOURCEID_SRSB , extf_hw);
                      std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > srsb_pair = std::make_pair(m_FPGAs[ifpga]->getSRSB_emon(SSB_HW_SRSB), sourceid);
                      return_pairs.push_back( srsb_pair);
                  }

              }
          }
          
          return return_pairs;
      }


  }
}
