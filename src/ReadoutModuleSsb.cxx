/*****************************************************************/
/*                                                               */
/*                                                               */
/*****************************************************************/


//#include <malloc.h>

#include <iostream>

// OKS, RC and IS
#include <rc/RunParams.h>
#include "ssb/ReadoutModuleSsb.h"
#include "ssb/dal/ReadoutModule_Ssb.h"
#include "ssb/dal/Ssb_Fpga.h"
#include "config/Configuration.h"
#include "DFdal/RCD.h"
#include "DFdal/ReadoutConfiguration.h"
#include "RunControl/Common/OnlineServices.h"
#include "is/info.h"

#include "ftkcommon/exceptions.h"
#include "ftkcommon/core.h"
// common tools
#include "ftkcommon/Utils.h"

// VME
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"

#include <chrono>


using namespace daq::ftk;
using namespace std;


ReadoutModule_Ssb::ReadoutModule_Ssb()
  : m_configuration(0),
    m_dryRun(false)
{
  #include "cmt/version.ixx"
  std::cout << "Build timestamp: " << __DATE__ << " " << __TIME__ << std::endl;
  std::cout << cmtversion;

  if (daq::ftk::isLoadMode())
    { ERS_LOG("Loading mode"); }

  ERS_LOG("ReadoutModule_Ssb::constructor: Done");
}


ReadoutModule_Ssb::~ReadoutModule_Ssb() noexcept
{
  ERS_LOG("ReadoutModule_Ssb::destructor: Done");
}


void ReadoutModule_Ssb::setup(DFCountedPointer<ROS::Config> configuration)
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), "Setup");
  
  // Get online objects from daq::rc::OnlineServices
  m_ipcpartition      = daq::rc::OnlineServices::instance().getIPCPartition();
  m_isInfoDict=ISInfoDictionary(m_ipcpartition);
  m_appName = daq::rc::OnlineServices::instance().applicationName();
  auto dal_rcdAppl    = daq::rc::OnlineServices::instance().getApplication().cast<daq::df::RCD>();
  Configuration *conf = configuration->getPointer<Configuration>("configurationDB");
  // OR: Configuration &conf   = daq::rc::OnlineServices::instance().getConfiguration();
  auto readOutConfig  = conf->cast<daq::df::ReadoutConfiguration>( dal_rcdAppl->get_Configuration() );
 
  // Read some info from the DB
  const ftk::dal::ReadoutModule_Ssb * module_dal = conf->get<ftk::dal::ReadoutModule_Ssb>(configuration->getString("UID"));
  m_name               = module_dal->UID();
  m_slot               = module_dal->get_Slot();
  m_crate              = module_dal->get_Crate();
  m_LinkID              = module_dal->get_LinkID();
  m_isServerName = readOutConfig->get_ISServerName(); 
  m_dryRun       = module_dal->get_DryRun();
  m_ReadHistosForPublish = module_dal->get_ReadHistosForPublish();
  m_PublishOnlyNonZeroBins = module_dal->get_PublishOnlyNonZeroBins();
  m_ispublish          = false;//keep false until after prepareForRun
  m_doPublishFullStats = true;  

  std::string fpga_type;
  for(std::vector<const daq::ftk::dal::Ssb_Fpga*>::const_iterator it = module_dal->get_FPGAs().begin();
          it != module_dal->get_FPGAs().end();
          it++ ){
      fpga_type = (*it)->get_Type();
  }

  if(m_dryRun)
  { 
      std::stringstream message;
      message << "ReadoutModule_Ssb.DryRun option set in DB, publish(fullstats) skipped!";
      daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str());
      ers::warning(excp);      
  };

  // Pass the configuration to ssb_board
  m_board=std::make_unique<ftk::ssb_board>(m_slot, m_LinkID);
  m_board->setup(module_dal);

  // Log the fgpa fw info
  std::vector<std::shared_ptr<ftk::ssb_fpga>> FPGAs =  m_board->getFPGAs();  
  for (unsigned int i_fpga =0; i_fpga<FPGAs.size(); i_fpga++){
      u_int FW_version = FPGAs[i_fpga]->getFWHash();
      u_int FWDate     = FPGAs[i_fpga]->getFWDate();
      stringstream ss;
      if (FPGAs[i_fpga]->get_m_fpgaID() < 4)  ss << "EXTF " << FPGAs[i_fpga]->get_m_fpgaID() << " Date:"<<std::hex<<FWDate;      
      else     ss<< "HW Date:" <<std::hex<<FWDate;

      daq::ftk::FTKFwVersion fw1(ERS_HERE, m_name, ss.str(), FW_version);
      ers::info(fw1);
  }

 
  // Creating the ISInfo object
  std::string isProvider = m_isServerName + "." + m_name;
  ERS_LOG("IS: publishing in " << isProvider);
  try 
  { 
    m_ssbNamed=std::make_unique<ssbNamed>( m_ipcpartition, isProvider.c_str() ); 
  }
  catch( ers::Issue &ex)
  {
    stringstream ss;
    ss << m_name << " Error while creating ssbNamed object";
    daq::ftk::ISException e(ERS_HERE, ss.str(), ex);  // NB: append the original exception (ex) to the new one 
    if (daq::ftk::isFatalMode()) throw e;
    else ers::error(e);
  }
  
  // Creating the Histogramming provider
  std::string OHServer("Histogramming"); // TODO: make it configurable
  std::string OHName("FTK_" + m_name);   // Name of the pubblication directory
  try { 
    m_ohProvider=std::make_unique<OHRootProvider> ( m_ipcpartition , OHServer, OHName );
    m_ohRawProvider=std::make_unique<OHRawProvider<>>( m_ipcpartition , OHServer, getDFOHNameString(m_name, "SSB", m_crate, m_slot) ); 
    ERS_LOG("OH: publishing in " << OHServer << "." << m_name << ", from application " << m_appName);
  }
  catch ( daq::oh::Exception & ex)
  { // Raise a warning or throw an exception. 
    daq::ftk::OHException e(ERS_HERE, m_name, ex);  // NB: append the original exception (ex) to the new one 
    if (daq::ftk::isFatalMode()) throw e; 
    else ers::error(e);
  }
  //common DataFlow parser
  m_dfReaderSsb=std::make_unique<DataFlowReaderSsb>(m_ohRawProvider, true);
  m_dfReaderSsb->init(m_name);
  m_dfReaderSsb->setNEXTF(SSB_N_EXTF_FPGA);
  m_dfReaderSsb->setEnabledFPGAs( m_board->getEnabledFPGAs() );
  m_dfReaderSsb->setEnabledChannels( m_board->getEnabledChannels() );
  m_dfReaderSsb->setPublishOnlyNonZeroBins(m_PublishOnlyNonZeroBins);
  m_dfReaderSsb->setSlot(m_slot);
  m_dfReaderSsb->setEXTFFreq(m_board->getEXTFFreq());
  m_dfReaderSsb->setOldFW(m_board->getEXTFOldFW());

  // Construct the histograms
  //m_histogram=std::make_unique<TH1F>("newHistogram","newRegHistogram",10,-50.5,50.5);

  // setup the MT functionality
  initialize( m_name, module_dal, BOARD_SSB );

  tLog.end(ERS_HERE);
}

unsigned int countBits(unsigned int data) {
  int count=0;
  for (unsigned int i=0; i<sizeof(unsigned int)*8; i++) 
    if ( (data>>i)&0x1 ) count++;
  return count;
}


void ReadoutModule_Ssb::doConfigure(const daq::rc::TransitionCmd& cmd)
{
   RunParams runParams; 
    m_isInfoDict.findValue("RunParams.RunParams",runParams);
   //   params.put("runNumber",runParams.run_number);

  m_ispublish = false;
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());
  // TODO this is a hack for testing
  //m_board->configure(runParams.run_number); 
  /*
  struct mallinfo before =  mallinfo();
  std::cout << "Total before: "<<before.arena <<std::endl;
  std::cout<< "before uordblks: "<<before.uordblks <<std::endl;
  std::cout<< "before fordblks: "<<before.fordblks <<std::endl;
  m_board->configure(10000); 
  struct mallinfo after =  mallinfo();
  std::cout << " Total after: "<<after.arena<<std::endl;
  std::cout<<  "after uordblks: "<<after.uordblks<<std::endl;
  std::cout<<  "after fordblks: "<<after.fordblks<<std::endl;
  */
  m_board->configure(10000);
  ERS_LOG(cmd.toString() << " Done");
  tLog.end(ERS_HERE);
}


void ReadoutModule_Ssb::doUnconfigure(const daq::rc::TransitionCmd& cmd)
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());
  m_board->unconfigure(); 
  tLog.end(ERS_HERE);
}



void ReadoutModule_Ssb::doConnect(const daq::rc::TransitionCmd& cmd)
{
  m_ispublish = false;
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());
  m_board->connect(); 
  //Api to read out a specific set of registers. The set is controlled by the third argument
  m_statusRegisterSsbFactory=std::make_unique<StatusRegisterSsbFactory>(m_name, m_slot, "014", m_isServerName, m_ipcpartition);

  // Setup for publishing to emon
  m_emonDataOut = reinterpret_cast< daq::ftk::FtkEMonDataOut* >(ROS::DataOut::sampled());
  if (!m_emonDataOut) { 
      std::stringstream message;
      message << "Failed to retrive pointer for emonDataOut (SSB)!";
      daq::ftk::FTKMonitoringError excp(ERS_HERE, name_ftk(), message.str());
      ers::error(excp);
  }
  else { m_emonDataOut->setScalingFactor(1); }

  tLog.end(ERS_HERE);
}

void ReadoutModule_Ssb::checkConfig(const daq::rc::SubTransitionCmd& cmd)
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());
  m_board->checkFWVersion();
  tLog.end(ERS_HERE);
}

void ReadoutModule_Ssb::doDisconnect(const daq::rc::TransitionCmd& cmd)
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());
  m_board->disconnect(); 
  tLog.end(ERS_HERE);
}

void ReadoutModule_Ssb::startNoDF(const daq::rc::SubTransitionCmd& cmd)
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());
  m_board->prepareForRun(); 
  tLog.end(ERS_HERE);
}

void ReadoutModule_Ssb::doPrepareForRun(const daq::rc::TransitionCmd& cmd)
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());
  m_ispublish = true;
  ERS_LOG("Enable IS Publishing.");
  tLog.end(ERS_HERE);
}


void ReadoutModule_Ssb::doStopRecording(const daq::rc::TransitionCmd& cmd)
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());
  this->publish(); // Final publish
  m_ispublish = false;
  m_board->stop(); 
  tLog.end(ERS_HERE);
}


void ReadoutModule_Ssb::doPublish( uint32_t flag, bool finalPublish )
{
    if(!m_ispublish){
        ERS_LOG("PrepareForRun is not finished. Exit Publish.");
        return;
    }

    ftkTimeLog tLog(ERS_HERE, name_ftk(), "Publish");
    //A std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
    try { // temporary bandaid
    
    // Collect the relevant information from VME
    if(!m_dryRun)
    {
        //read from the board and publish to IS raw register values
        try
        {
            m_statusRegisterSsbFactory->readout();
        }
        catch (daq::ftk::VmeError& e)
        {
            std::stringstream message;
            message << "statusRegisterSsbFactory failed to readout.";
            daq::ftk::FTKMonitoringError excp(ERS_HERE, name_ftk(), message.str(), e);
            ers::error(excp);
        }

        //read from IS, parse to OH common DataFlow information
        m_dfReaderSsb->readIS();       
        if (m_ReadHistosForPublish) // read histos from spybuffer
        {
            m_dfReaderSsb->setHistoSpybufferData( m_board->getSpybuffersHistos()  ); // has VME error catch inside
            m_dfReaderSsb->setSRSBs(  m_board->getSRSB() ); // has VME error catch inside
        }

        try
        {
            m_dfReaderSsb->publishAllHistos();
        }
        catch (daq::is::Exception &e)
        {
            stringstream ss;
            ss<< "IS exception in publish()";
            daq::ftk::ISException excp(ERS_HERE, ss.str(), e);
            ers::error(excp);
        }
    }

    m_board->checkLinks(); // Check links every publish, has VME error catch inside
    //A std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
    //A std::chrono::duration<double> time_span = t2 - t1;
    //A ERS_LOG( " Scalability: doPublish in "<< time_span.count() << " seconds. <<<< " );

    }
    catch(...) 
    {
        std::stringstream message;
        message << "Caught exception in SSB publish()";
        daq::ftk::FTKMonitoringError excp(ERS_HERE, name_ftk(), message.str());
        ers::warning(excp);
    }
    tLog.end(ERS_HERE);
  // Example: Publish in IS with schema, i.e. fill ISInfo object
  // Example:
  /*m_ssbNamed->ExampleString = "Test";
  m_ssbNamed->ExampleU16    = 77;
  m_ssbNamed->ExampleS32    = rand()%100;
  try { m_ssbNamed->checkin();}
  catch ( daq::oh::Exception & ex)
  { // Raise a warning or throw an exception. 
    daq::ftk::ISException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one 
    ers::warning(e);  //or throw e;  
  }

  // Fill and publish histograms
  // Example:
  m_histogram->Fill( rand()%100 - 50 );
  try { m_ohProvider->publish( *m_histogram, "newRegHistogram", true ); }
  catch ( daq::oh::Exception & ex)
  { // Raise a warning or throw an exception. 
    daq::ftk::OHException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one 
    ers::warning(e);  //or throw e;  
  }
  */

}

void ReadoutModule_Ssb::doPublishFullStats( uint32_t flag )
{
    ERS_LOG("Starting Publish FullStats");
    if(!m_ispublish or !m_doPublishFullStats){
        ERS_LOG("Skipping publish full stats.");
        return;
    }

    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
    try { // temporary bandaid
    if (!m_dryRun){
        
        if (!m_emonDataOut) return;
        
        ERS_LOG("Shipping SSB spybuffer to emon.");
        
        std::vector< std::pair<std::shared_ptr<daq::ftk::SpyBuffer>, uint32_t> > spybuffers;
        
        spybuffers = m_board->getSpybuffers(); // VME error catching is in here
        
        m_emonDataOut->sendData(spybuffers);              
               
    }
    std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> time_span = t2 - t1;
    ERS_LOG( " Scalability: doPublishFullStats in "<< time_span.count() << " seconds. <<<< " );
    }
    catch(...) 
    {
        std::stringstream message;
        message << "Caught exception in SSB publishFullStats()";
        daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str());
        ers::warning(excp);
    }
    ERS_LOG("Ending Publish FullStats");  
}

void ReadoutModule_Ssb::resynch(const daq::rc::ResynchCmd& cmd)
{
  ERS_LOG(cmd.toString());
  // Fill ME
  ERS_LOG(cmd.toString() << " done");
}
   


void ReadoutModule_Ssb::clearInfo() //TODO tis probably needs to be filled out
{
  ERS_LOG("Clear histograms and counters");

  // Reset histograms
  //m_histogram->Reset();

  // Reset the IS counters
  m_ssbNamed->ExampleS32 = 0 ;
  m_ssbNamed->ExampleU16 = 0 ;
}

//FOR THE PLUGIN FACTORY
extern "C"
{
  extern ROS::ReadoutModule* createReadoutModule_Ssb();
}

ROS::ReadoutModule* createReadoutModule_Ssb()
{
  return (new ReadoutModule_Ssb());
}




