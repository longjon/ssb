/*                          vme block transfer 1st try: Viviana Cavaliere
 */
#include <sstream>
#include <fstream>
#include <iostream>

#include "cmem_rcc/cmem_rcc.h"
#include "rcc_error/rcc_error.h"
#include "rcc_time_stamp/tstamp.h"
#include "vme_rcc/vme_rcc.h"
#include "ftkcommon/core.h"
#include "ssb/ssb_vme_regs.h"
#include <time.h>


#define DEBUG_BLOCK              false
#define ROD_BUFFER_MAX_SIZE    0x10000   // Buffer dimension in bytes                                                                                   
#define ROD_DMATIMEOUT            4000    // DMA timeout in ms                                                                                          
#define DEF_VME_ADDR       "0x20000100"  // VME default address                                                                                         
#define DEF_OPERATION               "r"  // Default operation        


namespace daq
{
  namespace ftk
  {
    bool blockTransferReadFromFilePure(int vmeBaseAddress, int numberOfWords, std::string filePath, bool isRead)
    {
      // Return value for VME function error management                                                                                
      VME_ErrorCode_t v_ret = 0;
      
      // Return valide for TimeStamp and CMEM error management                                                                         
      u_int err_code = 0;
      //      std::cout << "Hello World1" << std::endl; 
      
      // Block Transfer list                                                                                                           
      VME_BlockTransferList_t rlist;
      
      // CMEM variables                                                                                                                
      int cmem_desc;
      unsigned long cmem_desc_uaddr, cmem_desc_paddr; // Logical and physical addresses of the RAM buffer                              
      // used to read or write the blockstransfer                                      
      volatile u_int* d_ptr;                          // Volatile pointer to access RAM buffer                                          
      
      // Ratio words/bytes                                                                                                             
      unsigned int nByteToTransfer = numberOfWords * sizeof(int);
      std::fstream myfile;
      
      
      
      // store the file path                                                                                                           
      myfile.open( filePath.c_str() );
      if ( myfile.fail() )
	{
	  // if impossible to open file return error                                                                                
	  std::cerr <<  "\n\nError : Impossible to open file " << filePath << std::endl;
	  return false;
	}
// #ifdef DEBUG_BLOCK
//       std::cerr <<  "Opened " << filePath << std::endl;
// #endif
      
      
      //////////////////////////////////////////////////////////////                                                                   
      //// Allocating the receiver buffer   and Opening Vme address                                                                    
      //////////////////////////////////////////////////////////////                                                                    
      err_code = CMEM_Open();
      if (err_code)
        rcc_error_print(stdout, err_code);
      

      
      err_code = CMEM_SegmentAllocate(ROD_BUFFER_MAX_SIZE, const_cast<char*>("DMA_BUFFER"), &cmem_desc);
      if (err_code)
        rcc_error_print(stdout, err_code);
      
      err_code = CMEM_SegmentPhysicalAddress(cmem_desc, &cmem_desc_paddr);
      if (err_code)
        rcc_error_print(stdout, err_code);
      
      err_code = CMEM_SegmentVirtualAddress(cmem_desc, &cmem_desc_uaddr);
      if (err_code)
        rcc_error_print(stdout, err_code);
      
      
      v_ret = VME_Open();
      if (v_ret != VME_SUCCESS)
	VME_ErrorPrint(v_ret);
      
      
      //////////////////////////////////////////////////////////////                                                                   
      ////   If writing                                                                                                                
      ///////////////////////////////////////////////////////////////                                                                   
      if( ! isRead )
	{
	  d_ptr = (unsigned int*) cmem_desc_uaddr;
	  std::string temp;
	  int offset = 0;
	  while( std::getline( myfile, temp )  && (offset < numberOfWords))
	    {
	      std::stringstream ss2;
	      int mynum;
	      ss2 << temp;
	      if( ( ss2 >> std::hex >> mynum ).fail() )
		{
		  std::cerr << "\n\nFound a non number at line:" << offset << " \n\n from "  << std::endl;
		  return 1;
		}
	      std::cout << "Writing " << mynum << std::endl;
	      
	      d_ptr[offset] = mynum;
	      offset++;
	    }
	}
      //////////////////////////////////////////////////////////////                                                  
      ////   Parameters dump 


// #ifdef DEBUG_BLOCK
//       std::cout << "\nParameters: " << std::endl
// 		<< "Vme Address   = 0x"     << std::hex << vmeBaseAddress << std::endl
// 		<< "BufferAddress = 0x"     << std::hex << cmem_desc_paddr << std::endl
// 		<< "Parole trasm. = "       << std::dec << numberOfWords << std::endl
// 		<< "Bytes trasm.  = "       << std::dec << nByteToTransfer << std::endl;
// #endif
      
      //////////////////////////////////////////////////////////////                                                                   
      ////  BLOCK_TRANSFER                                                                               
      ///////////////////////////////////////////////////////////                                                                   
      // Preparation of the parameters                                                                                                  
      
      rlist.number_of_items = 1;
      rlist.list_of_items[0].vmebus_address       = vmeBaseAddress;  // VME address destination                                        
      rlist.list_of_items[0].system_iobus_address = cmem_desc_paddr; // RAM address obtained from CMEM                                 
      rlist.list_of_items[0].size_requested       = nByteToTransfer;
      
      if( isRead )
	rlist.list_of_items[0].control_word = VME_DMA_D32R;
      else
	rlist.list_of_items[0].control_word = VME_DMA_D32W;
      
      // Block transfer                                                                                                                
      v_ret = VME_BlockTransfer(&rlist, ROD_DMATIMEOUT);
      
// #ifdef DEBUG_BLOCK
//       std::cout << std::endl << "Block Transfer result: " << std::endl;
// #endif
      // check                                                                                                                          
      if (v_ret != VME_SUCCESS)
	VME_ErrorPrint(v_ret);
      else
	{
#ifdef DEBUG_BLOCK
	  std::cout << "Success ! " << std::endl;
#endif
	}
      
      //////////////////////////////////////////////////////////////                                                                   
      ////   Reading (iteration over the RAM buffer                                                                                    
      ///////////////////////////////////////////////////////////////                                                                   
      if( isRead )
	{
	  d_ptr = (unsigned int*) cmem_desc_uaddr;
	  //std::cout << std::endl << "Reading: " << std::endl;
	  
	  for ( int i = 0; i < ( numberOfWords ); i++)
	    {
	      d_ptr[i] = i;

	      std::cout << "[" << std::dec << i << "] : 0x" << std::hex << d_ptr[i] << std::endl;
	      myfile << "0x" << std::hex << d_ptr[i] << std::endl  ;
	    }
	}
      //////////////////////////////////////////////////////////////                                                                   
      //// Close everything                                                                                                            
      ///////////////////////////////////////////////////////////////                                                                   
      //      std::cout << "Closing All " << std::endl;
      
      v_ret = VME_Close();
      if (v_ret != VME_SUCCESS)
	VME_ErrorPrint(v_ret);
      
      err_code = CMEM_SegmentFree(cmem_desc);
      if (err_code)
	rcc_error_print(stdout, err_code);
      
      err_code = CMEM_Close();
      if (err_code)
	rcc_error_print(stdout, err_code);
      
      return true;
    }
    
  }
}


int main()
{

  int n_words = 80;
  time_t t1, t2;
  t1 = time(0);

  //  for (int i = 0; i< 16000 ;i++){ 
    bool status_read  =false;
    status_read = daq::ftk::blockTransferReadFromFilePure(0x20007100, n_words, "/afs/cern.ch/user/v/vcavalie/TESTBED/ssb/testvmebt.dat",true);
    
    bool status_write = daq::ftk::blockTransferReadFromFilePure(0x20007100, n_words,"/afs/cern.ch/user/v/vcavalie/TESTBED/ssb/testvmebt.dat",false);
  //   std::cout << "Hello World" << std::endl; 
  // if(status_write)
  //   std::cout << "Write successful" << std::endl;
  // else
  //   std::cout << "Not working" << std::endl;
    //  }

  t2 = time(0);

  printf("Elapsed: %.d\n", t2 - t1);    
  // if(status_read)
  //   std::cout << "Read and write successful, take a coffe now" << std::endl;
  // else
  //   std::cout << "Reading part not working" << std::endl;

  // std::cout << "Hello World" << std::endl; 

    return 0;
}



