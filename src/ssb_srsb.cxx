#include "ssb/ssb_srsb.h"

#include <thread>

namespace daq {
    namespace ftk {

        ssb_srsb::ssb_srsb(VMEInterface *vmeif, bool isEXTF): 
            directVME(true),
            m_isEXTF(isEXTF),            
            srsb_raw(SSB_EXTF_SRSB_SIZE)
            
        {
            m_fpga_vmeif = vmeif; // Grab pointer to vme
        }


        ssb_srsb::ssb_srsb(std::shared_ptr<ssb_fpga> fpga, bool isEXTF): 
            directVME(false),
            m_isEXTF(isEXTF),
            srsb_raw(SSB_EXTF_SRSB_SIZE)
        {
            m_fpga = fpga;
        }

        ssb_srsb::ssb_srsb(std::vector<uint32_t> &emon_srsb)
        {
            srsb_raw = emon_srsb;
        }


        void ssb_srsb::update()
        {
            
            uint32_t nspy_srsb = 1;
            uint32_t nBLT = ceil(float( (m_isEXTF ? SSB_EXTF_SRSB_SIZE : SSB_HW_SRSB_SIZE ) ) / SSB_SRSB_NWORDS);                

            if(directVME) // Directly pull from vme interface
            {

                // VME Access try-catch loop                                                                    
                for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
                {
                    try
                    {
                        m_fpga_vmeif->write_word( (m_isEXTF ? SSB_SRSB_FREEZE : SSB_HW_SRSB_FREEZE) , 0x1); // freeze SRSB to collect data
                        std::this_thread::sleep_for(std::chrono::milliseconds(100)); // give time to FW
                        
                        srsb_raw.clear();
                        std::vector<uint32_t> data_temp;


                        for(uint32_t iblt=0; iblt<nBLT*nspy_srsb; iblt++)
                        {
                            data_temp.clear();
                            data_temp=m_fpga_vmeif->read_block( (m_isEXTF ? SSB_ADDR_SRSB : SSB_HW_SRSB), SSB_SRSB_NWORDS);
                            srsb_raw.insert( srsb_raw.end(), data_temp.begin(), data_temp.end() );
                        }
                
                        m_fpga_vmeif->write_word( (m_isEXTF ? SSB_SRSB_UNFREEZE : SSB_HW_SRSB_UNFREEZE) , 0x1);

                    }
                    catch (daq::ftk::VmeError& e)
                    {
                        if (i == SSB_MAX_VME_RETRIES)
                        {
                            std::stringstream message;
                            message << "SSB caught VME Error while updating SRSB, may have unreliable contents.";
                            daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                            ers::warning(excp); 
                        }
                        continue;
                    }
                    catch (daq::ftk::CMEMError& e)
                    {
                        if (i == SSB_MAX_VME_RETRIES)
                        {
                            std::stringstream message;
                            message << "SSB caught VME Error while updating SRSB, may have unreliable contents.";
                            daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                            ers::warning(excp); 
                        }
                        continue;
                    }
                    break;
                }




            } else { // ask FPGA object for the srsb

                srsb_raw = m_fpga->getSRSB( (m_isEXTF ? SSB_ADDR_SRSB : SSB_HW_SRSB) , SSB_SRSB_NWORDS, nBLT);

            }
        }

    }
}
