#include "ssb/StatusRegisterSsbFactory.h"

#include "ftkcommon/exceptions.h"

namespace daq {
namespace ftk {

StatusRegisterSsbFactory::StatusRegisterSsbFactory(
	const std::string& ssbName,
	u_int ssbSlot,
	const std::string& registersToRead,
	const std::string& isServerName,
	const IPCPartition& ipcPartition
	) : StatusRegisterVMEFactory (ssbName, ssbSlot, registersToRead, isServerName) 
	{

	std::unique_ptr<StatusRegisterSsbStatusNamed> m_srSsbSNamed;
	try 
	{
		m_srSsbSNamed = std::make_unique<StatusRegisterSsbStatusNamed>(ipcPartition, m_isProvider.c_str());
	} 
	catch(ers::Issue &ex)  {
            stringstream ss;
            ss<< ssbName << " Error while creating StatusRegisterSsbStatusNamed object";
            daq::ftk::ISException e(ERS_HERE, ss.str(), ex);  // Append the original exception (ex) to the new one 
            throw(e);
	}

	// Store the name and slot of the board in StatusRegisterAMBStatusNamed
	m_srSsbSNamed->Name = m_name;
	m_srSsbSNamed->Slot = m_slot;
	
	// To read different registers, modify the following lines:
	// For documentation see https://svnweb.cern.ch/trac/atlastdaq/browser/FTK/ftkvme/tags/ftkvme-01-00-04/ftkvme/StatusRegisterVMEFactory.h#L37
	setupCollection('0',0,0x624,0x7cc,0x4,"EXTF0 Status registers: [0x630-> 0x730]","SSB_EXTF0",&(m_srSsbSNamed->EXTF0_sr_v), &(m_srSsbSNamed->EXTF0_sr_v_info));
	setupCollection('1',1,0x624,0x7cc,0x4,"EXTF1 Status registers: [0x630-> 0x730]","SSB_EXTF1",&(m_srSsbSNamed->EXTF1_sr_v), &(m_srSsbSNamed->EXTF1_sr_v_info));
	setupCollection('2',2,0x624,0x7cc,0x4,"EXTF2 Status registers: [0x630-> 0x730]","SSB_EXTF2",&(m_srSsbSNamed->EXTF2_sr_v), &(m_srSsbSNamed->EXTF2_sr_v_info));
	setupCollection('3',3,0x624,0x7cc,0x4,"EXTF3 Status registers: [0x630-> 0x730]","SSB_EXTF3",&(m_srSsbSNamed->EXTF3_sr_v), &(m_srSsbSNamed->EXTF3_sr_v_info));
	setupCollection('4',4,0x624,0x770,0x4,"HitWarrior Status registers: [0x630-> 0x730]","SSB_HW",&(m_srSsbSNamed->SSBHW_sr_v), &(m_srSsbSNamed->SSBHW_sr_v_info));



	// Pass the StatusRegisterSsbStatusNamed to SRVMEFactory so it can be checked into IS during readout()
	m_srxNamed = std::move(m_srSsbSNamed);
}

StatusRegisterSsbFactory::~StatusRegisterSsbFactory() {
	// No need to delete m_srSsbSNamed here as it's done by StatusRegisterVMEFactory when it deletes m_srxNamed
}

} // namespace ftk
} // namespace daq
