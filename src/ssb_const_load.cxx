#include <fstream>
#include <iostream>
#include <sstream>
#include <string>     // std::string, std::stoi
#include <chrono>

#include <ssb/SsbFitConstants.hh>
#include <ssb/SSBVMEloader.h>
#include "ssb/ssb_fpga_proc.h"
#include <ftkcommon/FtkCool.hh>
#include <boost/program_options.hpp>



int main(int argc,char *argv[])
{

  boost::program_options::variables_map vm;
  std::string constantDir;
  std::string outputDir;
  std::string dbConn;
  std::string dbTag;
  unsigned int region;
  int slot;
  int fpga;
  int run_number;
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "")
      ("constantDir,c", boost::program_options::value<std::string>(&constantDir)->default_value(""), "constant dir")
      ("dbConn,d", boost::program_options::value<std::string>(&dbConn)->default_value(""), "connection string")
      ("dbTag,t", boost::program_options::value<std::string>(&dbTag)->default_value(""), "tag")
      ("msgPack,m",  boost::program_options::bool_switch()->default_value(false), "")
      ("verify,v",  boost::program_options::bool_switch()->default_value(false), "")
      ("checksum,k",  boost::program_options::bool_switch()->default_value(false), "")
      ("outputDir,o",  boost::program_options::value<std::string>(&outputDir)->default_value(""), "output dir")
      ("region,r", boost::program_options::value<unsigned int>(&region)->default_value(0), "region")  
      ("slot,s", boost::program_options::value<int>(&slot)->default_value(-1), "slot")
      ("fpga,f", boost::program_options::value<int>(&fpga)->default_value(-1), "fpga")
      
      ("runNumber,n", boost::program_options::value<int>(&run_number)->default_value(-1), "run number");
    
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);

    if (vm.count("help")) {
      std::cout << desc << '\n';
      exit(0);
    }
  } 
  
  catch (const  boost::program_options::error &ex) {
    std::cerr << ex.what() << '\n';
  }
  if(constantDir!="") {
    if(dbConn!="" || dbTag !="") {
      std::cerr << "specify either constantDir or dbConn/dbTag\n";
      exit(-1);
    }
  }
  if(dbConn!="") {
    if(dbTag=="") { std::cerr << "dbTag must be set\n";exit(-1);}
    if(run_number==-1)  {std::cerr << "runNumber must be set\n";exit(-1);}
    
  }

  bool use_msgpack=vm["msgPack"].as<bool>();
  bool checksum=vm["checksum"].as<bool>();
  bool verify=vm["verify"].as<bool>();
   uint32_t fpga_exp;
  uint32_t fpga_tf;
  uint32_t sw_exp;
  uint32_t sw_tf;
 daq::ftk::ssb_fpga_proc proc(slot,fpga);

  if(checksum) {
  proc.getHWChecksums(fpga_exp,fpga_tf);

  std::cout << "FPGA EXP Checksum "<< std::hex << fpga_exp <<std::endl;
  std::cout << "FPGA TF  Checksum "<< std::hex << fpga_tf <<std::endl;
  exit(0);
  }
  
 
  storage::variant bank12;
  storage::variant sectorMap;
  FtkCool cool(dbConn,dbTag,run_number);
  cool.read(bank12,FtkCool::FtkConfigType::CORRGEN_RAW_12L_GCON,region);
  cool.read(sectorMap,FtkCool::FtkConfigType::SECTORS_RAW_8L_CONN,region);
  

  SsbFitConstants ssb(bank12,sectorMap);
  


  std::chrono::steady_clock::time_point read_start = std::chrono::steady_clock::now();
  std::chrono::steady_clock::time_point read_end = std::chrono::steady_clock::now();

  std::cout << "-> DB Read " 
            << std::chrono::duration_cast<std::chrono::milliseconds>(read_end-read_start).count() << " ms " << std::endl;
  std::chrono::steady_clock::time_point calc_start = std::chrono::steady_clock::now();
  ssb.calculate();
  std::chrono::steady_clock::time_point calc_end = std::chrono::steady_clock::now();

  std::cout << "-> Calculated EXP/TFK constants " 
            << std::chrono::duration_cast<std::chrono::milliseconds>(calc_end-calc_start).count() << " ms " << std::endl;
  if(outputDir!="") {
    std::chrono::steady_clock::time_point write_start = std::chrono::steady_clock::now();
    std::ofstream exp(outputDir+"/exp.out");
    std::ofstream tf(outputDir+"/tf.out");
      
    ssb.writeConstants(ssb.getEXPconstants(),exp);
    ssb.writeConstants(ssb.getTFconstants(),tf);

    std::chrono::steady_clock::time_point write_end = std::chrono::steady_clock::now();
    std::cout << "-> Wrote EXP/TFK constants " 
            << std::chrono::duration_cast<std::chrono::milliseconds>(write_end-write_start).count() << " ms " << std::endl;
  }
  if(fpga>=0 && slot >=0) {
    std::chrono::steady_clock::time_point vme_start = std::chrono::steady_clock::now();
    
  
    SSBVMEloader loader(slot,fpga);
    std::cout << "Using slot= " << slot << ", fpga=" << fpga << std::endl;
    const std::vector<uint32_t> &exp=ssb.getEXPconstants();
    const std::vector<uint32_t> &tkf=ssb.getTFconstants();

    std::cout << "EXP constants size=" <<exp.size() << std::endl;
    std::cout << "TKF constants size=" <<tkf.size() << std::endl;
    loader.loadEXP(exp);
    loader.loadTKF(tkf);
    std::chrono::steady_clock::time_point vme_end = std::chrono::steady_clock::now();
    std::cout << "-> Wrote VME constants " 
	      << std::chrono::duration_cast<std::chrono::milliseconds>(vme_end-vme_start).count() << " ms " << std::endl;

    

    proc.getHWChecksums(fpga_exp,fpga_tf);

    sw_exp=ssb.checkSumEXP();;
    sw_tf=ssb.checkSumTF();
    std::cout << "FPGA EXP Checksum "<< std::hex << fpga_exp <<std::endl;
    std::cout << "FPGA TF  Checksum "<< std::hex << fpga_tf <<std::endl;
    std::cout << "SW   EXP Checksum "<< std::hex << sw_exp <<std::endl;
    std::cout << "SW   TF  Checksum "<< std::hex << sw_tf <<std::endl;      
  

  }

  return 0;
}

