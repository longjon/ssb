#include "ftkvme/VMEInterface.h" 
#include "ftkvme/VMEManager.h" 
//#include "ssb/ssb_vme_addr.h"
#include "ssb/ssb.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>     // std::string, std::stoi



namespace daq 
{
  namespace ftk 
  {

    bool ssb_octopus_load(VMEInterface*& vme, const string& test_vector, const u_int tentacle_id, const u_int vector_id){

      std::vector<u_int> data;
      std::vector<u_int> addr;

      ifstream input1;
      string line;
      string data_word;
      string ctrl_word;

      u_int data_int; 
      u_int ctrl_int; 
      u_int addr_int; 

      stringstream stream;
      u_int counter; 


      if( test_vector!="" ){
        std::cout<< " >>>>  loading Tentacle=" << tentacle_id << "  TestVector="  << vector_id << "  <<<< " << test_vector.c_str() <<  std::endl;
        data.clear();
        input1.open(test_vector.c_str(), ios::in); 


	counter = 0;
	vme->write_word(0x010,0x1);    //Start the Octopus Load procedure in firmware  address "010"

        while( !input1.eof() ){

	  if(counter > 4095) break;

	  getline(input1, line);
	  ctrl_word = line.substr(8,1);      // Grab lowest 4 bits
	  // cout << line << endl;
	  // cout << ctrl_word << endl;

	  stream << std::hex << ctrl_word;
	  stream >> ctrl_int;                // Convert to Integer

	  
	  // Compute the address
	  // Tentacle_ID is bits [23:20] so its scaled by 2^20
	  // Vector_ID is bits [19:16] so its scaled by 2^16
	  // Word Number is bits [15:4] so its scaled by 2^4
	  // Ctrl Word is bits [3:0] so its not scaled
	  addr_int = tentacle_id*1048576 + vector_id*65536 + counter*16 + ctrl_int;
	  addr.push_back( addr_int );
	  //cout << "Ciao 1 " << endl;
	  vme->write_word(0x000, addr[0] );    // Send the address word,   needs to poke address "000"
	  stream.clear(); 
	  stream.str("");

	  data_word = line.substr(0,8);      // Grab the high 32 bits

	  //	  cout << "data " << data_word << "counter" << counter << endl;
	  stream << std::hex << data_word;
	  stream >> data_int;
	  data.push_back( data_int );
	  vme->write_word(0x004, data[0] );    // Send the data word,   needs to poke address "004"

	  
          data.clear();
          addr.clear();
	  stream.clear(); 
	  stream.str("");
	  counter++;
        }
        std::cout<< " >>>>  done loading Tentacle=" << tentacle_id << "  TestVector="  << vector_id << "  <<<< " <<  std::endl;
        data.clear(); 
        input1.close();
      }


      if(test_vector=="") {
	std::cout << "Please supply the test vector file" << std::endl;
	return false;
      }

      if(counter != 4096) {
	std::cout << "File only contained " << counter << " lines. Format requires 4096 lines." << std::endl;
	return false;
      }
      
      return true; 

    }//ssb_octopus_load
    
  } //namespcae daq
} //namespcae ftk


