#include "ftkvme/VMEInterface.h" 
#include "ftkvme/VMEManager.h" 
#include "ssb/ssb.h" 


#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>     // std::string, std::stoi

/*
 SSB VME address, 32 bits: 
    0000 1XXX XXXX XXXX 0001 0000 0000 0000
    ------             / --- --------------
    slot#           r/w  FPGA#            \ offset address
 */

int main(int argc, char *argv[]){

  using namespace  boost::program_options ;
  using namespace std;

  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("9"), "slot number")
    ("fpga", value< std::string >()->default_value("0"), "fpga id")
    ("auxpix", value< std::string >()->default_value(""), "SSMAP for PIX")
    ("auxsct", value< std::string >()->default_value(""), "SSMAP for SCT")
    ("dfpix", value< std::string >()->default_value(""), "SSMAP for PIX")
    ("dfsct", value< std::string >()->default_value(""), "SSMAP for SCT")
    ;

  positional_options_description p;

  variables_map vm;
  try
    {
      store(command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl;
      return 1;
    }

  notify(vm);

  if( vm.count("help") ) // if help is required, then desc is printed to output
    {
      std::cout << std::endl <<  desc << std::endl ;
      return 0;
    }

  string aux_pix_constant_file = vm["auxpix"].as<std::string>() ;
  string aux_sct_constant_file = vm["auxsct"].as<std::string>() ;
  string df_pix_constant_file = vm["dfpix"].as<std::string>() ;
  string df_sct_constant_file = vm["dfsct"].as<std::string>() ;
  u_int slot = std::stoi( vm["slot"].as<std::string>() );
  u_int fpga = std::stoi( vm["fpga"].as<std::string>() );

  std::cout << "Working on slot= " << slot << " fpga= " << fpga << std::endl;
  if( aux_pix_constant_file=="" && aux_sct_constant_file=="" && df_pix_constant_file=="" && df_sct_constant_file=="" ){
    std::cout << "Please supply the SSMAP file for pix and sct" << std::endl;
    return 0; 
  }

  VMEInterface *vme=VMEManager::global().ssb(slot,fpga); // always start with the base address of the ssb slot
  vme->disableBlockTransfers(false); // use real block transfer

  std::string name="";
  bool result = daq::ftk::ssb_ssmap_load_from_txt( vme, aux_pix_constant_file, aux_sct_constant_file, df_pix_constant_file, df_sct_constant_file, name); 

  return (int)result;
}
