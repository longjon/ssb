/*           Program to issue a routine for read the AUX RAM                   */
/*           Written for AMBFTK board v1.0   FTK collaboration                 */

#include <string>
#include <iostream>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ftkcommon/core.h"
#include "ssb/ssb_vme_regs.h"



namespace daq 
{
  namespace ftk 
  {
    bool ssb_vmetest( int slot, int fpga, u_int addr, u_int value  )
    { 
      int status;
      u_int ret, register_content;
      u_int vmeaddr, slotaddr, fpgaaddr;
      int handle;
      
      
      VME_MasterMap_t master_map ;
      
      slotaddr = ((slot&0x1f) << 27);
      fpgaaddr = ((fpga&0x1f) << 24);
      vmeaddr = slotaddr + fpgaaddr;

      //
      // Write random word
      //

      // Open connection
      std::cout << "Just trying to open " << std::endl;

      master_map.vmebus_address   = 0x20000000;
      master_map.window_size      = 0x1000000;
      master_map.address_modifier = VME_A32;
      master_map.options          = 0;

      ret = VME_Open();
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_Open' in function" );
	}

      std::cout << "Just trying to open map" << std::endl;

      ret = VME_MasterMap(&master_map, &handle);

      if (ret != VME_SUCCESS)
      	{
      	  VME_ErrorPrint(ret);
      	  FTK_VME_ERROR( "Failure when calling the function 'VME_MasterMap' in function"  );
      	}

      std::cout << "Should have opened map" << std::endl;


      // //set tmode rx stratix
      // status = VME_WriteSafeUInt(handle, TMODE, 0x1);
      // CHECK_ERROR(status);

      std::cout << "Writing random words " << value << std::endl;
      std::cout << "at this address " << addr << std::endl;

      // Write some random word
      status = VME_WriteSafeUInt(handle, addr, value);
      CHECK_ERROR(status);

      std::cout << "Should have written value " << value << std::endl;
      
      // //set tmode rx stratix
      // status = VME_WriteSafeUInt(handle, TMODE, 0x0);
      // CHECK_ERROR(status);

      
      // Close connection
      ret = VME_MasterUnmap(handle);
      if (ret != VME_SUCCESS)
      	{
      	  VME_ErrorPrint(ret);
      	  FTK_VME_ERROR( "Failure when calling the function 'VME_MasterUnMap' in function"  );
      	}
      
      ret = VME_Close();
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_Close' in function"  );	  
	}

      //
      // Read back!
      //

      // Open connection
      ret = VME_Open();
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_Open' in function" );
	}

      master_map.vmebus_address   = 0x20000000;
      master_map.window_size      = 0x1000000;
      master_map.address_modifier = VME_A32;
      master_map.options          = 0;

      ret = VME_MasterMap(&master_map, &handle);
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_MasterMap' in function"  );
	}
      
      // //set tmode rx stratix
      // status = VME_WriteSafeUInt(handle, TMODE, 0x1);
      // CHECK_ERROR(status);

      // Read back the random word
      status = VME_ReadSafeUInt(handle, addr, &register_content);
      CHECK_ERROR(status);
      
      
      // //set tmode rx stratix
      // status = VME_WriteSafeUInt(handle, TMODE, 0x0);
      // CHECK_ERROR(status);
     
      // Close connection
      ret = VME_MasterUnmap(handle);
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_MasterUnMap' in function"  );	  
	}
      
      ret = VME_Close();
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_Close' in function"  );	 
	}

      //
      // Error checking
      //
      std::cout << register_content << std::endl;

      if(value==register_content) return 0;
      
      PRINT_LOG("ERROR FPGA: " << fpga << "\tADDR: " << std::hex << addr << " (0x" << vmeaddr+addr <<")" << "\tWROTE: " << std::hex << value << "\tREAD: " << std::hex << register_content);

      return -1;
      
    }
    
  } //namespcae daq
} //namespcae ftk


#ifdef STANDALONE
/////////////////////////////////////////////////////////////////////////////
// Stand alone application
//   - Compiled when -DSTANDALONE
/////////////////////////////////////////////////////////////////////////////

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>


/** Parsing input parmaeters and calling the am_init function
 *
 */
int main(int argc, char **argv) 
{ 
 
  using namespace  boost::program_options ;
  
  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("4"), "The card slot")
    ("fpga", value< std::string >()->default_value("7000"), "The FPGA ID (-1 for random)")
    ("addr", value< std::string >()->default_value("-1"), "The VME address (-1 for random)")
    ("n", value< std::string >()->default_value("1"), "Number of tests to run")
    ;
  
  variables_map vm;
  try 
    {
      store(command_line_parser(argc, argv).options(desc).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl; 
      return 1;
    }
  
  notify(vm);
  
  if( vm.count("help") ) // if help is required, then desc is printed to output
    {	
      std::cout << std::endl <<  desc << std::endl ; 
      return 0;	
    }
  
  int n = daq::ftk::string_to_int( vm["n"].as<std::string>() );
  int slot = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  int fpga = daq::ftk::string_to_int( vm["fpga"].as<std::string>() );
  int addr = daq::ftk::string_to_int( vm["addr"].as<std::string>() );
  u_int value;

  // bool doRandomFPGA = (fpga==-1);
  // bool doRandomAddr = (addr==-1);

  // int good[48];
  // int fail[48];
  // for(int i=0;i<48;i++)
  //   {
  //     good[i]=0;
  //     fail[i]=0;
  //   }

  // for(int i=0;i<n;i++)
  //   {
  //     if(doRandomFPGA) fpga = 3 + (rand() % 4);

  //     if(doRandomAddr) addr = (1+rand()%8)<<4;

  //     value = ((rand()%16)<<0)
  // 	+ ((rand()%16)<<4)
  // 	+ ((rand()%16)<<8)
  // 	+ ((rand()%16)<<12)
  // 	+ ((rand()%16)<<16)
  // 	+ ((rand()%16)<<20)
  // 	+ ((rand()%16)<<24)
  // 	+ ((rand()%16)<<28);

  //     if(daq::ftk::ssb_vmetest(slot, fpga, addr, value, temp)==0)
  // 	good[(fpga-1)*8+(addr>>4)-1]++;
  //     else
  // 	fail[(fpga-1)*8+(addr>>4)-1]++;
  //   }

  // for(fpga=0;fpga<6;fpga++)
  //   {
  //     std::cout << "FPGA: " << fpga+1 << std::endl;
  //     for(addr=0;addr<8;addr++)
  // 	{
  // 	  std::cout << "0x" << std::hex << ((addr+1)<<4) << ":\t" << good[fpga*8+addr] << "\t" << fail[fpga*8+addr] << std::endl;
  // 	}
  //   }
  value = 3215;
  addr =0;

  if(  daq::ftk::ssb_vmetest(slot, fpga, addr, value)==0)  std::cout << " Ok " << std::endl;



    return 0;
}
#endif
