#include "ftkvme/VMEInterface.h" 
#include "ftkvme/VMEManager.h" 
#include "ssb/ssb.h" 


#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>     // std::string, std::stoi

/*
 SSB VME address, 32 bits: 
    0000 1XXX XXXX XXXX 0001 0000 0000 0000
    ------             / --- --------------
    slot#           r/w  FPGA#            \ offset address
 */

int main(int argc, char *argv[]){

  using namespace  boost::program_options ;
  using namespace std;

  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("6"), "slot number")
    ("fpga", value< std::string >()->default_value("4"), "fpga id")
    ("tentacle", value< std::string >()->default_value("0"), "tentacle id")
    ("vector", value< std::string >()->default_value("0"), "vector id")
    ("file", value< std::string >()->default_value(""), "file to load")
    ;

  positional_options_description p;

  variables_map vm;
  try
    {
      store(command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl;
      return 1;
    }

  notify(vm);

  if( vm.count("help") ) // if help is required, then desc is printed to output
    {
      std::cout << std::endl <<  desc << std::endl ;
      return 0;
    }

  string test_vector_file = vm["file"].as<std::string>() ;
  u_int slot = std::stoi( vm["slot"].as<std::string>() );
  u_int fpga = std::stoi( vm["fpga"].as<std::string>() );
  u_int t_id = std::stoi( vm["tentacle"].as<std::string>() );
  u_int v_id = std::stoi( vm["vector"].as<std::string>() );

  std::cout << "Working on slot= " << slot << " fpga= " << fpga << std::endl;
  if( test_vector_file=="" ){
    std::cout << "Please supply the test vector file" << std::endl;
    return 0; 
  }

  VMEInterface *vme=VMEManager::global().ssb(slot,fpga); // always start with the base address of the ssb slot
  vme->disableBlockTransfers(false); // use real block transfer

  bool result = daq::ftk::ssb_octopus_load( vme, test_vector_file, t_id, v_id); 

  return (int)result;
}
