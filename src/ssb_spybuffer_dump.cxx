#include "ftkvme/VMEInterface.h" 
#include "ftkvme/VMEManager.h" 
#include "ftkcommon/exceptions.h"
#include "ssb/ssb_vme_addr.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>     // std::string, std::stoi

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

using namespace std;

/*
 SSB VME address, 32 bits: 
    0000 1XXX XXXX XXXX 0001 0000 0000 0000
    ------             / --- --------------
    slot#           r/w  FPGA#            \ offset address
 */

int main(int argc, char *argv[]){

    using namespace  boost::program_options ;

    //  Parsing parameters using namespace boost::program:options
    //

    options_description desc("Allowed options");
    desc.add_options()
        ("help", "produce help message")
        ("slot", value< std::string >()->default_value("9"), "The card slot.")
        ("fpga", value< std::string >()->default_value("0"), "The FPGA ID.")
        ("addr", value< std::string >()->default_value("0x500"), "The offset address for spy buffer.")
        ("nSPY", value< std::string >()->default_value("10"), "Total number of spy buffers.")
        ("nBLT", value< std::string >()->default_value("64"), "Total number of blocks to read.")
        ("nWords", value< std::string >()->default_value("64"), "Words per block read.")
        ("hmode", "Histogram mode.")
        ;
    positional_options_description p;
    variables_map vm;
    try
    {
        store(command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    }
    catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
        std::cerr << desc << std::endl;
        return 1;
    }

    notify(vm);

    if( vm.count("help") ) // if help is required, then desc is printed to output
    {
        std::cout << std::endl <<  desc << std::endl ;
        return 0;
    }


    u_int slot = std::stoi( vm["slot"].as<std::string>() );
    u_int fpga = std::stoi( vm["fpga"].as<std::string>() );
    u_int nSPY = std::stoi( vm["nSPY"].as<std::string>() );
    u_int nWords = std::stoi( vm["nWords"].as<std::string>() ); // per block transfer 
    u_int nBLT   = std::stoi( vm["nBLT"].as<std::string>() ); // per spy buffer


    //u_int addr = std::stoi( vm["addr"].as<std::string>() );
    std::stringstream ss; 
    ss << vm["addr"].as<std::string>(); 
    u_int addr = SSB_ADDR_SPYBUFFER; 
    ss >> std::hex >> addr; 

    // Block read the spy buffer address space 
    //
    if ( vm.count("hmode") )
        ERS_LOG("Dump histogram on slot " << slot <<" and fpga " << fpga << " : " );
    else
        ERS_LOG("Dump the spybuffers on slot " << slot <<" and fpga " << fpga << " : " );

    VMEInterface *vme=VMEManager::global().ssb(slot,fpga); // always start with the base address of the ssb slot
    vme->disableBlockTransfers(false); // use real block transfer

    std::vector<u_int> data; 
    std::vector<u_int> data_temp; 

    // big block of try-catch...
    try{
        vme->write_word( addr, 0x1);  // vme command to trigger spybuffer freeze
        
        for(u_int iblt=0; iblt<nBLT*nSPY; iblt++){
            data_temp.clear();
            try {
                data_temp=vme->read_block(addr, nWords);
            } catch (daq::ftk::VmeError& e) {
                std::cout <<"Caught error in block read!"<<std::endl;
                throw e;
            }
            data.insert( data.end(), data_temp.begin(), data_temp.end() );
        }
        
        
        for(u_int i=0; i<nBLT*nWords; i++){ // the first word should be thrown away due to latency issue
            if ( vm.count("hmode") )
            {
                //if ( data[i] )
                std::cout<< "word " << std::setw(4) << std::setfill('0') << std::dec << i <<": 0x" << std::setw(8) << std::setfill('0') << std::right <<std::hex << data[i]; 
            } else {
                for(u_int ispy=0; ispy<nSPY; ispy++){
                    std::cout<< "word " << std::setw(4) << std::setfill('0') << std::dec << i <<": 0x" << std::setw(8) << std::setfill('0') << std::right <<std::hex << data[i+ispy*nBLT*nWords] <<"  "; 
                }
                //std::cout<<std::endl;
            }
            std::cout<<std::endl;
        }
        
        // Unfreeze the spybuffer
        if (fpga < 4) vme->write_word( SSB_EXTF_SPYBUFFER_UNFREEZE, 0x1);  
        else          vme->write_word( SSB_HW_SPYBUFFER_UNFREEZE, 0x1);  

    } catch (daq::ftk::VmeError& e) {
        stringstream ss;
        ss <<  "Caught VME error when trying to dump spybuffer for FPGA " << fpga <<" in slot "<<slot;
        std::cout<< ss.str() <<std::endl;
    }
    
    return 0;
}
