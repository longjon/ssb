#include "ssb/ssb_fpga_hitwarrior.h"
#include "ssb/ssb_vme_addr.h"

#include <chrono>
#include <thread>


namespace daq
{
  namespace ftk
  {

      ssb_fpga_hitwarrior::ssb_fpga_hitwarrior(uint slot, uint fpga) :
          ssb_fpga(slot, fpga)
      {
          ERS_LOG("Constructing ssb_fpga_hitwarrior with slot " << slot << " and fpga " << fpga );
      }
      
      ssb_fpga_hitwarrior::~ssb_fpga_hitwarrior()
      {
          ERS_LOG("Destructor of ssb_fpga_hitwarrior called" );
      }
      
      
      
      void ssb_fpga_hitwarrior::connect()
      {
          ERS_LOG("ssb_fpga_hitwarrior::connect()" );
        
          // VME Access try-catch loop                                                                    
          for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
          {
              try
              {                
                  m_vme->write_word( SSB_ADDR_CONNECT, SSB_COMMAND); 
              }
              catch (daq::ftk::VmeError& e)
              {
                  if (i == SSB_MAX_VME_RETRIES)
                  {
                      std::stringstream message;
                      message << "SSB failed to send connect command to FPGA.";
                      daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                      throw excp;
                  }
                  continue;
              }
              catch (daq::ftk::CMEMError& e)
              {
                  if (i == SSB_MAX_VME_RETRIES)
                  {
                      std::stringstream message;
                      message << "SSB failed to send connect command to FPGA.";
                      daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                      throw excp;
                  }
                  continue;
              }
              break;
          }
      }

      void ssb_fpga_hitwarrior::prepareForRun()
      {
          ERS_LOG("ssb_fpga_hitwarrior::prepareForRun()" );

          // VME Access try-catch loop                                                                    
          for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
          {
              try
              {                
                  m_vme->write_word( SSB_ADDR_START, SSB_COMMAND); 
              }
              catch (daq::ftk::VmeError& e)
              {
                  if (i == SSB_MAX_VME_RETRIES)
                  {
                      std::stringstream message;
                      message << "SSB failed to send prepareForRun command to FPGA.";
                      daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                      throw excp;
                  }
                  continue;
              }
              catch (daq::ftk::CMEMError& e)
              {
                  if (i == SSB_MAX_VME_RETRIES)
                  {
                      std::stringstream message;
                      message << "SSB failed to send prepareForRun command to FPGA.";
                      daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                      throw excp;
                  }
                  continue;
              }
              break;
          }
      }

      void ssb_fpga_hitwarrior::stop()
      {
          ERS_LOG("ssb_fpga_hitwarrior::stop()" );

          // VME Access try-catch loop                                                                                                                                                                               
          for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
          {
              try
              {
                  m_vme->write_word( SSB_ADDR_STOP, SSB_COMMAND); 
              }
              catch (daq::ftk::VmeError& e)
              {
                  if (i == SSB_MAX_VME_RETRIES)
                  {
                      std::stringstream message;
                      message << "SSB failed to send stop command to FPGA.";
                      daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                      throw excp;
                  }
                  continue;
              }
              catch (daq::ftk::CMEMError& e)
              {
                  if (i == SSB_MAX_VME_RETRIES)
                  {
                      std::stringstream message;
                      message << "SSB failed to send stop command to FPGA.";
                      daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                      throw excp;
                  }
                  continue;
              }
              break;
          }      
      }

      void ssb_fpga_hitwarrior::disconnect()
      {
          ERS_LOG("ssb_fpga_hitwarrior::disconnect()" );

          // VME Access try-catch loop                                                                                                                                                                               
          for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
          {
              try
              {      
                  m_vme->write_word( SSB_ADDR_DISCONNECT, SSB_COMMAND); 
              }
              catch (daq::ftk::VmeError& e)
              {
                  if (i == SSB_MAX_VME_RETRIES)
                  {
                      std::stringstream message;
                      message << "SSB failed to send disconnect command to FPGA.";
                      daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                      throw excp;
                  }
                  continue;
              }
              catch (daq::ftk::CMEMError& e)
              {
                  if (i == SSB_MAX_VME_RETRIES)
                  {
                      std::stringstream message;
                      message << "SSB failed to send disconnect command to FPGA.";
                      daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                      throw excp;
                  }
                  continue;
              }
              break;
          }
      }

      void ssb_fpga_hitwarrior::unconfigure()
      {
          ERS_LOG("ssb_fpga_hitwarrior::unconfigure()" );

          // VME Access try-catch loop                                                                                                                                                                               
          for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
          {
              try
              {
                  m_vme->write_word( SSB_ADDR_UNCONFIG, SSB_COMMAND); 
              }
              catch (daq::ftk::VmeError& e)
              {
                  if (i == SSB_MAX_VME_RETRIES)
                  {
                      std::stringstream message;
                      message << "SSB failed to send unconfigure command to FPGA.";
                      daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                      throw excp;
                  }
                  continue;
              }
              catch (daq::ftk::CMEMError& e)
              {
                  if (i == SSB_MAX_VME_RETRIES)
                  {
                      std::stringstream message;
                      message << "SSB failed to send unconfigure command to FPGA.";
                      daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                      throw excp;
                  }
                  continue;
              }
              break;
          }
      }

      void ssb_fpga_hitwarrior::configure(unsigned int runNumber, std::map<uint32_t, std::unique_ptr<SsbFitConstants> >& db_constants, bool UseCool)
      {
          ERS_LOG("ssb_fpga_hitwarrior::configure()" );

          // VME Access try-catch loop                                                                                                                                                                               
          for (uint i = 0; i <= SSB_MAX_VME_RETRIES; i++)
          {
              try
              {
                  m_vme->write_word( SSB_ADDR_CONFIG, SSB_COMMAND); // This line triggers internal state transitions in the firmware. The FIFOs, state machines, GTX links are reset upon receiving this vme command. 

                  std::this_thread::sleep_for(std::chrono::milliseconds(100)); // make sure internal fpga configure finishes, this is a guess of the time based on the mem init working in the EXTF
                  
                  if(m_conf->get_IgnoreBP()){
                      ERS_LOG("ssb_fpga_hitwarrior::Setting Ignore Bits" );
                      m_vme->write_word(SSB_ADDR_IGNOREBP, 0x1);
                  }
                  else {
                      m_vme->write_word(SSB_ADDR_IGNOREBP, 0x0);
                  }

                  m_vme->write_word(SSB_HW_FREEZE_MENU, m_conf->get_FreezeMask() );
                  m_vme->write_word(SSB_HW_FREEZE_TRACK_MODE, m_conf->get_HW_TrackMode() );
                  m_vme->write_word(SSB_HW_FREEZE_NTRACK_MIN, m_conf->get_HW_TrackMin() );
                  m_vme->write_word(SSB_HW_FREEZE_NTRACK_MAX, m_conf->get_HW_TrackMax() );
                  m_vme->write_word(SSB_HW_FREEZE_NTRACK_NOT, m_conf->get_HW_TrackNotEqualTo() );
                  
                  ERS_LOG("ssb_fpga_hitwarrior:: Setting EXTF input mask" );
                  m_vme->write_word(SSB_ADDR_NINPUTSHW, m_conf->get_HWInputMask());
              }
              catch (daq::ftk::VmeError& e)
              {
                  if (i == SSB_MAX_VME_RETRIES)
                  {
                      std::stringstream message;
                      message << "SSB failed to send configure command to FPGA.";
                      daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                      throw excp;
                  }
                  continue;
              }
              catch (daq::ftk::CMEMError& e)
              {
                  if (i == SSB_MAX_VME_RETRIES)
                  {
                      std::stringstream message;
                      message << "SSB failed to send configure command to FPGA.";
                      daq::ftk::ftkException excp(ERS_HERE, name_ftk(), message.str(), e);
                      throw excp;
                  }
                  continue;
              }
              break;
          }	

      }

      bool ssb_fpga_hitwarrior::check_fw_version(uint32_t version_number)
      {
          ERS_LOG("ssb_fpga_hitwarrior::check_fw_version(" << version_number << ")" );
          return version_number;
      }

      void ssb_fpga_hitwarrior:: set_upstream_flow(bool enable)
      {
          ERS_LOG("ssb_fpga_hitwarrior::set_upstream_flow(" << enable << ")" );
      }

      void ssb_fpga_hitwarrior:: set_downstream_flow(bool enable)
      {
          ERS_LOG("ssb_fpga_hitwarrior::set_downstream_flow(" << enable << ")" );
      }

      void ssb_fpga_hitwarrior::setup_high_speed()
      {
          ERS_LOG("ssb_fpga_hitwarrior::setup_high_speed()" );
      }

      void ssb_fpga_hitwarrior::rx_link_idle()
      {
          ERS_LOG("ssb_fpga_hitwarrior::rx_link_idle()" );
      }

      void ssb_fpga_hitwarrior::config_ram_lut()
      {
          ERS_LOG("ssb_fpga_hitwarrior::config_ram_lut()" );
      }

      void ssb_fpga_hitwarrior::reset()
      {
          ERS_LOG("ssb_fpga_hitwarrior::reset()" );
      }

      uint ssb_fpga_hitwarrior::getRCState()
      {
          return -1;
      }

  }
}
