#ifndef SSB_FPGA_H
#define SSB_FPGA_H

#include "ssb/ssb.h"
#include "ssb/dal/Ssb_Fpga.h"
#include "ssb/ssb_vme_addr.h"
#include "ssb/SSBSpybuffer.h"
#include "ssb/ssb_defines.h"
#include <ssb/SsbFitConstants.hh>

#include "ftkvme/VMEInterface.h"
#include "ftkvme/VMEManager.h"



/*
Base class to describe an FPGA of the SSB
 */

namespace daq
{
  namespace ftk
  {

    class ssb_fpga
    {

    public:
      // constructor
      ssb_fpga(uint slot, uint fpgaId);

      // load real configuration
      // we probably need to pass a tag or a config name

      virtual ~ssb_fpga();

      std::string name_ftk(){return m_name;};
      virtual void setup(const dal::Ssb_Fpga *conf) {m_conf=conf; };

      // set the configuration
      // it might be useful to return the status of the action
      // we might also need some parameters, since we will most probably
      // send only the checksum
      virtual void configure(unsigned int runNumber, std::map<uint32_t, std::unique_ptr<SsbFitConstants> >& db_constants, bool useCool=false) { };

      virtual void connect() { };

      virtual void prepareForRun() { };

      virtual void stop() { };

      virtual void disconnect() { };

      virtual void unconfigure() { };
      
      virtual void set_LinkID(uint8_t LinkID)     {m_LinkID=LinkID; };

      virtual void set_LoadTFConstants(bool LoadTFConstants) {m_LoadTFConstants = LoadTFConstants;};
      virtual bool get_LoadTFConstants() {return m_LoadTFConstants;};

      virtual void set_LoadEXPConstants(bool LoadEXPConstants) {m_LoadEXPConstants = LoadEXPConstants;};
      virtual bool get_LoadEXPConstants() {return m_LoadEXPConstants;};

      // used to disable communication to downstream
      // and to tell aux and df I am ready to take data
      // enable = true --> connection up
      // enable = false --> connection down
      ////virtual void set_upstream_flow(bool enable) { };

      // disable communication from upstream
      ////virtual void set_downstream_flow(bool enable) { };

      // start s-link protocol
      ////virtual void set_slink() { };

      // setup high speed link
      ////virtual void setup_high_speed() { };

      // check RX links are receiving idle and are aligned
      ////virtual void rx_link_idle() { };

      // check link status
      virtual bool getLinkStatus(u_int &linkStatus) { return true; };

      // configure RAMs LUT???
      virtual void config_ram_lut() { };

      // reset all registers?
      virtual void reset() { };
     

      // Write 0s to the memory
      virtual bool initializeMemory(){return true;};
      virtual bool initializeTFMemory(){return true;};
      virtual bool initializeEXPMemory(){return true;};

      // load the constants
      virtual bool loadTFConstants(){return true;};
      virtual bool loadEXPConstants(){return true;};
      
      // Is this object a hitwarrior
      virtual bool isHitWarrior() {return false;}; // defined as true in HW

      // Get FW version
      uint32_t getFWHash(); // hash from board
      uint32_t getFWDate(); // hash from board
      uint32_t getFWHashOKS(){return m_conf->get_FWHash();}; // hash from oks
      uint32_t getFWDateOKS(){return m_conf->get_FWDate();}; // hash from oks
      uint32_t getFWFreq();
      virtual std::string get_EXPConf(){return m_conf->get_EXPPath();};
      virtual std::string get_TFConf(){return m_conf->get_TFPath();};
      uint32_t getTower(){return m_conf->get_Tower();};

      virtual void getHWChecksums(uint32_t &EXP_Checksum, uint32_t &TF_Checksum ){EXP_Checksum=0; TF_Checksum=0; }; // hardware checksum
      virtual void getSWChecksums(uint32_t &EXPChecksum, uint32_t &TFChecksum, bool useCool=false){TFChecksum=0; EXPChecksum=0;};

      // Spybuffers

      void freezeSpybuffer();
      void unfreezeSpybuffer(bool force = false);
      void freezeSRSB();
      void unfreezeSRSB(bool force = false);
      std::shared_ptr< ssb_spyBuffer > getSpybuffer(); 
      std::shared_ptr< ssb_spyBuffer > getSpybufferSSB();
      std::shared_ptr< ssb_spyBuffer > getSpybuffer_histo(uint32_t addr, u_int nWords=16, u_int nBLT=64);
      std::vector<uint32_t> getSRSB(uint32_t addr, u_int nWords=16, u_int nBLT=1); // should be pass by reference, but causes crashes, todo make it a shared pointer?
      std::shared_ptr< ssb_spyBuffer > getSRSB_emon(uint32_t addr, u_int nWords=16, u_int nBLT=1);

      void setName(std::string name){m_name = name;};
      
      uint get_m_fpgaID(){return m_fpgaId;};

      bool isOldFW(){return m_FWDate<SSB_OLD_FW;};

      virtual uint getRCState(){ return -1; };
      

    protected:
      std::string m_name;
      const dal::Ssb_Fpga *m_conf;
      uint m_slot;
      uint m_fpgaId;
      bool m_LoadEXPConstants;
      bool m_LoadTFConstants;
      uint16_t m_LinkID;
      uint16_t  m_FREQ;
      uint32_t m_FWDate;

      VMEInterface *m_vme; // vme interface

    };
  }
}

#endif
