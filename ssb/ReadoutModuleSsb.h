/********************************************************/
/*                                                      */
/********************************************************/

#ifndef READOUT_MODULE_SSB_H
#define READOUT_MODULE_SSB_H 

#include <string>
#include <vector>

#include "ipc/partition.h"
#include "ipc/core.h"
#include "is/info.h"
#include "oh/OHRootProvider.h"
#include "ssb/dal/ssbNamed.h"
#include "ssb/ssb_board.h"
//common DataFlow monitoring
#include "ssb/StatusRegisterSsbFactory.h"
#include "ssb/DataFlowReaderSsb.h"
#include "oh/OHRawProvider.h"

#include "ftkcommon/FtkEMonDataOut.h"
#include "ftkcommon/ReadoutModule_FTK.h"

namespace daq {
namespace ftk {

  class ReadoutModule_Ssb : public ReadoutModule_FTK
  {
  public:

    /// Constructor (NB: executed at CONFIGURE transition)
    ReadoutModule_Ssb();
 
    /// Destructor (NB: executed at UNCONFIGURE transition)
    virtual ~ReadoutModule_Ssb() noexcept;
  
  public: // overloaded methods inherited from ROS::ReadoutModule or ROS::IOMPlugin
    
    /// Set internal variables (NB: executed at CONFIGURE transition)
    virtual void setup(DFCountedPointer<ROS::Config> configuration) override;
    
    /// Reset internal statistics
    virtual void clearInfo() override;
   
    /// Get the list of channels connected to this ReadoutModule
    /// Used for monitoring. It starts at connect and gets data via VME
    virtual const std::vector<ROS::DataChannel *> *channels() override; 

    /// Set the Run Parameters
    //virtual void setRunConfiguration(DFCountedPointer<Config> runConfiguration);
    
    /// Get values of statistical counters
    // virtual DFCountedPointer<Config> getInfo();

    /// Get access to statistics stored in ISInfo class
    // virtual ISInfo* getISInfo();



  public: // overloaded methods inherited from RunControl::Controllable

    /// RC configure transition
    virtual void doConfigure(const daq::rc::TransitionCmd& cmd) override;
    
    /// RC connect transition
    virtual void doConnect(const daq::rc::TransitionCmd& cmd) override;

    /// RC checkConnect transition
    virtual void checkConfig(const daq::rc::SubTransitionCmd& cmd) override;

    /// RC start of run transition
    virtual void doPrepareForRun(const daq::rc::TransitionCmd& cmd) override;

    /// RC startNoDF subtransition
    virtual void startNoDF(const daq::rc::SubTransitionCmd& cmd) override;

    /// RC stop transition
    virtual void doStopDC(const daq::rc::TransitionCmd& cmd) override {}
    virtual void doStopROIB(const daq::rc::TransitionCmd& cmd) override {}
    virtual void doStopHLT(const daq::rc::TransitionCmd& cmd) override {}
    virtual void doStopRecording(const daq::rc::TransitionCmd& cmd) override;
    virtual void doStopGathering(const daq::rc::TransitionCmd& cmd) override {}
    virtual void doStopArchiving(const daq::rc::TransitionCmd& cmd) override {}
    
    /// RC unconfigure transition 
    virtual void doUnconfigure(const daq::rc::TransitionCmd& cmd) override;
    
    /// RC disconnect transition: 
    virtual void doDisconnect(const daq::rc::TransitionCmd& cmd) override;

    /// RC subtransition (IF NEEDED)
    // virtual void subTransition(const daq::rc::SubTransitionCmd& cmd) override;
    
    // Method called periodically by RC
    // if needed
    virtual void doPublish( uint32_t flag, bool finalPublish  ) override;

    // Method called periodically by RC with a longer interval
    virtual void doPublishFullStats( uint32_t flag ) override;
    
    /// RC HW re-synchronization request
    virtual void resynch(const daq::rc::ResynchCmd& cmd);

    // Function that returns the name of the ReadoutModule
    std::string name_ftk(){return m_name;};

  private:
    
    std::unique_ptr<ssbNamed>        m_ssbNamed;      // Access IS via schema
    
    std::vector<ROS::DataChannel*>   m_dataChannels;   
    DFCountedPointer<ROS::Config>    m_configuration; // Configuration Object, Map Wrapper
    std::string                      m_isServerName;  // IS Server
    IPCPartition                     m_ipcpartition;  // Partition 
    uint32_t                         m_runNumber;     // Run Number
    bool                             m_dryRun;        // Bypass VME calls
    bool                             m_ReadHistosForPublish; // Read spybuffer histos in publish
    bool                             m_PublishOnlyNonZeroBins; // publish only non-zero bins
    bool                             m_ispublish; // Used to prevent publishing until end of prepare for run.
    bool                             m_doPublishFullStats; // Turns off just publish full stats
    std::unique_ptr<OHRootProvider>  m_ohProvider;    // Histogram provider  
    std::unique_ptr<TH1F>            m_histogram;     // Histogram
    std::string                      m_appName;
    ISInfoDictionary m_isInfoDict;
    // common DataFlow monitoring
    std::unique_ptr<StatusRegisterSsbFactory> m_statusRegisterSsbFactory;
    std::unique_ptr<DataFlowReaderSsb> m_dfReaderSsb;   
    std::shared_ptr<OHRawProvider<>> m_ohRawProvider;    ///< Raw histogram provider (needed for DataFlowReader)
    daq::ftk::FtkEMonDataOut *m_emonDataOut;
    
    // SPECIFIC ONES
    std::unique_ptr<ftk::ssb_board> m_board;   // pointer to board
    std::string     m_name;    // board name
    uint        m_slot;    // slot number
    uint        m_crate;    // crate number
    uint        m_LinkID;    // LinkID for track header
};

  // The DataChannels are used to get samples while running via the VME. 
  // For example they can be used for monitoring porpuses
  inline const std::vector<ROS::DataChannel *> *ReadoutModule_Ssb::channels()
  {
    return &m_dataChannels;
  }

} // namespace ftk
} // namespace daq
#endif // READOUT_MODULE_SSB_H
 
