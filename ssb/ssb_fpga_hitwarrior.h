#ifndef SSB_FPGA_HITWARRIOR_H
#define SSB_FPGA_HITWARRIOR_H

#include "ssb/ssb_fpga.h"


namespace daq
{
  namespace ftk
  {
    
    class ssb_fpga_hitwarrior: public ssb_fpga
    {
    public:
      ssb_fpga_hitwarrior(uint slot, uint fpga);
      ~ssb_fpga_hitwarrior();

      void load_config();

      void configure(unsigned int runNumber, std::map<uint32_t, std::unique_ptr<SsbFitConstants> >& db_constants, bool UseCool = false) override;
      void connect() override;
      void prepareForRun() override;
      void stop() override;
      void disconnect() override;
      void unconfigure() override;

      bool check_fw_version(uint32_t version_number);
      void set_upstream_flow(bool enable);
      void set_downstream_flow(bool enable);
      void setup_high_speed();
      void rx_link_idle();
      void config_ram_lut();
      void reset();
      bool isHitWarrior() {return true;};

      void unfreezeSpybuffer(){ m_vme->write_word(SSB_HW_SPYBUFFER_UNFREEZE , 0x1); };

      uint getRCState();
    };
  }
}

#endif
