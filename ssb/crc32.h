#ifndef CRC32_h
#define CRC32_h

#include <cstdint>

uint32_t
CRC32(uint8_t *data, int len, uint32_t crc);

#endif
