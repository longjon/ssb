#ifndef SSB_VME_ADDR_H
#define SSB_VME_ADDR_H


/*
  

 */

// SSB VME registers
#define SSB_VME_ADDR         0x500


// Run Control State Transition addresses for the commands

#define SSB_ADDR_CONFIG     0x600
#define SSB_ADDR_CONNECT    0x604
#define SSB_ADDR_START      0x608
#define SSB_ADDR_STOP       0x60C
#define SSB_ADDR_DISCONNECT 0x610
#define SSB_ADDR_UNCONFIG   0x614

#define SSB_COMMAND 0x0000000 // any word to be written to the above address
                              // will execute the commmand

#define SSB_ADDR_EXPCONST_BLOCK    0x200 // to write and read via block transfer
#define SSB_ADDR_EXPCONST_SECTORID 0x0FC // write to this address the sector ID to be block read out 
#define SSB_ADDR_TKFCONST_BLOCK    0x300 // to write and read via block transfer
#define SSB_ADDR_TKFCONST_SECTORID 0x0F8 // write to this address the sector ID to be block read out 


// configuration
#define SSB_ADDR_IGNOREBP       0x618
#define SSB_ADDR_SLINKMODE      0x618
#define SSB_ADDR_EXT_IGNOREBPHW 0x624
#define SSB_ADDR_NINPUTSHW      0x61c
#define SSB_EXTF_AUXDUP_REMOVAL 0x730
#define SSB_EXTF_LINKID         0x770
#define SSB_EXTF_IGNOREDFB      0x61c
#define SSB_EXTF_IGNOREAUXB     0x62c
#define SSB_EXTF_TF_TRACKLIMITS    0x76c   // TF fit limits
#define SSB_EXTF_AUXEXP_TRACKLIMITS    0x768   // AUX and EXP track limits
#define SSB_EXTF_DIVISOR_ETA_IBL 0X710  
#define SSB_EXTF_DIVISOR_PHI_IBL 0X714  
#define SSB_EXTF_DIVISOR_PHI_SCT 0X718 
#define SSB_EXTF_NSSID_PHI_IBL   0X71c 
#define SSB_EXTF_NSSID_SCT       0X720 
#define SSB_EXTF_CHI2_CUT        0X774   // cut on all tracks out of TF
#define SSB_EXTF_REC_CHI2_CUT    0X778   // cut on nom track to go to recovery

// Checksums
#define SSB_EXP_MEM_INIT    0x43c // write 0s to memory, and check if done
#define SSB_DO_EXP_CHECKSUM 0x438
#define SSB_EXP_CHECKSUM    0x68c  // hw computed checksum
#define SSB_TF_MEM_INIT     0x45c // write 0s to memory, and check if done
#define SSB_DO_TF_CHECKSUM  0x458
#define SSB_TF_CHECKSUM     0x6bc  // hw computed checksum


/********************
Status registers (Monitoring) and other commands
*********************/

//// spy buffers
#define SSB_ADDR_SPYBUFFER 0x500 // write to this address to freeze the spy buffers before reading 
                                 // block read this address for spy buffer data 
#define SSB_ADDR_SRSB 0xa00
#define SSB_SRSB_FREEZE   0x504
#define SSB_SRSB_UNFREEZE 0x508

#define SSB_HW_SRSB          0x400 
#define SSB_HW_SRSB_FREEZE   0xd00 
#define SSB_HW_SRSB_UNFREEZE 0xd04 

//// Freeze menu
// EXTF
#define SSB_EXTF_SPYBUFFER_UNFREEZE 0x72c
#define SSB_EXTF_BP_FREEZE_TRIGGER  0x728
#define SSB_EXTF_FREEZE_MENU        0x734 // write the menu here, and read the freeze reason
#define SSB_EXTF_TFDFB_DELAY        0x738
#define SSB_EXTF_AUX_DELAY          0x73c

#define SSB_EXTF_FREEZE_DFPACKETLENGTH 0x740
#define SSB_EXTF_FREEZE_AUXTRACKCOUNT  0x744
#define SSB_EXTF_FREEZE_TFTRACKCOUNT   0x748
#define SSB_EXTF_FREEZE_EXPOUTSIZE     0x74c
#define SSB_EXTF_TIMEOUT_THRESH 0x780

// HW
#define SSB_HW_SPYBUFFER_UNFREEZE 0x620
#define SSB_HW_FREEZE_MENU        0x734 // write the menu here, and read the freeze reason
#define SSB_HW_FREEZE_TRACK_MODE  0xd14 
#define SSB_HW_FREEZE_NTRACK_MIN  0xd1c // >=
#define SSB_HW_FREEZE_NTRACK_MAX  0xd20 // <
#define SSB_HW_FREEZE_NTRACK_NOT  0xd24



// EXTF Histograms
#define SSB_EXTF_TRACKHISTO_OLD 0x800 
#define SSB_EXTF_TRACKHISTO     0x880 
#define SSB_EXTF_AUXHISTO_OLD 0x900 
#define SSB_EXTF_AUXHISTO     0x840 
#define SSB_EXTF_DFHISTO_OLD  0xa00 
#define SSB_EXTF_DFHISTO      0x800 
#define SSB_EXTF_OUT_PACKET_LEN_HISTO_OLD  0xb00
#define SSB_EXTF_OUT_PACKET_LEN_HISTO      0x8c0
#define SSB_EXTF_SKEWHISTO  0xc00
#define SSB_EXTF_LAYERMAPHISTO 0x900
// these need to be read out in 16bit transactions
#define SSB_EXTF_DFA_PH_TIMEOUT_HISTO   0xd00
#define SSB_EXTF_DFB_PH_TIMEOUT_HISTO   0xd40
#define SSB_EXTF_AUXA_PH_TIMEOUT_HISTO  0xd80
#define SSB_EXTF_AUXB_PH_TIMEOUT_HISTO  0xdc0
#define SSB_EXTF_L1ID_SKIP_HISTO      0xe00
#define SSB_EXTF_TFFIFO_L1ID_SKIP_HISTO 0xe40
#define SSB_EXTF_DFA_L1ID_SKIP_HISTO  0xf00
#define SSB_EXTF_DFB_L1ID_SKIP_HISTO  0xf40
#define SSB_EXTF_AUXA_L1ID_SKIP_HISTO 0xf80
#define SSB_EXTF_AUXB_L1ID_SKIP_HISTO 0xfc0



//HW Histograms
#define SSB_HW_NTRACKS_HISTO     0x900
#define SSB_HW_L1ID_SKIP_HISTO   0x940
#define SSB_HW_CHI2_HISTO        0x980
#define SSB_HW_D0_HISTO          0x9c0
#define SSB_HW_Z0_HISTO          0xa00
#define SSB_HW_COTTH_HISTO       0xa40
#define SSB_HW_PHI0_HISTO        0xa80
#define SSB_HW_CURV_HISTO        0xac0
#define SSB_HW_LAYERMAP_HISTO    0xb00


//// Hitwarrior

#define SSB_ERROR_REGISTER         0x008 
#define SSB_HW_FIRMWARE_VERSION_ADDR  0x630 
#define SSB_HW_EVENTSIN            0x628
#define SSB_HW_EVENTSOUT           0x62c
#define SSB_HW_TRACKSOUT           0x640
#define SSB_HW_COUNTBP             0x650
#define SSB_HW_FULLREG             0x660
#define SSB_HW_FLIC_FC             0x668


#define SSB_HW_PACKET_COUNT_SE               0x66c 
#define SSB_HW_RAW_ERR_COUNTER_LVL1ID        0x670 
#define SSB_HW_SE_PREV_LVL1ID		     0x680 
#define SSB_HW_SE_PREV_LVL1ID_SYNC	     0x690 
#define SSB_HW_SE_ERR_COUNTER_LVL1ID_0	     0x700 
#define SSB_HW_SE_ERR_COUNTER_LVL1ID_1	     0x704 
#define SSB_HW_SE_ERR_COUNTER_LVL1ID_2	     0x708 
#define SSB_HW_SE_ERR_COUNTER_LVL1ID_3	     0x70c 
#define SSB_HW_FQUIN_PREV_LVL1_ID	     0x710 
#define SSB_HW_FQUIN_ERR_COUNTER_LVL1_ID     0x720 
#define SSB_HW_FQUOUT_PREV_LVL1_ID	     0x730 
#define SSB_HW_FQUOUT_ERR_COUNTER_LVL1_ID    0x740 

#define SSB_HW_TEMPERATURE 0x6a8
#define SSB_HW_FWHASH      0x6a0
#define SSB_HW_FWDATE      0x6a4
#define SSB_HW_VCCINT	   0x6c4
#define SSB_HW_VCCAUX	   0x6c8
#define SSB_HW_VPVN	   0x6cc
#define SSB_HW_VREFP       0x6d0
#define SSB_HW_VREFN       0x6d4
#define SSB_HW_VCCBRAM	   0x6d8

#define SSB_HW_EVENT_RATE  0x6b0
#define SSB_HW_TRACK_RATE  0x6b4
#define SSB_HW_FLIC_BPFRAC 0x6b8
#define SSB_HW_FREQ        0x0

//// EXTF
#define SSB_EXTF_STATE       0x454
#define SSB_FIRMWARE_VERSION_ADDR_EXTF  0x624 
#define SSB_EXTF_LINKS       0x628 // link status to aux/df bits:[df_slink_err, df_slink_xoff, df_slink_up, aux_slink_err, aux_slink_xoff, aux_slink_ up], see EXTF.srcs/sources_1/shell/main_exp.v
#define SSB_EXTF_DF_LINK_DISPARITY_ERRORS 0x758
#define SSB_EXTF_AUX_LINK_DISPARITY_ERRORS 0x75c
#define SSB_EXTF_DF_LINK_NONINTABLE_ERRORS 0x760
#define SSB_EXTF_AUX_LINK_NONINTABLE_ERRORS 0x764
#define SSB_DISABLE_EXTF 0x7f4


#define SSB_LVLID_DFA        0x62c
#define SSB_LVLID_DFB        0x630
#define SSB_LVLID_AUXA       0x634
#define SSB_LVLID_AUXB       0x638
#define SSB_ERR_LVLID_DFA    0x63c
#define SSB_ERR_LVLID_DFB    0x640
#define SSB_ERR_LVLID_AUXA   0x644
#define SSB_ERR_LVLID_AUXB   0x648

#define SSB_EXTF_EVENTSIN    0x64c
#define SSB_EXTF_EVENTSOUT   0x650
#define SSB_EXTF_TRACKSEXO   0x654
#define SSB_EXTF_TRACKSNOM   0x658
#define SSB_EXTF_TRACKSPIX   0x65c
#define SSB_EXTF_TRACKSSCT   0x660
#define SSB_EXTF_COUNTBPEXP  0x664
#define SSB_EXTF_COUNTBPDFA  0x670
#define SSB_EXTF_COUNTBPDFB  0x674
#define SSB_EXTF_COUNTBPAUXA 0x668
#define SSB_EXTF_COUNTBPAUXB 0x66c
#define SSB_EXTF_COUNTBPHW   0x678
#define SSB_EXTF_FULLREG     0x67c
#define SSB_EXTF_FWHASH      0x680
#define SSB_EXTF_FWDATE      0x684
#define SSB_EXTF_TEMPERATURE 0x688
#define SSB_EXTF_RATEIN      0x690
#define SSB_EXTF_FREQ        0x69c
#define SSB_EXTF_RATEOUT     0x694
#define SSB_EXTF_RATENOM     0x698
#define SSB_EXTF_RATEPIX     0x6b0
#define SSB_EXTF_RATESCT     0x6b4
#define SSB_EXTF_DFABPFRAC   0x6a0
#define SSB_EXTF_DFBBPFRAC   0x6a4
#define SSB_EXTF_AUXABPFRAC  0x6a8
#define SSB_EXTF_AUXBBPFRAC  0x6ac
#define SSB_EXTF_RESET       0x750

#define SSB_EXTF_EXP_STATE    0x6e0
#define SSB_EXTF_READY        0x6e4
#define SSB_EXTF_MASTER_STATE 0x6e8
#define SSB_EXTF_SE_STATE     0x6ec
#define SSB_EXTF_TFW_FULL     0x6f0

#define SSB_EXTF_EOR_DF       0x7a0
#define SSB_EXTF_EOR_AUX      0x7a4
#define SSB_EXTF_EOP_DF       0x7a8
#define SSB_EXTF_EOP_AUX      0x7ac
#define SSB_EXTF_EOL          0x7b0
#define SSB_EXTF_CNT_TRAILER  0x7b4
#define SSB_EXTF_LAST_TTOTF   0x7b8
#define SSB_EXTF_LAST_TTOTF_E 0x7bc
#define SSB_EXTF_CNT_L1ID_OUT 0x7c0
#define SSB_EXTF_CNT_L1ID_OUT_E 0x7c4

#define SSB_EXTF_AUX_EXTRA_WORD  0x6b8

#define SSB_EXTF_DF_TX_RESET  0x790
#define SSB_EXTF_DF_RX_RESET  0x794
#define SSB_EXTF_AUX_TX_RESET 0x798
#define SSB_EXTF_AUX_RX_RESET 0x79c

#define SSB_EXTF_SYNC_PROC_EVENTS_COUNT_DFA  0x6c0
#define SSB_EXTF_SYNC_PROC_EVENTS_COUNT_DFB  0x6c4
#define SSB_EXTF_SYNC_PROC_EVENTS_COUNT_AUXA 0x6c8
#define SSB_EXTF_SYNC_PROC_EVENTS_COUNT_AUXB 0x6cc

#define SSB_EXTF_SYNC_DISC_EVENTS_COUNT_DFA  0x6d0
#define SSB_EXTF_SYNC_DISC_EVENTS_COUNT_DFB  0x6d4
#define SSB_EXTF_SYNC_DISC_EVENTS_COUNT_AUXA 0x6d8
#define SSB_EXTF_SYNC_DISC_EVENTS_COUNT_AUXB 0x6dc




#endif
