#ifndef SSB_BOARD_H
#define SSB_BOARD_H

#include "ssb/ssb_fpga.h"
#include "ssb/ssb_fpga_proc.h"
#include "ssb/ssb_fpga_hitwarrior.h"
#include "ssb/ssb_srsb.h"

#include <vector>
#include <math.h>


#include "DFdal/ReadoutConfiguration.h"

#include "ssb/dal/ReadoutModule_Ssb.h"
#include "ssb/dal/Ssb_Fpga.h"

#include "ftkcommon/SourceIDSpyBuffer.h"


namespace daq
{
  namespace ftk
  {

    class ssb_board
    {

    public:
      // constructor
      ssb_board(uint slot, uint linkID);
      
      std::string name_ftk(){return m_name;};

 
      void setup(const dal::ReadoutModule_Ssb*);
      void configure(unsigned int runNumber);
      void unconfigure();
      void connect();
      void disconnect();
      void prepareForRun();
      void stop();

      const std::vector<std::shared_ptr<ftk::ssb_fpga>> getFPGAs(){return m_FPGAs;};


      void freezeSpybuffers(); // Triggers spybuffer freeze on all FPGAs via VME
      void unfreezeSpybuffers(bool force = false); // Triggers spybuffer unfreeze on all FPGAs via VME


      void freezeSRSB(); // Triggers SRSB freeze on all FPGAs via VME
      void unfreezeSRSB(bool force = false); // Triggers SRSB unfreeze on all FPGAs via VME


      // Check link status and throw errors
      void checkLinks();

      // Check firmware version against OKS and throw errors
      void checkFWVersion();
      
      std::vector< std::pair<std::shared_ptr<daq::ftk::SpyBuffer>, uint32_t> > getSpybuffers(); // todo make these pass by reference, causes a crash now
      std::vector< std::pair<std::shared_ptr<daq::ftk::SpyBuffer>, uint32_t> > getSpybuffersHistos(); // todo make these pass by reference, causes a crash now
      const std::vector< std::shared_ptr<ssb_srsb> > getSRSB(bool update=true);
      std::vector< std::pair<std::shared_ptr<daq::ftk::SpyBuffer>, uint32_t> > getSRSB_emon();

      std::shared_ptr<daq::ftk::SpyBuffer> getSpybuffersHisto(uint addr); // return data for single histo at address addr TODO


      std::vector<bool> getEnabledFPGAs(){ return m_enabled_fpgas;};

      std::vector<bool> getEnabledChannels(){return m_enabled_channels;};

      const std::shared_ptr< std::vector<uint32_t> >  getEXTFFreq(){return m_EXTF_Freq;};

      const std::shared_ptr< std::vector<bool> >  getEXTFOldFW(){return m_EXTF_Old_FW;};

    private:
      const dal::ReadoutModule_Ssb * m_conf;
      std::string m_name;
      uint m_slot;
      uint m_LinkID;
      VMEInterface *m_fpga_vmeif; // vme interface (FPGA_VMEIF)
      std::vector<bool> m_enabled_fpgas;
      std::vector<bool> m_enabled_channels;
      std::shared_ptr< std::vector<uint32_t> > m_EXTF_Freq;
      std::shared_ptr< std::vector<bool> > m_EXTF_Old_FW;

      // vector fpga class pointers
      std::vector<std::shared_ptr<ftk::ssb_fpga>> m_FPGAs;

      std::vector< std::shared_ptr<ssb_srsb> > m_SRSBs;

    };

  }
}

#endif
